﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Clients.DecisionEngine;
using Moq;
using Xunit;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.InstantBankVerification.Tests
{
    public class InstantBankVerificationTest : BaseService
    {
        public InstantBankVerificationTest()
        {
            FillData();
            Configuration = new Mock<IConfiguration>();
            BankLinkRepository = new Mock<IBankLinkRepository>();
            BankAccountRepository = new Mock<IBankAccountRepository>();
            BankAccountHistoryRepository = new Mock<IBankAccountHistoryRepository>();
            BankTransactionRepository = new Mock<IBankTransactionRepository>();
            BalanceRepository = new Mock<IBalanceRepository>();
            Logger = new Mock<ILogger>();
            TenantTime = new Mock<ITenantTime>();
            DecisionEngineService = new Mock<IDecisionEngineService>();
            PlaidUniteService = new Mock<IPlaidUniteService>();
            FinicityService = new Mock<IFinicityService>();
            OcrolusPerfectAuditService = new Mock<IOcrolusPerfectAuditService>();

            Configuration.Setup(x => x.TransactionDays).Returns(365);
            Configuration.Setup(x => x.FilterInMonths).Returns(2);

            Service = new InstantBankVerificationService(Configuration.Object, BankLinkRepository.Object, BankAccountRepository.Object,
                                                          BankAccountHistoryRepository.Object, BankTransactionRepository.Object, Logger.Object,
                                                          TenantTime.Object, DecisionEngineService.Object, PlaidUniteService.Object,null, FinicityService.Object, OcrolusPerfectAuditService.Object, BalanceRepository.Object);
        }

        private IInstantBankVerificationService Service { get; }

        private Mock<IConfiguration> Configuration { get; }

        private Mock<IBankLinkRepository> BankLinkRepository { get; }

        private Mock<IBankAccountRepository> BankAccountRepository { get; }

        private Mock<IBankAccountHistoryRepository> BankAccountHistoryRepository { get; }

        private Mock<IBankTransactionRepository> BankTransactionRepository { get; }
        private Mock<IBalanceRepository> BalanceRepository { get; }

        private Mock<ILogger> Logger { get; }

        private Mock<ITenantTime> TenantTime { get; }

        private Mock<IDecisionEngineService> DecisionEngineService { get; }

        private Mock<IPlaidUniteService> PlaidUniteService { get; }
        private Mock<IFinicityService> FinicityService { get; }
        private Mock<IOcrolusPerfectAuditService> OcrolusPerfectAuditService { get; }

        private IConfiguration ConfigurationResponse { get; set; }

        private string EntityType { get; set; } = "application";

        private string EntityId { get; set; } = "529711";

        private string FilterType { get; set; } = "ALL";

        private string AccountId { get; set; }

        private RequestConnection RequestConnection { get; set; }

        private IResponseConnection ResponseConnection { get; set; }

        private ITransactionCashflowResponse TransactionCashflowResponse { get; set; }

        private ICashflowPayload CashflowPayloadResponse { get; set; }

        private IBankAccountWrapperResponse BankAccountWrapperResponse { get; set; }

        private RequestManualBankLink RequestManualBankLink { get; set; }

        private RequestAddBank RequestAddBank { get; set; }

        private RequestTransaction RequestTransaction { get; set; }

        private object GetAccountDetailsResult { get; set; }

        private IList<IBankAccount> GetAccountDetailsResult1 { get; set; }

        private string BankLinkId { get; set; } = "5a65935a5e75540007b23fd7";

        private string AccessToken { get; set; } = "access-sandbox-3404b250-50a6-49f4-8e0f-cd84227d8ab6";

        #region Methods

        /// <summary>
        /// TestBankLinkingAsync
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task TestBankLinkingAsync()
        {
            PlaidUniteService.Setup(x=>x.AddBankLinkAndAccounts(EntityType,EntityId,RequestConnection))
                .Returns(Task.FromResult<IResponseConnection>(ResponseConnection));

            var result = await Service.BankLinking(EntityType, EntityId, RequestConnection);
            Assert.Same(result, this.ResponseConnection);
        }

        /// <summary>
        /// TestGetAccounts
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task TestGetAccounts()
        {
            BankLinkRepository.Setup(x => x.GetBankLinkId(RequestConnection.ProviderToken))
               .Returns(Task.FromResult<string>(BankLinkId));

            this.BankAccountRepository.Setup(x => x.GetAccounts(BankLinkId)).Returns(Task.FromResult<IList<IBankAccount>>(GetAccountDetailsResult1));

            var result = await Service.GetAccounts(EntityType, EntityId, RequestConnection);
            Assert.Same(result, this.GetAccountDetailsResult1);
        }

        /// <summary>
        /// FillData
        /// </summary>
        private void FillData()
        {
            ConfigurationResponse = JsonConvert.DeserializeObject<Configuration>(this.LoadJson("IBV/configuration.json"));

            this.RequestConnection = JsonConvert.DeserializeObject<RequestConnection>(this.LoadJson("IBV/requestConnection.json"));
            this.ResponseConnection = JsonConvert.DeserializeObject<ResponseConnection>(this.LoadJson("IBV/responseConnection.json"));
            this.GetAccountDetailsResult = JsonConvert.DeserializeObject<object>(this.LoadJson("IBV/accountResult.json"));
            this.TransactionCashflowResponse = JsonConvert.DeserializeObject<TransactionCashflowResponse>(this.LoadJson("IBV/transactionCashflowResponse.json"));
            this.CashflowPayloadResponse = JsonConvert.DeserializeObject<CashflowPayload>(this.LoadJson("IBV/cashflowPayload.json"));
            this.BankAccountWrapperResponse = JsonConvert.DeserializeObject<BankAccountWrapperResponse>(this.LoadJson("IBV/accountWrapperResponse.json"));
            this.RequestManualBankLink = JsonConvert.DeserializeObject<RequestManualBankLink>(this.LoadJson("IBV/requestManualBankLink.json"));
            this.RequestAddBank = JsonConvert.DeserializeObject<RequestAddBank>(this.LoadJson("IBV/requestAddBank.json"));
            this.RequestTransaction = JsonConvert.DeserializeObject<RequestTransaction>(this.LoadJson("IBV/transactionPullRequest.json"));
            this.GetAccountDetailsResult1 = JsonConvert.DeserializeObject<List<BankAccount>>(this.LoadJson("IBV/accountResult.json"))
                                                    .ConvertAll<IBankAccount>(item => item as IBankAccount);
         
        }

        #endregion
    }
}

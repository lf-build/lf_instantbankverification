﻿using LendFoundry.Foundation.Logging;
using LendFoundry.InstantBankVerification.Api.Controllers;
using Microsoft.AspNet.Mvc;
using Moq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.InstantBankVerification.Api.Tests
{
    public class ApiControllerTest : BaseService
    {
        public ApiControllerTest()
        {
            FillData();
            Logger = new Mock<ILogger>();
            this.InstantBankVerificationService = new Mock<IInstantBankVerificationService>();
        }

        private Mock<ILogger> Logger { get; }

        private Mock<IInstantBankVerificationService> InstantBankVerificationService { get; set; }

        private RequestConnection RequestConnection { get; set; }

        private IResponseConnection ResponseConnection { get; set; }

        private ITransactionCashflowResponse TransactionCashflowResponse { get; set; }

        private ICashflowPayload CashflowPayloadResponse { get; set; }

        private IBankAccountWrapperResponse BankAccountWrapperResponse { get; set; }

        private RequestManualBankLink RequestManualBankLink { get; set; }

        private RequestAddBank RequestAddBank { get; set; }

        private RequestTransaction RequestTransaction { get; set; }

        private object GetAccountDetailsResult { get; set; }

        private string EntityType { get; set; } = "application";

        private string EntityId { get; set; } = "529711";

        private string FilterType { get; set; } = "ALL";

        private string AccountId { get; set; }

        #region PBGP

        /// <summary>
        /// AddBankTest
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task AddBankTest()
        {
            InstantBankVerificationService.Setup(x => x.AddBank(EntityType, EntityId, this.RequestAddBank))
                    .Returns(Task.FromResult<bool>(false));
            var controller = new ApiController(
                this.InstantBankVerificationService.Object,
                this.Logger.Object);
            var result = await controller.AddBank(EntityType, EntityId, this.RequestAddBank);
            var resultResponse = ((HttpOkObjectResult)result).Value;
            Assert.Equal(resultResponse, false);
        }

        /// <summary>
        /// DeleteAccountTest
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task DeleteAccountTest()
        {
            InstantBankVerificationService.Setup(x => x.DeleteAccount(EntityType, EntityId, this.RequestAddBank))
                .Returns(Task.FromResult<bool>(false));
            var controller = new ApiController(
                this.InstantBankVerificationService.Object,
                this.Logger.Object);
            var result = await controller.DeleteAccount(EntityType, EntityId, this.RequestAddBank);
            var resultResponse = ((HttpOkObjectResult)result).Value;
            Assert.Equal(resultResponse, false);
        }

        /// <summary>
        /// PullTransactionTest
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task PullTransactionTest()
        {
            InstantBankVerificationService.Setup(x => x.PullTransaction(this.RequestTransaction));
            var controller = new ApiController(
                this.InstantBankVerificationService.Object,
                this.Logger.Object);
            await controller.PullTransaction(this.RequestTransaction);
            Assert.True(true);
        }

        #endregion

        #region Methods

        /// <summary>
        /// BankConnectTest
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task BankConnectTest()
        {
            this.InstantBankVerificationService.Setup(x => x.BankLinking(EntityType, EntityId, this.RequestConnection))
                 .Returns(Task.FromResult<IResponseConnection>(this.ResponseConnection));
            var controller = new ApiController(
                this.InstantBankVerificationService.Object,
                this.Logger.Object);
            var result = await controller.BankConnect(EntityType, EntityId, this.RequestConnection);
            var resultResponse = ((HttpOkObjectResult)result).Value as IResponseConnection;
            Assert.Same(resultResponse, this.ResponseConnection);
        }

        /// <summary>
        /// GetAccountsDetailsTest
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task GetAccountsDetailsTest()
        {
            InstantBankVerificationService.Setup(x => x.GetAccounts(EntityType, EntityId, this.RequestConnection))
                .Returns(Task.FromResult<object>(this.GetAccountDetailsResult));
            var controller = new ApiController(
                this.InstantBankVerificationService.Object,
                this.Logger.Object);
            var result = await controller.GetAccountsDetails(EntityType, EntityId, this.RequestConnection);
            var resultResponse = ((HttpOkObjectResult)result).Value as IResponseConnection;
            Assert.Same(resultResponse, this.GetAccountDetailsResult);
        }

        /// <summary>
        /// DeleteConnectionTest
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task DeleteConnectionTest()
        {
            InstantBankVerificationService.Setup(x => x.DeleteConnection(EntityType, EntityId, this.RequestConnection))
                .Returns(Task.FromResult<bool>(true));
            var controller = new ApiController(
                this.InstantBankVerificationService.Object,
                this.Logger.Object);
            var result = await controller.DeleteConnection(EntityType, EntityId, this.RequestConnection);
            var resultResponse = ((HttpOkObjectResult)result).Value;
            Assert.Same(resultResponse, true);
        }

        /// <summary>
        /// GetTransactionsTest
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task GetTransactionsTest()
        {
            this.AccountId = "LKQLqBVQyKH7vvGvQQG3UxLMyLbnjdcgWvD6x";
            InstantBankVerificationService.Setup(x => x.GetTransactionForCashflow(EntityType, EntityId, FilterType, AccountId))
                .Returns(Task.FromResult<ITransactionCashflowResponse>(TransactionCashflowResponse));
            var controller = new ApiController(
                this.InstantBankVerificationService.Object,
                this.Logger.Object);
            var result = await controller.GetTransactions(EntityType, EntityId, FilterType, AccountId);
            var resultResponse = ((HttpOkObjectResult)result).Value as ITransactionCashflowResponse;
            Assert.Same(resultResponse, TransactionCashflowResponse);
        }

        /// <summary>
        /// GetCashflowPayloadTest
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task GetCashflowPayloadTest()
        {
            this.AccountId = "LKQLqBVQyKH7vvGvQQG3UxLMyLbnjdcgWvD6x";
            InstantBankVerificationService.Setup(x => x.GetCashflowPayload(EntityType, EntityId, AccountId))
                .Returns(Task.FromResult<ICashflowPayload>(CashflowPayloadResponse));
            var controller = new ApiController(
                this.InstantBankVerificationService.Object,
                this.Logger.Object);
            var result = await controller.GetCashflowPayload(EntityType, EntityId, AccountId);
            var resultResponse = ((HttpOkObjectResult)result).Value as ICashflowPayload;
            Assert.Same(resultResponse, CashflowPayloadResponse);
        }

        /// <summary>
        /// GetAccountInformationTest
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task GetAccountInformationTest()
        {
            this.AccountId = "LKQLqBVQyKH7vvGvQQG3UxLMyLbnjdcgWvD6x";
            InstantBankVerificationService.Setup(x => x.GetAccount(EntityType, EntityId, AccountId))
                .Returns(Task.FromResult<IBankAccountWrapperResponse>(BankAccountWrapperResponse));
            var controller = new ApiController(
                this.InstantBankVerificationService.Object,
                this.Logger.Object);
            var result = await controller.GetAccountInformation(EntityType, EntityId, AccountId);
            var resultResponse = ((HttpOkObjectResult)result).Value as IBankAccountWrapperResponse;
            Assert.Same(resultResponse, BankAccountWrapperResponse);
        }

        [Fact]
        public async Task ManualUploadDetailsTest()
        {
            InstantBankVerificationService.Setup(x => x.AddManualDetails(EntityType, EntityId, RequestManualBankLink));
            //.Returns(Task.FromResult<IBankAccountWrapperResponse>(BankAccountWrapperResponse));
            var controller = new ApiController(
                this.InstantBankVerificationService.Object,
                this.Logger.Object);
            await controller.ManualUploadDetails(EntityType, EntityId, RequestManualBankLink);
            Assert.True(true);
        }

        #endregion

        //[Fact]
        //public async Task ManualUploadTest()
        //{
        //    InstantBankVerificationService.Setup(x => x.AddManualDetails(EntityType, EntityId, RequestManualBankLink));
        //    //.Returns(Task.FromResult<IBankAccountWrapperResponse>(BankAccountWrapperResponse));
        //    var controller = new ApiController(
        //        this.InstantBankVerificationService.Object,
        //        this.Logger.Object);
        //    await controller.ManualUploadDetails(EntityType, EntityId, RequestManualBankLink);
        //    Assert.Same(true, true);
        //}



        /// <summary>
        /// FillData
        /// </summary>
        private void FillData()
        {
            this.RequestConnection = JsonConvert.DeserializeObject<RequestConnection>(this.LoadJson("IBV/requestConnection.json"));
            this.ResponseConnection = JsonConvert.DeserializeObject<ResponseConnection>(this.LoadJson("IBV/responseConnection.json"));
            this.GetAccountDetailsResult = JsonConvert.DeserializeObject<object>(this.LoadJson("IBV/accountResult.json"));
            this.TransactionCashflowResponse = JsonConvert.DeserializeObject<TransactionCashflowResponse>(this.LoadJson("IBV/transactionCashflowResponse.json"));
            this.CashflowPayloadResponse = JsonConvert.DeserializeObject<CashflowPayload>(this.LoadJson("IBV/cashflowPayload.json"));
            this.BankAccountWrapperResponse = JsonConvert.DeserializeObject<BankAccountWrapperResponse>(this.LoadJson("IBV/accountWrapperResponse.json"));
            this.RequestManualBankLink = JsonConvert.DeserializeObject<RequestManualBankLink>(this.LoadJson("IBV/requestManualBankLink.json"));
            this.RequestAddBank = JsonConvert.DeserializeObject<RequestAddBank>(this.LoadJson("IBV/requestAddBank.json"));
            this.RequestTransaction = JsonConvert.DeserializeObject<RequestTransaction>(this.LoadJson("IBV/transactionPullRequest.json"));
        }
    }
}

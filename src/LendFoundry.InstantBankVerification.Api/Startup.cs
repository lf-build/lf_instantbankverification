﻿using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.InstantBankVerification.Persistence;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.PlaidIBV.Client;
using LendFoundry.Tenant.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Security.Encryption;


using LendFoundry.EventHub.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using LendFoundry.Syndication.PerfectAudit.Client;

namespace LendFoundry.InstantBankVerification.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "InstantBankVerification"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.InstantBankVerification.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif
            //// services
            services.AddTenantTime();

            services.AddTokenHandler();
            services.AddEncryptionHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);

            services.AddConfigurationService<Configuration>(
                Settings.Configuration.Host,
                Settings.Configuration.Port,
                Settings.ServiceName);


            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });

            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddDecisionEngine(Settings.DecisionEngine.Host, Settings.DecisionEngine.Port);
            services.AddPlaidIbv(Settings.PlaidV2ibv.Host, Settings.PlaidV2ibv.Port);
            services.AddPerfectAuditServic(Settings.PerfectAudit.Host, Settings.PerfectAudit.Port);

            services.AddTransient<IInstantBankVerificationService, InstantBankVerificationService>();
            services.AddTransient<IConfiguration>(provider => provider.GetRequiredService<IConfigurationService<Configuration>>().Get());

            //// Providers
            services.AddTransient<IPlaidListener, PlaidListener>();
            services.AddTransient<IPlaidUniteService, PlaidUniteService>();

            services.AddTransient<IBankLinkRepository, BankLinkRepository>();
            services.AddTransient<IBankAccountRepository, BankAccountRepository>();
            services.AddTransient<IBankAccountHistoryRepository, BankAccountHistoryRepository>();
            services.AddTransient<IBankTransactionRepository, BankTransactionRepository>();

            services.AddTransient<IInstantBankServiceFactory, InstantBankServiceFactory>();
            services.AddTransient<IPlaidUniteServiceFactory, PlaidUniteServiceFactory>();

            services.AddTransient<IIBSRepositoryFactory, IBSRepositoryFactory>();

            services.AddTransient<IFinicityService, FinicityService>();
            services.AddTransient<IOcrolusPerfectAuditService, OcrolusPerfectAuditService>();
            services.AddTransient<IFinicityProxy, FinicityProxy>();
            services.AddTransient<IFinicityServiceFactory, FinicityServiceFactory>();
            services.AddTransient<IOcrolusPerfectAuditServiceFactory, OcrolusPerfectAuditServiceFactory>();
            services.AddTransient<IFinicityProxyFactory, FinicityProxyFactory>();
            services.AddTransient<IClientTrackRepository, ClientTrackRepository>();
            services.AddTransient<IBalanceRepository, BalanceRepository>();
            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "InstantBankVerification Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            //app.UseSwaggerDocumentation();

            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UsePlaidListener();
            app.UseHealthCheck();
            app.UseMvc();

        }
    }
}

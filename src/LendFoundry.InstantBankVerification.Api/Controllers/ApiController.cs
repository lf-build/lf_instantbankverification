﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;

#if DOTNET2
using Microsoft.AspNetCore.Http;
using  Microsoft.AspNetCore.Mvc;
using System.Net.Http.Headers;
using LendFoundry.Syndication.PerfectAudit.Abstractions;
using LendFoundry.Syndication.PerfectAudit;
#else
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Net.Http.Headers;
using LendFoundry.Syndication.PerfectAudit;
using LendFoundry.Syndication.PerfectAudit.Abstractions;
#endif
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using RestSharp.Extensions;




namespace LendFoundry.InstantBankVerification.Api.Controllers
{
    /// <summary>
    /// ApiController
    /// </summary>
    public class ApiController : ExtendedController
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiController"/> class.
        /// </summary>
        /// <param name="service">service</param>
        public ApiController(IInstantBankVerificationService service, ILogger logger) : base(logger)
        {
            Service = service;
        }
        #endregion

        #region Variable Declaration

        /// <summary>
        /// Gets Service
        /// </summary>
        private IInstantBankVerificationService Service { get; }

        ///// <summary>
        ///// Gets Logger
        ///// </summary>
        //private ILogger Logger { get; }

        /// <summary>
        /// NoContent
        /// </summary>
        private static readonly NoContentResult NoContent = new NoContentResult();

        #endregion  

        #region Methods

        /// <summary>
        /// BankConnect
        /// </summary>
        /// <param name="entityId">entityId</param>
        /// <param name="entityType">entityType</param>
        /// <param name="request">request</param>
        /// <returns>true or false</returns>
        [HttpPost]
        [Route("/{entityType}/{entityId}/connect")]
#if DOTNET2
        [ProducesResponseType(typeof(IResponseConnection), 200)]
        [ProducesResponseType(typeof(NoContentResult), 204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> BankConnect(string entityType, string entityId, [FromBody]RequestConnection request)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the BankConnect Method...");
                    var result = await Service.BankLinking(entityType, entityId, request);
                    if (result == null)
                    {
                        return NoContent;
                    }
                    return this.Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method BankConnect Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// GetAccountsDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/{entityType}/{entityId}/getaccount")]
#if DOTNET2
        [ProducesResponseType(typeof(Object), 200)]
        [ProducesResponseType(typeof(NoContentResult), 204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
       public Task<IActionResult> GetAccountsDetails(string entityType, string entityId, [FromBody]RequestConnection request)
        {
            return ExecuteAsync(async () =>
            {
                Logger.Info("Starting the GetAccountsDetails Method...");
                var result = await Service.GetAccounts(entityType, entityId, request);
                if (result == null)
                {
                    return NoContent;
                }
                return this.Ok(result);
            });
        }

        [HttpPost]
        [Route("/{entityType}/{entityId}/account/sync")]
        [Route("/{entityType}/{entityId}/getaccount")]
#if DOTNET2
        [ProducesResponseType(typeof(Object), 200)]
        [ProducesResponseType(typeof(NoContentResult), 204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> GetAccountsSync(string entityType, string entityId, [FromBody]RequestConnection request)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the GetAccountsSync Method...");
                    var result = await Service.AccountSync(entityType, entityId, request);
                    if (result == null)
                    {
                        Logger.Info("No data found");
                        return NoContent;
                    }
                    return this.Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Info($"Error in GetAccountsSync method : {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }

            });
        }

        /// <summary>
        /// DeleteConnection
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/{entityType}/{entityId}/deleteconnection")]
#if DOTNET2
        [ProducesResponseType(typeof(Boolean), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> DeleteConnection(string entityType, string entityId, [FromBody]RequestConnection request)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the DeleteConnection Method...");
                    return this.Ok(await Service.DeleteConnection(entityType, entityId, request));
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method DeleteConnection Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }



        [HttpGet]
        [Route("/{entityType}/{entityId}/cashflowPayload/{accountId}")]
#if DOTNET2
        [ProducesResponseType(typeof(ICashflowPayload), 200)]
        [ProducesResponseType(typeof(NoContentResult), 204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> GetCashflowPayload(string entityType, string entityId, string accountId)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the GetCashflowPayload Method...");
                    var result = await Service.GetCashflowPayload(entityType, entityId, accountId);
                    if (result == null)
                    {
                        return NoContent;
                    }
                    return this.Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method GetCashflowPayload Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        //// NOT IN USE
        /// <summary>
        /// GetTransactions
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/transactions/{accountId}")]
#if DOTNET2
        [ProducesResponseType(typeof(List<IBankTransaction>), 200)]
        [ProducesResponseType(typeof(NoContentResult), 204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> GetTransactions(string accountId)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the GetTransactions Method...");
                    var result = await Service.GetTransactions(accountId);
                    if (result == null)
                    {
                        return NoContent;
                    }
                    return this.Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method GetTransactions Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }



#region Manual

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("/{entityType}/{entityId}/manual/details")]
#if DOTNET2
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> ManualUploadDetails(string entityType, string entityId, [FromBody] RequestManualBankLink request)
        {
            try
            {
                await Service.AddManualDetails(entityType, entityId, request);
                return Ok();
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        //// NOT IN USE
        /// <summary>
        /// ManualUpload
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPut("/{entityType}/{entityId}/manual/upload")]
#if DOTNET2
        [ProducesResponseType(typeof(Object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> ManualUpload(string entityType, string entityId, IFormFile file)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                file.OpenReadStream().CopyTo(ms);
                ms.Position = 0;
                var fileName = ContentDispositionHeaderValue
                            .Parse(file.ContentDisposition)
                            .FileName
                            .Trim('"');

                PostedFileDetails fileDetails = new PostedFileDetails()
                {
                    FileName = fileName,
                    Content = ms
                };
                return Ok(await Service.UploadManualCsv(entityType, entityId, fileDetails));
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
        }

#endregion

#region navigator use

        [HttpGet("{entitytype}/{entityid}/transaction/{accountId}/{fromDate}/{toDate}")]
#if DOTNET2
        [ProducesResponseType(typeof(List<IBankTransaction>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetTransactions(string entityType, string entityId, string accountId, string fromDate, string toDate)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        var result = await Service.GetTransactions(entityType, entityId, accountId, fromDate, toDate);
                        return result;

                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }


        [HttpGet]
        [Route("/{entityType}/{entityId}/transaction/{filter}/{accountId}")]
#if DOTNET2
        [ProducesResponseType(typeof(ITransactionCashflowResponse), 200)]
        [ProducesResponseType(typeof(NoContentResult), 204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> GetTransactions(string entityType, string entityId, string filter, string accountId)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the GetCashflowPayload Method...");
                    var result = await Service.GetTransactionForCashflow(entityType, entityId, filter, accountId);
                    if (result == null)
                    {
                        return NoContent;
                    }
                    return this.Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method GetCashflowPayload Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        [HttpGet]
        [Route("/{entityType}/{entityId}/transactionResult/{accountId}/{fromDate?}/{toDate?}")]
#if DOTNET2
        [ProducesResponseType(typeof(ITransactionCashflowResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(NoContentResult), 204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> GetResultTransactions(string entityType, string entityId, string accountId, string fromDate = null, string toDate = null)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the GetResultTransactions Method...");
                    var result = await Service.GetTransactionForCashflow(entityType, entityId, accountId, fromDate, toDate);
                    if (result == null)
                    {
                        return NoContent;
                    }
                    return this.Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method GetResultTransactions Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        [HttpPost("/{entityType}/{entityId}/updateBankAccount")]
#if DOTNET2
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> AddOrUpdateBankAccount(string entityType, string entityId, [FromBody] BankAccountRequest request)
        {
            try
            {
                await Service.UpdateBankAccount(entityType, entityId, request);
                return Ok();
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("/{entityType}/{entityId}/accountInfo/{accountId}")]
#if DOTNET2
        [ProducesResponseType(typeof(IBankAccountResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> GetBankAccountInformation(string entityType, string entityId, string accountId)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the GetCashflowPayload Method...");
                    var result = await Service.GetBankAccountInformation(entityType, entityId, accountId);
                    if (result == null)
                    {
                        return NoContent;
                    }
                    return this.Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method GetCashflowPayload Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// GetAccountIds
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{entityId}/accountIds/{bankLinkId}")]
#if DOTNET2
        [ProducesResponseType(typeof(List<string>), 200)]
        [ProducesResponseType(typeof(NoContentResult), 204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> GetAccountIds(string entityId, string bankLinkId)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the GetAccountIds Method...");
                    var result = await Service.GetAccountIds(entityId, bankLinkId);
                    if (result == null)
                    {
                        return NoContent;
                    }
                    return this.Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method GetCashflowPayload Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }


        /// <summary>
        /// AddAccountPreference
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="accountPreference"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityId}/add/account/preference")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> AddAccountPreference(string entityType, string entityId, [FromBody]AccountPreferenceRequest accountPreference)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the AddAccountPreference Method...");
                    var result = await Service.AddAccountPreference(entityType, entityId, accountPreference);
                    return this.Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method AddAccountPreference Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        [HttpPost("{entitytype}/{entityId}/accountTypeSet")]
#if DOTNET2
        [ProducesResponseType(typeof(IAccountTypeResponse), 200)]
        [ProducesResponseType(typeof(NoContentResult), 204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> GetAccountByType(string entityType, string entityId, [FromBody]AccountTypeRequest request)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the GetAccountByType Method...");
                    var result = await Service.GetAccountByType(entityType, entityId, request);
                    if (result == null)
                    {
                        return NoContent;
                    }
                    return this.Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method GetAccountByType Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// GetBankAccountDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        [HttpGet("{entitytype}/{entityId}/{accountId}")]
#if DOTNET2
        [ProducesResponseType(typeof(IBankAccount), 200)]
        [ProducesResponseType(typeof(NoContentResult), 204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> GetBankAccountDetails(string entityType, string entityId, string accountId)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the GetBankAccountDetails Method...");
                    var result = await Service.GetBankAccountDetails(entityType, entityId, accountId);
                    if (result == null)
                    {
                        return NoContent;
                    }
                    return this.Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method GetBankAccountDetails Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }


        /// <summary>
        /// GetBankAccountDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        [HttpGet("{entitytype}/{entityId}/account/all")]
#if DOTNET2
        [ProducesResponseType(typeof(IList<IBankAccount>), 200)]
        [ProducesResponseType(typeof(NoContentResult), 204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> GetAllAccountDetails(string entityType, string entityId)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the AddAccountPreference Method...");
                    var result = await Service.GetAllAccountDetails(entityType, entityId);
                    if (result == null)
                    {
                        return NoContent;
                    }
                    return this.Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method AddAccountPreference Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }
#endregion

        [HttpPost("/updaterunningbalance")]
#if DOTNET2
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> UpdateRunningBalance()
        {
            try
            {
                await Service.UpdateRunningBalance();
                return Ok();
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        [HttpGet("{bankLinkId}/{entityType}/{entityId}/addBankLinks")]
#if DOTNET2
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> AddBankLinks(string bankLinkId, string entityType, string entityId)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the AddBankLinks Method...");
                    await Service.AddBankLink(bankLinkId, entityType, entityId);
                    return Ok();
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method AddBankLinks Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

#endregion

#region Filter Methods

        [HttpGet]
        [Route("/getbanklinkdata/{entityId}/{bankLinkId}")]
#if DOTNET2
        [ProducesResponseType(typeof(IFilterBankLinkView), 200)]
        [ProducesResponseType(typeof(NoContentResult), 204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> GetBankLinkData(string entityId, string bankLinkId)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the GetBankLinkData Method...");
                    var result = await Service.GetBankLinkFilterViewData(entityId, bankLinkId);
                    if (result == null)
                    {
                        return NoContent;
                    }
                    return this.Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method GetBankLinkData Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        //// NOT IN USE
        /// <summary>
        /// GetAccountInformation
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/{entityType}/{entityId}/account/{accountId}")]
#if DOTNET2
        [ProducesResponseType(typeof(IBankAccountWrapperResponse), 200)]
        [ProducesResponseType(typeof(NoContentResult), 204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> GetAccountInformation(string entityType, string entityId, string accountId)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the GetAccountInformation Method...");
                    var result = await Service.GetAccount(entityType, entityId, accountId);
                    if (result == null)
                    {
                        return NoContent;
                    }
                    return this.Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method GetCashflowPayload Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        [HttpGet]
        [Route("/getaccountdata/{bankLinkId}")]
#if DOTNET2
        [ProducesResponseType(typeof(List<IFilterAccountDataView>), 200)]
        [ProducesResponseType(typeof(NoContentResult), 204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> GetAccountData(string bankLinkId)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the GetBankLinkData Method...");
                    var result = await Service.GetAccoutFilterData(bankLinkId);
                    if (result == null || result.Count == 0)
                    {
                        return NoContent;
                    }
                    return this.Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method GetBankLinkData Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }


#endregion

#region PBGP Methods

        /// <summary>
        /// AddBank
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/{entityType}/{entityId}/addBank")]
#if DOTNET2
        [ProducesResponseType(typeof(Boolean), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> AddBank(string entityType, string entityId, [FromBody]RequestAddBank request)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the AddBank Method...");
                    var result = await Service.AddBank(entityType, entityId, request);
                    return this.Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method AddBank Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// DeleteAccount
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/{entityType}/{entityId}/removeBank")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> DeleteAccount(string entityType, string entityId, [FromBody]RequestAddBank request)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the DeleteAccount Method...");
                    var result = await Service.DeleteAccount(entityType, entityId, request);
                    return this.Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method DeleteAccount Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// PullTransaction
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/pullTransaction")]
#if DOTNET2
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> PullTransaction([FromBody]RequestTransaction request)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the PullTransaction Method...");
                    await Service.PullTransaction(request);
                    return this.Ok();
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method DeleteAccount Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

#endregion

#region Finicity Methods

        [HttpPost("{entitytype}/{entityid}/accesstoken")]
        [HttpGet("{entitytype}/{entityid}/accesstoken")]
#if DOTNET2
        [ProducesResponseType(typeof(IAccessTokenResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetAccessToken(string entityType, string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        Logger.Info("Call Service Method");
                        var result = await Service.GetAccessToken(entityType, entityId);
                        Logger.Info("Return result");
                        Logger.Info("Access Token : " + result.AccessToken);
                        return result;
                    }));

                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        [HttpPost("{entitytype}/{entityid}/customer/add")]
#if DOTNET2
        [ProducesResponseType(typeof(IResponseConnection), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> AddCustomer(string entityType, string entityId, [FromBody]AddCustomerRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the AddCustomer Method...");
                    var result = await Service.AddCustomer(entityType, entityId, request);
                    return this.Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method AddCustomer Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        [HttpPost("{entitytype}/{entityid}/connect/generate")]
#if DOTNET2
        [ProducesResponseType(typeof(IConnectLinkResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GenerateConnectLink(string entityType, string entityId, [FromBody]ConnectLinkRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        var result = await Service.GenerateConnectLink(entityType, entityId, request);

                        return result;


                    }));

                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        [HttpPost("{entitytype}/{entityid}/customer/accounts")]
#if DOTNET2
        [ProducesResponseType(typeof(ICustomerAccountsResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetCustomerAccounts(string entityType, string entityId, [FromBody]CustomerDetailRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        var result = await Service.GetCustomerAccounts(entityType, entityId, request);
                        return result;
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }


        [HttpPost("{entitytype}/{entityid}/customer/account/transactions")]
#if DOTNET2
        [ProducesResponseType(typeof(ITransactionsResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetTransactionsByAccount(string entityType, string entityId, [FromBody]Request.TransactionRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        var result = await Service.GetTransactionsByAccount(entityType, entityId, request);
                        return result;

                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }



        [HttpPost("{entitytype}/{entityid}/customer/account/txpush/enable")]
#if DOTNET2
        [ProducesResponseType(typeof(ITxPushResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> EnableTxPush(string entityType, string entityId, [FromBody]TxPushRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        var result = await Service.EnableTxPush(entityType, entityId, request);
                        return result;
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        [HttpPost("{entitytype}/{entityId}/finicity/events/{eventName}")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> VerifyFinicityEvents(string entityType, string entityId, string eventName)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => Service.VerifyFinicityEvents(entityType, entityId, eventName)));
            });
        }

        [HttpPost("testtransaction/add/finicity")]
#if DOTNET2
        [ProducesResponseType(typeof(IAddTransactionDetailResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> AddFinicityTestTransaction([FromBody]AddTransactionDetailRequest transactionDetailRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        var result = await Service.AddFinicityTransaction(transactionDetailRequest);
                        return result;

                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }


        [HttpPost("{entitytype}/{entityid}/account/transactions/historic/load")]
#if DOTNET2
        [ProducesResponseType(typeof(Object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> LoadHistoricTransactions(string entityType, string entityId, [FromBody]CustomerDetailRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        var result = await Service.LoadHistoricTransactions(entityType, entityId, request);
                        return result;
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        [HttpPost("{entitytype}/{entityid}/finicity/process")]
#if DOTNET2
        [ProducesResponseType(typeof(ICustomerAccountsResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> ProcessRemaining(string entityType, string entityId, [FromBody]CustomerDetailRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        var result = await Service.ProcessRemaining(entityType, entityId, request);
                        return result;
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }
#endregion


#region Perfect Audit Methods

        [HttpPost("{entitytype}/{entityid}/createbook")]
#if DOTNET2
        [ProducesResponseType(typeof(IResponseConnection), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> CreateBook(string entitytype, string entityid, [FromBody]RequestConnection connection)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var response = await Service.CreateBook(entitytype, entityid, connection);
                    return Ok(response);

                }
                catch (Exception exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        [HttpGet("transactions/ocrolus/{bookPk}")]
#if DOTNET2
        [ProducesResponseType(typeof(Object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetPerfectAuditTransactions(string bookPk)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var response = await Service.GetPerfectAuditTransactionData(bookPk);
                    return Ok(response);
                }
                catch (Exception exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        [HttpPost("{entitytype}/{entityid}/uploadstatement/{bookPk}/multiple")]
#if DOTNET2
        [ProducesResponseType(typeof(CommonResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> UploadStatement(string entitytype, string entityid, List<IFormFile> files, string bookPk)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    if (files == null || files.Count <= 0)
                        throw new ArgumentException("Must upload at least one file.", nameof(files));

                    var filelist = new List<FileuploadData>();
                    foreach (var item in files)
                    {
                        var itemdetails = new FileuploadData();

                        itemdetails.FileName = ContentDispositionHeaderValue.Parse(item.ContentDisposition).FileName.Trim('"');
                        itemdetails.Filedetails = item.OpenReadStream().ReadAsBytes();
                        filelist.Add(itemdetails);
                    }

                    var response = await Service.UploadStatement(entitytype, entityid, filelist/*, filename*/, bookPk);
                    return Ok(response);
                }
                catch (Exception exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        [HttpPost("{entitytype}/{entityid}/uploadstatement/{bookPk}/single")]
#if DOTNET2
        [ProducesResponseType(typeof(CommonResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> UploadStatement(string entitytype, string entityid, string bookPk, IFormFile file)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    if (file == null || file.Length <= 0)
                        throw new ArgumentException("file content cannot be null or empty.", nameof(file));

                    var filename = ContentDispositionHeaderValue
                         .Parse(file.ContentDisposition)
                         .FileName
                         .Trim('"');


                    Logger.Info("executing signle UploadStatement in perfect audit syndication controller");
                    var response = await
                          Service.UploadStatement(entitytype, entityid,
                              file.OpenReadStream().ReadAsBytes(), filename, bookPk);


                    return Ok(response);
                }
                catch (Exception exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        #endregion

        #region Plaid Refresh

        [HttpGet("/{entityid}/plaidRefresh")]
        #if DOTNET2
        [ProducesResponseType(typeof(CommonResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        #endif
        public async Task<IActionResult> CreatingPublicToken(string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var response = await Service.PlaidRefreshLinking(entityId);
                    return Ok(response);
                }
                catch (Exception exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        #endregion


        [HttpGet]
        [Route("/{entityType}/{entityId}/accountdetails/{accountId}")]
#if DOTNET2
        [ProducesResponseType(typeof(ICashflowPayload), 200)]
        [ProducesResponseType(typeof(NoContentResult), 204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public Task<IActionResult> GetAccountDetails(string entityType, string entityId, string accountId)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the GetAccountDetails Method...");
                    var result = await Service.GetBankLinkAccountDetails(entityType, entityId, accountId);
                    if (result == null)
                    {
                        return NoContent;
                    }
                    return this.Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method GetAccountDetails Method raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

 [HttpPost]
        [Route("item/deletetoken")]
#if DOTNET2
        [ProducesResponseType(typeof(List<string>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif        
        public Task<IActionResult> DeleteToken()
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await Service.DeleteToken());
            });
        }

    }
}
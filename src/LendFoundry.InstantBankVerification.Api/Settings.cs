﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.InstantBankVerification.Api
{
    public class Settings
    {
        private const string Prefix = "IBV";

        public static string ServiceName => Prefix.ToLower();

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

        public static ServiceSettings PlaidV2ibv { get; } = new ServiceSettings($"{Prefix}_PLAIDV2IBV", "plaidv2ibv");
        public static ServiceSettings PerfectAudit { get; } = new ServiceSettings($"{Prefix}_PERFECTAUDIT", "perfectaudit");

        public static ServiceSettings DecisionEngine { get; } = new ServiceSettings($"{Prefix}_DECISIONENGINE", "decision-engine");

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", ServiceName);
        
        public static string Nats { get; } = Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats://nats:4222";

        public static ServiceSettings Finicity { get; } = new ServiceSettings($"{Prefix}_FINICITY", "finicity");

    }
}
﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.InstantBankVerification.Request;
#if DOTNET2
using LendFoundry.Syndication.PerfectAudit.Abstractions;
using LendFoundry.Syndication.PerfectAudit.Client;
#else
using LendFoundry.Syndication.PerfectAudit.Abstractions;
using LendFoundry.Syndication.PerfectAudit.Client;
#endif

namespace LendFoundry.InstantBankVerification.Client
{
    /// <summary>
    /// InstantBankVerificationClient
    /// </summary>
    public class InstantBankVerificationClient : IInstantBankVerificationService
    {
        public InstantBankVerificationClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<IResponseConnection> BankLinking(string entityType, string entityId, IRequestConnection requestData)
        {
            var request = new RestRequest($"/{entityType}/{entityId}/connect", Method.POST);
            request.AddJsonBody(requestData);
            return await Client.ExecuteAsync<ResponseConnection>(request);
        }

        public async Task<object> GetAccounts(string entityType, string entityId, IRequestConnection requestData)
        {
            var request = new RestRequest($"/{entityType}/{entityId}/getaccount", Method.POST);
            request.AddJsonBody(requestData);
            return await Client.ExecuteAsync<object>(request);
        }

        public async Task<bool> DeleteConnection(string entityType, string entityId, IRequestConnection requestData)
        {
            var request = new RestRequest($"/{entityType}/{entityId}/deleteconnction", Method.POST);
            request.AddJsonBody(requestData);
            return await Client.ExecuteAsync<bool>(request);
        }

        public async Task<bool> AddBank(string entityType, string entityId,  IRequestAddBank requestData)
        {
            var request = new RestRequest($"/{entityType}/{entityId}/deleteconnction", Method.POST);
            request.AddJsonBody(requestData);
            return await Client.ExecuteAsync<bool>(request);
        }

        public async Task<bool> DeleteAccount(string entityType, string entityId, IRequestAddBank requestData)
        {
            var request = new RestRequest($"/{entityType}/{entityId}/deleteconnction", Method.POST);
            request.AddJsonBody(requestData);
            return await Client.ExecuteAsync<bool>(request);
        }

        public async Task PullTransaction(IRequestTransaction request)
        {
            throw new NotImplementedException();
        }

        public async Task<object> AccountSync(string entityId, string entityType, IRequestConnection request)
        {
            throw new NotImplementedException();
        }
        public async Task<ITransactionCashflowResponse> GetTransactionForCashflow(string entityType, string entityId, string filter, string accountId)
        {
            var request = new RestRequest($"/{entityType}/{entityId}/transaction/{filter}/{accountId}", Method.GET);
            return await Client.ExecuteAsync<TransactionCashflowResponse>(request);
        }

        public async Task<List<IBankTransaction>> GetTransactions(string entityType, string entityId, string accountId, string fromDate, string toDate)
        {
            var request = new RestRequest($"/{entityType}/{entityId}/transaction/{accountId}/{fromDate}/{toDate}", Method.GET);
            var result = await Client.ExecuteAsync<List<BankTransaction>>(request);
            return result.ToList<IBankTransaction>();
        }

        public async Task<ITransactionCashflowResponse> GetTransactionForCashflow(string entityType, string entityId, string accountId, string fromDate, string toDate)
        {
            var request = new RestRequest($"/{entityType}/{entityId}/transaction/{accountId}/{fromDate}/{toDate}", Method.GET);
            return await Client.ExecuteAsync<TransactionCashflowResponse>(request);
        }

        public async Task<ICashflowPayload> GetCashflowPayload(string entityType, string entityId,  string accountId)
        {
            var request = new RestRequest($"/{entityType}/{entityId}/cashflowPayload/{accountId}", Method.GET);
            return await Client.ExecuteAsync<CashflowPayload>(request);
        }

        public async Task<object> UploadManualCsv(string entityType, string entityId, PostedFileDetails file)
        {
            throw new NotImplementedException();
        }

        public async Task AddManualDetails(string entityType, string entityId, IRequestManualBankLink request)
        {
            throw new NotImplementedException();
        }

        public async Task<IBankAccountWrapperResponse> GetAccount(string entityType, string entityId, string accountId)
        {
            var request = new RestRequest($"/{entityType}/{entityId}/account/{accountId}", Method.GET);
            return await Client.ExecuteAsync<BankAccountWrapperResponse>(request);
        }

        public async Task<bool> AddAccountPreference(string entityType, string entityId, IAccountPreferenceRequest accountPreference)
        {
            var request = new RestRequest($"/{entityType}/{entityId}/add/account/preference", Method.POST);
            request.AddJsonBody(accountPreference);
            return await Client.ExecuteAsync<bool>(request);
        }

        public async Task<IAccountTypeResponse> GetAccountByType(string entityType, string entityId, IAccountTypeRequest accountTypeRequest)
        {
            var request = new RestRequest($"/{entityType}/{entityId}/accountTypeSet", Method.POST);
            request.AddJsonBody(accountTypeRequest);
            return await Client.ExecuteAsync<AccountTypeResponse>(request);
        }

        public async Task AddBankLink(string bankLinkId, string entityType, string entityId)
        {
            var request = new RestRequest($"/{bankLinkId}/{entityType}/{entityId}/addBankLinks", Method.GET);
            await Client.ExecuteAsync(request);
        }
        

        public async Task<IBankAccount> GetBankAccountDetails(string entityType, string entityId, string accountId)
        {
            var request = new RestRequest($"/{entityType}/{entityId}/{accountId}", Method.GET);
            return await Client.ExecuteAsync<BankAccount>(request);
        }

        public async Task<IList<IBankAccount>> GetAllAccountDetails(string entityType, string entityId)
        {
            var request = new RestRequest($"/{entityType}/{entityId}/account/all", Method.GET);
            var result = await Client.ExecuteAsync<List<BankAccount>>(request);
            return result.ToList<IBankAccount>();
        }

        #region Filter Methods
        public async Task<IFilterBankLinkView> GetBankLinkFilterViewData(string entityId, string bankLinkId)
        {
            var request = new RestRequest($"/getbanklinkdata/{entityId}/{bankLinkId}", Method.GET);
            return await Client.ExecuteAsync<FilterBankLinkView>(request);
        }

        public async Task<List<IFilterAccountDataView>> GetAccoutFilterData(string bankLinkId)
        {
            var request = new RestRequest($"/getaccountdata/{bankLinkId}", Method.GET);
            var result = await Client.ExecuteAsync<List<FilterAccountDataView>>(request);
            return result.ToList<IFilterAccountDataView>();
        }

        public async Task<List<IBankTransaction>> GetTransactions(string accountId)
        {
            var request = new RestRequest($"transactions/{accountId}", Method.GET);
            var result = await Client.ExecuteAsync<List<BankTransaction>>(request);
            return result.ToList<IBankTransaction>();
        }

        #endregion
        
        public async Task<List<string>> GetAccountIds(string entityId, string bankLinkId)
        {
            var request = new RestRequest($"{entityId}/accountIds/{bankLinkId}", Method.GET);
            return await Client.ExecuteAsync<List<string>>(request);
        }
      

        public Task<IAccessTokenResponse> GetAccessToken(string entityType, string entityId)
        {
            throw new NotImplementedException();
        }

        public Task<IResponseConnection> AddCustomer(string entityType, string entityId, IAddCustomerRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<IConnectLinkResponse> GenerateConnectLink(string entityType, string entityId, IConnectLinkRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<ICustomerAccountsResponse> GetCustomerAccounts(string entityType, string entityId, ICustomerDetailRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<ITransactionsResponse> GetTransactionsByAccount(string entityType, string entityId, ITransactionRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<ITxPushResponse> EnableTxPush(string entityType, string entityId, ITxPushRequest txPushRequest)
        {
            throw new NotImplementedException();
        }

        public Task<bool> VerifyFinicityEvents(string entityType, string entityId, string eventName)
        {
            throw new NotImplementedException();
        }

        public Task<IAddTransactionDetailResponse> AddFinicityTransaction(IAddTransactionDetailRequest transactionDetailRequest)
        {
            throw new NotImplementedException();
        }

        public Task<dynamic> LoadHistoricTransactions(string entityType, string entityId, ICustomerDetailRequest customerDetailRequest)
        {
            throw new NotImplementedException();
        }

        public Task<IResponseConnection> CreateBook(string entityType, string entityId)
        {
            throw new NotImplementedException();
        }

        public Task<dynamic> GetPerfectAuditTransactionData(string bookPk)
        {
            throw new NotImplementedException();
        }

        public Task<bool> InBoundHookInvokedHandler(string entityType, string entityId, IEventRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<CommonResponse> UploadStatement(string entityType, string entityId, byte[] statement, string name, string bookPk)
        {
            throw new NotImplementedException();
        }

        public Task<IBankAccountResponse> GetBankAccountInformation(string entityType, string entityId, string accountId)
        {
            throw new NotImplementedException();
        }

        public Task UpdateBankAccount(string entityType, string entityId, IBankAccountRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<IResponseConnection> CreateBook(string entityType, string entityId, RequestConnection connection)
        {
            throw new NotImplementedException();
        }

        public Task<CommonResponse> UploadStatement(string entityType, string entityId, List<FileuploadData> statement, string bookPk)
        {
            throw new NotImplementedException();
        }

        public async Task UpdateRunningBalance()
        {
            var request = new RestRequest($"/updaterunningbalance", Method.POST);
           await Client.ExecuteAsync<List<BankAccount>>(request);
            
        }         

        public Task<ICustomerAccountsResponse> ProcessRemaining(string entityType, string entityId, ICustomerDetailRequest customerDetailRequest)
        {
            throw new NotImplementedException();
        }

        public Task<IPlaidRefreshResponse> PlaidRefreshLinking(string entityId)
        {
            throw new NotImplementedException();
        }

        public Task<IBankAccount> GetBankLinkAccountDetails(string entityType, string entityId, string accountId)
        {
            throw new NotImplementedException();
        }

        public async Task<List<string>> DeleteToken()
        {
             var request = new RestRequest("item/deletetoken", Method.POST);            
             var result = await Client.ExecuteAsync<List<string>>(request);
            return result.ToList<string>();
        }
    }
}

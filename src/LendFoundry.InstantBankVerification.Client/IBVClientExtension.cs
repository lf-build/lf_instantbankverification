﻿using LendFoundry.Security.Tokens;


#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.InstantBankVerification.Client
{
    public static class IBVClientExtension
    {
        /// <summary>
        /// AddIBVService
        /// </summary>
        /// <param name="services"></param>
        /// <param name="endpoint"></param>
        /// <param name="port"></param>
        /// <returns></returns>
        public static IServiceCollection AddIBVService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IInstantBankVerificationClientFactory>(p => new InstantBankVerificationClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IInstantBankVerificationClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}

﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using LendFoundry.Foundation.Logging;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.InstantBankVerification.Client
{
    public class InstantBankVerificationClientFactory : IInstantBankVerificationClientFactory
    {
        public InstantBankVerificationClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public InstantBankVerificationClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }
        private Uri Uri { get; }

        //public IInstantBankVerificationService Create(ITokenReader reader)
        //{
        //    var client = Provider.GetServiceClient(reader, Endpoint, Port);
        //    return new InstantBankVerificationClient(client);
        //}

        public IInstantBankVerificationService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("ibv");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new InstantBankVerificationClient(client);
        }
    }
}

﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification.Persistence
{
    public class ClientTrackRepository : MongoRepository<IClientTrack, ClientTrack>, IClientTrackRepository
    {
        /// <summary>
        /// Maps the collection properties.
        /// </summary>
        static ClientTrackRepository()
        {
            BsonClassMap.RegisterClassMap<ClientTrack>(map =>
            {
                map.AutoMap();
                map.MapProperty(item => item.EntityId).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.EntityType).SetIgnoreIfDefault(true);
                //map.MapProperty(item => item.Institutions).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.LinkRequestId).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.LinkSessionId).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.CreatedDate).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.PlaidApiRequestId).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.Status).SetIgnoreIfDefault(true);
                var type = typeof(ClientTrack);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemRepository"/> class.
        /// </summary>
        /// <param name="tenantService">tenantService</param>
        /// <param name="configuration">configuration</param>
        public ClientTrackRepository(ITenantService tenantService, IMongoConfiguration configuration) : 
                                    base(tenantService, configuration, "client-track")
        {
            //CreateIndexIfNotExists("tenant_id", Builders<IClientTrack>.IndexKeys
            //    .Ascending(item => item.TenantId), false);            

            //CreateIndexIfNotExists("EntityId", Builders<IClientTrack>.IndexKeys
            //    .Ascending(item => item.TenantId)
            //    .Ascending(item => item.EntityId), false);
        }


        /// <summary>
        /// Add a new item
        /// </summary>
        /// <param name="item">item</param>
        /// <returns>string</returns>
        public async Task<string> AddItem(IClientTrack item)
        {
            //// Update TenantId 
            item.TenantId = TenantService.Current.Id;

            //// Store item in database 
            await Collection
                .InsertOneAsync(item);

            return "ClientTrack added successfully.";
        }
    }
}

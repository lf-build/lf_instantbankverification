﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification.Persistence
{
    public class BankTransactionRepository : MongoRepository<IBankTransaction, BankTransaction>, IBankTransactionRepository
    {
        static BankTransactionRepository()
        {
            BsonClassMap.RegisterClassMap<BankTransaction>(map =>
            {
                map.AutoMap();
                var type = typeof(BankTransaction);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public BankTransactionRepository(ITenantService tenantService, IMongoConfiguration configuration) :
                base(tenantService, configuration, "banktransaction")
        {
            CreateIndexIfNotExists("id", Builders<IBankTransaction>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Id));

            CreateIndexIfNotExists("unique_key", Builders<IBankTransaction>.IndexKeys
                .Ascending(transaction => transaction.TenantId)
                .Ascending(transaction => transaction.ProviderAccountId)
                .Ascending(transaction => transaction.TransactionId)
                .Ascending(transaction => transaction.ProviderBankLinkId), true);
        }

        /// <summary>
        /// AddTransaction
        /// </summary>
        /// <param name="transaction">transaction</param>
        /// <returns>string</returns>
        public async Task<bool> AddTransaction(IBankTransaction transaction)
        {
            // Check if data exists
            var objTransaction = (await Collection.FindAsync(x => x.TransactionId == transaction.TransactionId &&  x.ProviderBankLinkId == transaction.ProviderBankLinkId)).FirstOrDefault();

            if (objTransaction == null)
            {
                transaction.TenantId = TenantService.Current.Id;
                await Collection.InsertOneAsync(transaction);
                return true;
            }

            return false;
        }

        /// <summary>
        /// AddTransactions
        /// </summary>
        /// <param name="transactions">transactions</param>
        /// <returns>string</returns>
        public async Task<bool> AddTransactions(IEnumerable<IBankTransaction> transactions)
        {
            try
            {
                foreach (var transaction in transactions)
                {
                    transaction.TenantId = TenantService.Current.Id;
                }

                if (transactions.Any())
                {
                    await Collection.InsertManyAsync(transactions);
                    return true;
                }

                return false;
            }
            catch (MongoBulkWriteException)
            {
                foreach (var transaction in transactions)
                {
                    await AddTransaction(transaction);
                }
            }

            return true;
        }

        /// <summary>
        /// GetTransactions
        /// </summary>
        /// <param name="bankLinkId"></param>
        /// <returns></returns>
        public async Task<IList<IBankTransaction>> GetTransactions(string accountId)
        {
            return (await Collection.FindAsync(x => x.TenantId == TenantService.Current.Id && x.ProviderAccountId == accountId)).ToList();
        }


        /// <summary>
        /// Get list of TransactionId given ProviderBankLinkId aka ItemId in Plaid
        /// </summary>
        /// <param name="providerLinkId">provider link id</param>
        /// <returns>List of TransactionId</returns>
        public async Task<IList<string>> GetTransactionIdsByProviderBankLinkId(string providerLinkId)
        {
            return
                 await Task.FromResult(
                    Query.Where(x => x.TenantId == TenantService.Current.Id && x.ProviderBankLinkId == providerLinkId)
                        .Select(t => t.TransactionId).ToList<string>());
        }

        /// <summary>
        /// Get the First Transaction RunningBalance
        /// </summary>
        /// <param name="accountId">AccountId</param>
        /// <returns></returns>
        public async Task<double?> GetFirstTransactionRunningBalance(string accountId)
        {
            var result = await Task.FromResult(
                    Query.Where(x => x.TenantId == TenantService.Current.Id && x.ProviderAccountId == accountId)
                    .OrderByDescending(o=>o.TransactionDate)
                    .OrderByDescending(o=>o.Id)                    
                    .FirstOrDefault());
            if (result != null && result.RunningBalance!= null)
                return result.RunningBalance - result.Amount;
            else
                return null;
        }

        public async Task<List<IBankTransaction>> GetTransactions(string entityType, string entityId, string accountId, DateTime fromDate, DateTime toDate)
        {
            //checked for x.TransactionDate < toDate as we have added 1 day in todate in service method for taking last day transaction when time is greater than 00:00
            //duet to date issue when date is having time 01:00:00.000Z or greater
            return (
                await Collection.FindAsync(x => 
                    x.TenantId == TenantService.Current.Id && x.ProviderAccountId == accountId
                    && x.TransactionDate >= fromDate && x.TransactionDate < toDate)
            ).ToList();
        }

        public async Task<long> GetTransactionCount(string providerBankLinkId)
        {
            return (await Collection.CountAsync(x => x.TenantId == TenantService.Current.Id && x.ProviderBankLinkId == providerBankLinkId));
        }

        public async Task<IList<IBankTransaction>> GetFilterTransactionData(string accountId , DateTime serverDate, int filterInMonths)
        {
            var builder = Builders<IBankTransaction>.Filter;
            var filter = builder.Where(transaction => transaction.ProviderAccountId == accountId);
            filter = filter & builder.Gte("TransactionDate", serverDate.AddMonths(-filterInMonths));
            filter = filter & builder.Lte("TransactionDate", serverDate);

            return  (await Collection.FindAsync(filter)).ToList();
        }

        public async Task<bool> UpdateTransaction(IBankTransaction item)
        {
            return (await Collection.UpdateOneAsync
                    (
                        Builders<IBankTransaction>.Filter
                            .Where(x => x.TenantId == TenantService.Current.Id &&
                                        x.TransactionId == item.TransactionId && x.Id == item.Id),
                        Builders<IBankTransaction>.Update.Set(x => x.RunningBalance, item.RunningBalance)
                    )).IsAcknowledged;
            
        }

    }
}

﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification.Persistence
{
    public class BankAccountHistoryRepository : MongoRepository<IBankAccount, BankAccount>, IBankAccountHistoryRepository
    {
        static BankAccountHistoryRepository()
        {
            
        }

        public BankAccountHistoryRepository(ITenantService tenantService, IMongoConfiguration configuration) :
                base(tenantService, configuration, "log")
        {
            CreateIndexIfNotExists("uniqueId", Builders<IBankAccount>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Id));
        }

        /// <summary>
        /// AddAccount
        /// </summary>
        /// <param name="account">account</param>
        /// <returns>string</returns>
        public async Task AddAccount(IBankAccount account)
        {
            account.TenantId = TenantService.Current.Id;
            await Collection.InsertOneAsync(account);
        }

        /// <summary>
        /// GetAccounts
        /// </summary>
        /// <param name="bankLinkId"></param>
        /// <returns></returns>
        public async Task<IList<IBankAccount>> GetAccounts(string bankLinkId)
        {
            return (await Collection.FindAsync(x => x.BankLinkId == bankLinkId && x.IsActive == true && x.IsDeleted == false)).ToList();
        }

        /// <summary>
        /// GetAccounts
        /// </summary>
        /// <param name="bankLinkId"></param>
        /// <returns></returns>
        public async Task<IBankAccount> GetAccount(string bankLinkId, string accountNumber)
        {
            return (await Collection.FindAsync(x => x.BankLinkId == bankLinkId && x.AccountNumber == accountNumber && x.IsActive == true && x.IsDeleted == false)).FirstOrDefault();
        }

        public async Task<bool> UpdateBankAccount(string bankLinkId)
        {
            return (await Collection.UpdateManyAsync(
                   Builders<IBankAccount>.Filter
                       .Where(account => account.BankLinkId == bankLinkId),
                    Builders<IBankAccount>.Update
                       .Set(objAccount => objAccount.IsDeleted, true)
                       .Set(objAccount => objAccount.UpdatedDate, new TimeBucket())
               )).IsAcknowledged;
        }

        public async Task<bool> UpdateBankAccount(string bankLinkId, string accountNumber)
        {
            return (await Collection.UpdateManyAsync(
                   Builders<IBankAccount>.Filter
                       .Where(account => account.BankLinkId == bankLinkId && account.AccountNumber == accountNumber),
                    Builders<IBankAccount>.Update
                       .Set(objAccount => objAccount.IsDeleted, true)
                       .Set(objAccount => objAccount.UpdatedDate, new TimeBucket())
               )).IsAcknowledged;
        }

        /// <summary>
        /// AccountExists
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public async Task<bool> AccountExists(string accountId)
        {
            var accountData = (await Collection.FindAsync(x => x.TenantId == TenantService.Current.Id && x.ProviderAccountId == accountId
                                                          && x.IsActive == true && x.IsDeleted == false)).FirstOrDefault();

            if (accountData != null)
                return true;
            else
                return false;
        }

        /// <summary>
        /// UpdateAccount
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAccount(IBankAccount account)
        {
            var result = (await Collection
                       .UpdateOneAsync(Builders<IBankAccount>
                                   .Filter
                                   .Where(objItem => objItem.TenantId.Equals(TenantService.Current.Id)
                                           && objItem.ProviderAccountId == account.ProviderAccountId),
                                  Builders<IBankAccount>
                                   .Update
                                   .Set(objItem => objItem.CurrentBalance, account.CurrentBalance)
                                   .Set(objItem => objItem.AvailableBalance, account.AvailableBalance)
                                   .Set(objItem => objItem.UpdatedDate, account.UpdatedDate)
                                  )).IsAcknowledged;

            return result;
           
        }
    }
}

﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification.Persistence
{
    public class BalanceRepository : MongoRepository<IRunningBalanceDetails, RunningBalanceDetails>, IBalanceRepository
    {
        static BalanceRepository()
        {
            BsonClassMap.RegisterClassMap<RunningBalanceDetails>(map =>
            {
                map.AutoMap();
                var type = typeof(RunningBalanceDetails);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

           
        }

        public BalanceRepository(ITenantService tenantService, IMongoConfiguration configuration) :
                base(tenantService, configuration, "RunningBalanceDetails")
        {
            CreateIndexIfNotExists("id", Builders<IRunningBalanceDetails>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Id));
            
        }

        /// <summary>
        /// GetBankLinkId
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<IList<IRunningBalanceDetails>> GetProviderAccountIds()
        {
            return (await Collection.FindAsync(x => x.IsProcessed == false)).ToList();
        }

        /// <summary>
        /// GetBankLinkIdByEntityId
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAccountDetails(string Id)
        {
            return (await Collection.UpdateManyAsync(
                   Builders<IRunningBalanceDetails>.Filter
                       .Where(account => account.ProviderAccountId == Id),
                    Builders<IRunningBalanceDetails>.Update
                       .Set(objAccount => objAccount.IsProcessed, true)
                       .Set(objAccount => objAccount.UpdatedDate, new TimeBucket().Time)
               )).IsAcknowledged;
        }


        /// <summary>
        /// AddTransaction
        /// </summary>
        /// <param name="transaction">transaction</param>
        /// <returns>string</returns>
        public async Task<bool> AddAccountDetail(IRunningBalanceDetails account)
        {

                account.TenantId = TenantService.Current.Id;
                await Collection.InsertOneAsync(account);
                return true;
            
        }


        /// <summary>
        /// AddAccountDetails
        /// </summary>
        /// <param name="accounts">AddAccountDetailsaccounts</param>
        /// <returns>string</returns>
        public async Task<bool> AddAccountDetails(IEnumerable<IRunningBalanceDetails> accounts)
        {
            try
            {
                foreach (var account in accounts)
                {
                    account.TenantId = TenantService.Current.Id;
                }

                if (accounts.Any())
                {
                    await Collection.InsertManyAsync(accounts);
                    return true;
                }

                return false;
            }
            catch (MongoBulkWriteException)
            {
                foreach (var transaction in accounts)
                {
                    await AddAccountDetail(transaction);
                }
            }

            return true;
        }
    }
}

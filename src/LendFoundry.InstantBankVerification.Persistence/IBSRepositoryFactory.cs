﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Encryption;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

using System;

namespace LendFoundry.InstantBankVerification.Persistence
{
    public class IBSRepositoryFactory  : IIBSRepositoryFactory
    //: IDrawDownRepositoryFactory
    {
        public IBSRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IBankLinkRepository CreateBankLinkRepository(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new BankLinkRepository(tenantService, mongoConfiguration);
        }

        public IBankAccountRepository CreateBankAccountRepository(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            var encryptionService = Provider.GetService<IEncryptionService>();
            return new BankAccountRepository(tenantService, mongoConfiguration, encryptionService);
        }

        public IBankAccountHistoryRepository CreateBankAccountHistoryRepository(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new BankAccountHistoryRepository(tenantService, mongoConfiguration);
        }

        public IBankTransactionRepository CreateBankTransactionRepository(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new BankTransactionRepository(tenantService, mongoConfiguration);
        }

        public IClientTrackRepository CreateClientTrackRepository(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new ClientTrackRepository(tenantService, mongoConfiguration);
        }

        public IBalanceRepository CreateBalanceRepository(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new BalanceRepository(tenantService, mongoConfiguration);
        }

    }
}
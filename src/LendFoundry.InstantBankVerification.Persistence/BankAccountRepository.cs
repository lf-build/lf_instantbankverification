﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Encryption;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification.Persistence
{
    public class BankAccountRepository : MongoRepository<IBankAccount, BankAccount>, IBankAccountRepository
    {
        static BankAccountRepository()
        {
        }

        public BankAccountRepository(ITenantService tenantService, IMongoConfiguration configuration, IEncryptionService encrypterService) :
                base(tenantService, configuration, "bankaccount")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(BankAccount)))
            {
                BsonClassMap.RegisterClassMap<BankAccount>(map =>
                {
                    map.AutoMap();
                    map.MapMember(m => m.AccountNumber).SetSerializer(new BsonEncryptor<string, string>(encrypterService));
                    var type = typeof(BankAccount);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
            }

            CreateIndexIfNotExists("id", Builders<IBankAccount>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Id));
        }

        /// <summary>
        /// AddAccount
        /// </summary>
        /// <param name="account">account</param>
        /// <returns>string</returns>
        public async Task AddAccount(IBankAccount account)
        {
            account.TenantId = TenantService.Current.Id;
            await Collection.InsertOneAsync(account);
        }

        /// <summary>
        /// GetAccounts
        /// </summary>
        /// <param name="bankLinkId"></param>
        /// <returns></returns>
        public async Task<IList<IBankAccount>> GetAccounts(string bankLinkId)
        {
            return (await Collection.FindAsync(x => x.BankLinkId == bankLinkId && x.IsActive == true && x.IsDeleted == false)).ToList();
        }

        /// <summary>
        /// GetAccounts
        /// </summary>
        /// <param name="bankLinkId"></param>
        /// <returns></returns>
        public async Task<IBankAccount> GetAccount(string bankLinkId, string accountNumber)
        {
            return (await Collection.FindAsync(x => x.BankLinkId == bankLinkId && x.AccountNumber == accountNumber && x.IsActive == true && x.IsDeleted == false)).FirstOrDefault();
        }

        /// <summary>
        /// GetAccounts
        /// </summary>
        /// <param name="bankLinkId"></param>
        /// <returns></returns>
        public async Task<IBankAccount> GetAccount(string bankLinkId)
        {
            return (await Collection.FindAsync(x => x.BankLinkId == bankLinkId && x.IsActive == true && x.IsDeleted == false)).FirstOrDefault();
        }

        public async Task<bool> UpdateBankAccount(string bankLinkId)
        {
            return (await Collection.UpdateManyAsync(
                   Builders<IBankAccount>.Filter
                       .Where(account => account.BankLinkId == bankLinkId),
                    Builders<IBankAccount>.Update
                       .Set(objAccount => objAccount.IsDeleted, true)
                       .Set(objAccount => objAccount.UpdatedDate, new TimeBucket())
               )).IsAcknowledged;
        }

        public async Task<bool> UpdateBankAccount(string bankLinkId, string accountNumber)
        {
            return (await Collection.UpdateManyAsync(
                   Builders<IBankAccount>.Filter
                       .Where(account => account.BankLinkId == bankLinkId && account.AccountNumber == accountNumber),
                    Builders<IBankAccount>.Update
                       .Set(objAccount => objAccount.IsDeleted, true)
                       .Set(objAccount => objAccount.UpdatedDate, new TimeBucket())
               )).IsAcknowledged;
        }

        /// <summary>
        /// AccountExists
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public async Task<bool> AccountExists(string accountId)
        {
            var accountData = (await Collection.FindAsync(x => x.TenantId == TenantService.Current.Id && x.ProviderAccountId == accountId
                                                          && x.IsActive == true && x.IsDeleted == false)).FirstOrDefault();

            if (accountData != null)
                return true;
            else
                return false;
        }

        public async Task<IBankAccount> GetAccountByProviderAccountId(string accountId)
        {
            var accountData = (await Collection.FindAsync(x => x.TenantId == TenantService.Current.Id && x.ProviderAccountId == accountId
                                                          && x.IsActive == true && x.IsDeleted == false)).FirstOrDefault();

            if (accountData != null)
                return accountData;
            else
                return null;
        }


        /// <summary>
        /// UpdateAccount
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAccount(IBankAccount account)
        {
            var result = (await Collection
                       .UpdateOneAsync(Builders<IBankAccount>
                                   .Filter
                                   .Where(objItem => objItem.TenantId.Equals(TenantService.Current.Id)
                                           && objItem.ProviderAccountId == account.ProviderAccountId),
                                  Builders<IBankAccount>
                                   .Update
                                   .Set(objItem => objItem.CurrentBalance, account.CurrentBalance)
                                   .Set(objItem => objItem.AvailableBalance, account.AvailableBalance)
                                   .Set(objItem => objItem.UpdatedDate, account.UpdatedDate)
                                  )).IsAcknowledged;

            return result;
        }

        /// <summary>
        /// GetAccounts
        /// </summary>
        /// <param name="bankLinkId"></param>
        /// <returns></returns>
        public async Task<List<string>> GetAccountIds(string bankLinkId)
        {
           var list = (await Collection.FindAsync(item => item.BankLinkId.Equals(bankLinkId) && item.IsActive == true && item.IsDeleted == false))
                         .ToList<IBankAccount>().SelectMany(x=> new List<string>()
                         {
                             x.ProviderAccountId
                         }).ToList();

            return list;
        }

        /// <summary>
        /// AddBankAccount
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<string> AddBankAccount(IBankAccount request)
        {
            if (!string.IsNullOrEmpty(request.Id))
            {
                var account = await Get(request.Id);
                if (account != null)
                {
                    account.RoutingNumber = request.RoutingNumber;
                    account.AccountNumber = request.AccountNumber;
                    account.AccountType = request.AccountType;
                    account.AvailableBalance = request.AvailableBalance;
                    account.CurrentBalance = request.CurrentBalance;
                    //account.UpdatedOn = request.UpdatedOn;
                    //account.UpdatedBy = request.UpdatedBy;
                    //account.BalanceAsOfDate = request.BalanceAsOfDate;
                    //account.NameOnAccount = request.NameOnAccount;
                    //account.OfficialAccountName = request.OfficialAccountName;
                    Update(account);
                }
            }
            else
            {
                Add(request);
                return request.Id;
            }
            return request.Id;
        }

        /// <summary>
        /// ResetAccountPreference
        /// </summary>
        /// <param name="bankLinkId"></param>
        /// <param name="accountType"></param>
        /// <returns></returns>
        public async Task<bool> ResetAccountPreference(List<string> bankLinkIds, AccountType accountType)
        {
            var records = (await Collection.FindAsync(item => bankLinkIds.Contains(item.BankLinkId) && item.IsActive == true && item.IsDeleted == false 
                                            && item.TenantId == TenantService.Current.Id)).ToList();

            if (records == null || !records.Any())
                throw new NotFoundException($"Record with bankLinkId : {string.Join(",", bankLinkIds)} is not found");

            if (accountType == AccountType.Cashflow)
            {
                foreach (var account in records)
                {
                    account.IsCashflowAccount = false;
                    account.CashflowAccountVersion = null;
                    Update(account);
                }
            }
            else if (accountType == AccountType.Funding)
            {
                foreach (var account in records)
                {
                    account.IsFundingAccount = false;
                    account.FundingAccountVersion = null;
                    Update(account);
                }
            }
            return true;
        }

        /// <summary>
        /// UpdateAccountPreference
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAccountPreference(IAccountPreference request, int? accountVersion)
        {
            var account = (await Collection.FindAsync(item => item.ProviderAccountId == request.AccountID && item.IsActive == true && item.IsDeleted == false
                                            && item.TenantId == TenantService.Current.Id)).FirstOrDefault();

            if (account == null)
            {
                return false;
            }

            if (request.IsCashflowAccount.HasValue)
            {
                account.IsCashflowAccount = request.IsCashflowAccount.Value;
                account.CashflowAccountVersion = accountVersion;
            }
            if (request.IsFundingAccount.HasValue)
            {
                account.IsFundingAccount = request.IsFundingAccount.Value;
                account.FundingAccountVersion = accountVersion;
            }

            Update(account);

            return true;
        }

        public async Task<IBankAccount> GetAccountDetails(List<string> bankLinkIds, string accontId)
        {
            var account = (await Collection.FindAsync(item => bankLinkIds.Contains(item.BankLinkId)  && item.ProviderAccountId == accontId && item.IsActive == true && item.IsDeleted == false
                                            && item.TenantId == TenantService.Current.Id)).FirstOrDefault();

            return account;
        }

        public async Task<IList<IBankAccount>> GetAccountsByEntityId(List<string> bankLinkIds)
        {
            var account = (await Collection.FindAsync(item => bankLinkIds.Contains(item.BankLinkId) && item.IsActive == true && item.IsDeleted == false))
                            .ToList();

            return account;
        }


        public async Task<IBankAccount> GetCashflowAccount(List<string> bankLinkIds)
        {
            var accounts = (await Collection.FindAsync(item => bankLinkIds.Contains(item.BankLinkId) && item.IsActive == true && item.IsDeleted == false
                                                       && item.IsCashflowAccount == true)).ToList();
            if (accounts == null || !accounts.Any())
                throw new NotFoundException($"accounts with bankLinkIds {string.Join(",", bankLinkIds)} is not found");

            return accounts.FirstOrDefault();
        }
        public async Task<IBankAccount> GetFundingAccount(List<string> bankLinkIds)
        {
            var accounts = (await Collection.FindAsync(item => bankLinkIds.Contains(item.BankLinkId) && item.IsActive == true && item.IsDeleted == false
                                                       && item.IsFundingAccount == true)).ToList();
            if (accounts == null || !accounts.Any())
                throw new NotFoundException($"accounts with bankLinkIds {string.Join(",", bankLinkIds)} is not found");

            return accounts.FirstOrDefault();
        }
    }
}

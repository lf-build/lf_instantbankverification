﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
using Microsoft.AspNet.Builder;
#endif

namespace LendFoundry.InstantBankVerification
{
    public static class PlaidListenerExtensions
    {
        public static void UsePlaidListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IPlaidListener>().Start();
        }
    }
}

﻿using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Reflection;

namespace LendFoundry.InstantBankVerification
{
    public class PlaidListener : IPlaidListener
    {
        public PlaidListener
        (
            IConfigurationServiceFactory<Configuration> apiConfigurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IPlaidUniteServiceFactory plaidUniteServiceFactory,
            IFinicityServiceFactory finicityServiceFactory,
            IOcrolusPerfectAuditServiceFactory ocrolusServiceFactory
        )
        {

            this.EventHubFactory = eventHubFactory;
            this.ApiConfigurationFactory = apiConfigurationFactory;
            this.TokenHandler = tokenHandler;
            this.TenantServiceFactory = tenantServiceFactory;
            this.Logger = loggerFactory.Create(NullLogContext.Instance);
            this.PlaidUniteServiceFactory = plaidUniteServiceFactory;
            this.FinicityServiceFactory = finicityServiceFactory;
            this.OcrolusServiceFactory = ocrolusServiceFactory;
            Logger.Debug($"Plaid Listner constructor");
        }

        #region Private Variables
        private IConfigurationServiceFactory<Configuration> ApiConfigurationFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private ILogger Logger { get; }

        private IPlaidUniteServiceFactory PlaidUniteServiceFactory { get; }

        private IFinicityServiceFactory FinicityServiceFactory { get; }

        private IOcrolusPerfectAuditServiceFactory OcrolusServiceFactory { get; }
        #endregion

        public void Start()
        {
            Logger.Debug($"Eventhub listener started");
            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();
                Logger.Info($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach(tenant =>
                {
                    Logger.Info($"Initializing event hub for teanant #{tenant.Id}");
                    // Tenant token creation         
                    var token = TokenHandler.Issue(tenant.Id, "PlaidIssuer");
                    var reader = new StaticTokenReader(token.Value);

                    // Needed resources for this operation
                    var eventHub = EventHubFactory.Create(reader);
                    var configuration = ApiConfigurationFactory.Create(reader).Get();
                    if (configuration == null)
                        throw new ArgumentException("Api configuration cannot be found, please check");

                    var plaidUniteService = PlaidUniteServiceFactory.Create(reader, TokenHandler, Logger);
                    var finicityService = FinicityServiceFactory.Create(reader, TokenHandler, Logger);
                    var ocrolusService = OcrolusServiceFactory.Create(reader, TokenHandler, Logger);
                    
                    // Attach all configured events to be listen
                    configuration
                        .Events
                        .ToList()
                        .ForEach(eventConfig =>
                        {
                            Logger.Info("----------------------------------------");
                            Logger.Info($"New subscription attached to tenant #{tenant.Id} event #{eventConfig.Name}");
                            Logger.Info("----------------------------------------");
                            eventHub.On(eventConfig.Name, ProcessEvent(eventConfig, this.Logger, plaidUniteService, finicityService, ocrolusService));
                        });
                    eventHub.StartAsync();
                });
            }
            catch (Exception ex)
            {
                Logger.Error("Error while listening eventhub", ex);
                Start();
            }
        }

        #region Static Methods
        private static Action<EventHub.EventInfo> ProcessEvent(
            EventMapping eventConfiguration, 
            ILogger logger, 
            IPlaidUniteService plaidUniteService,
            IFinicityService finicityService,
            IOcrolusPerfectAuditService ocrolusService)
        {
            return async @event =>
            {
                try
                {
                    logger.Info("UpdateStatus");
                    var eventName = eventConfiguration.EventName.FormatWith(@event);
                    logger.Info($"eventName : {eventName}");

                    if (eventName == "plaidnotification")
                    {
                        var webhookType = eventConfiguration.WebhookType.FormatWith(@event);
                        var webhookCode = eventConfiguration.WebhookCode.FormatWith(@event);
                        var plaidresponse = eventConfiguration.Response.FormatWith(@event);
                        var plaiddata = JsonConvert.DeserializeObject<dynamic>(plaidresponse);
                        var itemId = eventConfiguration.ItemId.FormatWith(@event);

                        logger.Info($"webhookType: {webhookType}; webhookCode: { webhookCode}; itemId: {itemId}");

                        if (webhookType == WebhookType.TRANSACTIONS.ToString())
                        {
                            var newTransactions = eventConfiguration.NewTransactions.FormatWith(@event);
                            logger.Info($"newTransactions: {newTransactions}");
                            IRequestTransaction request = new RequestTransaction
                            {
                                ItemId = itemId,
                                TransactionCount = Convert.ToInt32(newTransactions),
                                WebhookCode = webhookCode
                            };

                            await plaidUniteService.PullTransaction(request);
                        }
                        else if(webhookType == WebhookType.ITEM.ToString() &&  webhookCode == "ERROR")
                        {
                            logger.Info($"Error code found in webhook");
                            //IRequestTransaction request = new RequestTransaction
                            //{
                            //    ItemId = itemId,
                            //    TransactionCount = 0,
                            //    WebhookCode = webhookCode
                            //};
                            var plaideventRequest = new EventRequest();
                            plaideventRequest.data = plaiddata;
                            plaideventRequest.DERuleName = eventConfiguration.PlaidDERuleName;

                            await plaidUniteService.PlaidErrorHandling(itemId, plaideventRequest);
                        }
                    }
                    else if (eventName == "finicity")
                    {
                        object objRequest = null;
                        object objResponse = null;

                        try
                        {
                            var responseData = eventConfiguration.Response.FormatWith(@event);
                            objResponse = JsonConvert.DeserializeObject<dynamic>(responseData);
                        }
                        catch
                        {
                            // ignored
                        }
                        try
                        {
                            var requestData = eventConfiguration.Request.FormatWith(@event);
                            objRequest = JsonConvert.DeserializeObject<dynamic>(requestData);
                        }
                        catch
                        {
                            // ignored
                        }

                        object eventData = new { Request = objRequest, Response = objResponse };

                        var eventRequest = new EventRequest();

                        var entityId = eventConfiguration.EntityId.FormatWith(@event);
                        var entityType = eventConfiguration.EntityType.FormatWith(@event);
                        var name = eventConfiguration.Name.FormatWith(@event);

                        eventRequest.DERuleName = eventConfiguration.FinicityDERuleName.FormatWith(@event);
                        // eventRequest.InitiateCashflow = eventConfiguration.InitiateCashflow;
                        eventRequest.data = eventData;


                        var methodName = eventConfiguration.FinicityMethodToExecute;
                        MethodInfo method = typeof(IFinicityService).GetMethod(methodName);
                        object[] param = { entityType, entityId, eventRequest };
                        if (method != null)
                        {
                            var result = await(dynamic)method.Invoke(finicityService, param);
                            if (!string.IsNullOrEmpty(eventConfiguration.FinicityCompletionEventName))
                            {
                                //TODO Check if need to raise completion event
                            }
                        }

                        logger.Info($"Entity Id: #{entityId}");
                        logger.Info($"Response Data : #{eventData}");
                        logger.Info($"event {name} completed for {entityId} ");

                    }
                    else if (eventName == "perfectaudit")
                    {
                        object objResponse = null;

                        try
                        {
                            var entityId = eventConfiguration.EntityId.FormatWith(@event);
                            var entityType = eventConfiguration.EntityType.FormatWith(@event);
                            var name = eventConfiguration.Name.FormatWith(@event);

                            var responseData = eventConfiguration.Response.FormatWith(@event);
                            objResponse = JsonConvert.DeserializeObject<dynamic>(responseData).body;

                            var response = JsonConvert.DeserializeObject<Model.OcrolusWebHookResponse>(objResponse.ToString());

                            if (response.status == "VERIFICATION_COMPLETE")
                            {
                                await ocrolusService.GetTransactions(response.book_pk);

                            }
                            

                            logger.Info($"Entity Id: #{entityId}");
                            logger.Info($"Response Data : #{responseData}");
                            logger.Info($"event {name} completed for {eventName} ");
                        }
                        catch
                        {
                            // ignored
                        }
                       
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                }
            };
        }
        #endregion

        #region Ensure Parameter
        private static void EnsureParameter(string name, object value)
        {
            if (value == null) throw new ArgumentNullException(nameof(value));
        }
        #endregion
    }
}

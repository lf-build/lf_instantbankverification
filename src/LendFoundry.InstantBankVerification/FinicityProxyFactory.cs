﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.Syndication.PlaidIBV;
using LendFoundry.Foundation.Date;
using LendFoundry.Configuration;
using LendFoundry.Syndication.PlaidIBV.Client;
using LendFoundry.EventHub;
using LendFoundry.Clients.DecisionEngine;

namespace LendFoundry.InstantBankVerification
{
    public class FinicityProxyFactory : IFinicityProxyFactory
    {

        #region Public Constructor
        public FinicityProxyFactory(IServiceProvider provider) { Provider = provider; }
        #endregion
        private IServiceProvider Provider { get; }



        public IFinicityProxy Create(ITokenReader reader, ILogger logger)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var configurationService = configurationServiceFactory.Create<Configuration>(Settings.ServiceName, reader);
            var configuration = configurationService.Get();
            return new FinicityProxy(configuration, logger);
        }




    }
}

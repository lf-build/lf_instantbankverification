﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.InstantBankVerification.Model;
using Newtonsoft.Json;
using System.IO;

namespace LendFoundry.InstantBankVerification
{
    public class FinicityService : IFinicityService
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FinicityService"/> class.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="bankLinkRepository"></param>
        /// <param name="bankAccountRepository"></param>
        /// <param name="bankTransactionRepository"></param>
        /// <param name="PlaidClient"></param>
        public FinicityService(ILogger logger,
            IConfiguration configuration,
            IBankLinkRepository bankLinkRepository,
            IBankAccountRepository bankAccountRepository,
            IBankAccountHistoryRepository bankAccountHistoryRepo,
            IBankTransactionRepository bankTransactionRepository,
            IEventHubClient eventHubClient,
            ITenantTime tenantTime,
            IFinicityProxy proxy,
            IDecisionEngineService decisionEngineService,
            IBalanceRepository balanceRepository)
        {
            if (configuration == null) throw new ArgumentException($"{nameof(configuration)} is mandatory");

            Logger = logger;
            Configuration = configuration;
            TenantTime = tenantTime;
            BankLinkRepository = bankLinkRepository;
            BankAccountRepository = bankAccountRepository;
            BankAccountHistoryRepository = bankAccountHistoryRepo;
            BankTransactionRepository = bankTransactionRepository;
            EventHub = eventHubClient;
            Proxy = proxy;
            DecisionEngineService = decisionEngineService;
            BalanceRepository = balanceRepository;
        }

        #endregion

        #region Variable Declaration

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Gets Logger
        /// </summary>
        private ILogger Logger { get; }

        /// <summary>
        /// Gets TenantTime
        /// </summary>
        private ITenantTime TenantTime { get; }

        /// <summary>
        /// Gets BankLinkRepository
        /// </summary>
        private IBankLinkRepository BankLinkRepository { get; }

        /// <summary>
        /// Gets BankAccountRepository
        /// </summary>
        private IBankAccountRepository BankAccountRepository { get; }

        /// <summary>
        /// Gets BankAccountHistoryRepository
        /// </summary>
        private IBankAccountHistoryRepository BankAccountHistoryRepository { get; }

        /// <summary>
        /// Gets BankTransactionRepository
        /// </summary>
        private IBankTransactionRepository BankTransactionRepository { get; }

        /// <summary>
        /// Gets EventHubClient
        /// </summary>
        private IEventHubClient EventHub { get; }

        private IFinicityProxy Proxy { get; }

        private IDecisionEngineService DecisionEngineService { get; }

        private IBalanceRepository BalanceRepository { get; }


        #endregion

        public async Task<IAccessTokenResponse> GetAccessToken(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            return await Task.Run(() =>
            {
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                var response = Proxy.GetAccessToken(entityType, entityId);
                var result = new AccessTokenResponse(response);

                return result;
            });
        }

        public async Task<IResponseConnection> AddCustomer(string entityType, string entityId, IAddCustomerRequest request)
        {
            Logger.Info("Processing the AddBankLinkAndAccounts method in Finicity service");

            var accountType = Configuration.AccountTypes;

            var response = Proxy.AddCustomer(entityType, entityId, request);


            Logger.Info($"Finicity Service getting response successfully");

            var bankLinkId = await BankLinkConversionForFinicity(entityType, entityId, request, response);

            if (string.IsNullOrWhiteSpace(bankLinkId))
            {
                Logger.Info($"bankLinkId is null or white space");
                throw new InvalidOperationException($"bankLinkId is null or white space");
            }

            Logger.Info($"bank link id created successfully");


            var customerResult = await CreatingConnectionResponse(request.FinicityAppToken, response.CustomerId, bankLinkId);
            return customerResult;
        }

        public async Task<IConnectLinkResponse> GenerateConnectLink(string entityType, string entityId, IConnectLinkRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            return await Task.Run(() =>
            {
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                var response = Proxy.GenerateConnectLink(entityType, entityId, request);
                // var result = new ConnectLinkResponse(response);

                return response;
            });
        }

        private async Task BankAccountConversionForFinicity(IBankLink bankLinkdata, ICustomerAccountsResponse response, List<string> accountTypes)
        {
            Logger.Info($"Processing the BankAccountConversionForFinicity method");

            foreach (var item in response.CustomerAccountsDetails)
            {
                if (accountTypes.Contains(item.Type))
                {
                    var accountdetails = await BankAccountRepository.GetAccountByProviderAccountId(Convert.ToString(item.Id));
                    if (accountdetails == null)
                    {
                        IBankAccount bankAccount = new BankAccount();
                        bankAccount.NameOnAccount = item.Name;
                        bankAccount.AccountNumber = item.Number;
                        if (item.Number.Length >= 4)
                            bankAccount.AccountNumberMask = item.Number.Substring(item.Number.Length - 4, 4);
                        bankAccount.AccountType = item.Type;
                        bankAccount.AvailableBalance = item.Balance;
                        bankAccount.CurrentBalance = item.Balance;
                        bankAccount.BankLinkId = bankLinkdata.Id;
                        bankAccount.IsDeleted = false;
                        bankAccount.IsActive = true;
                        bankAccount.RoutingNumber = item.RoutingNumber;
                        bankAccount.WireRoutingNumber = null;
                        bankAccount.ProviderAccountId = Convert.ToString(item.Id);
                        bankAccount.CreatedDate = new TimeBucket(TenantTime.Now);
                        bankAccount.UpdatedDate = new TimeBucket(TenantTime.Now);
                        bankAccount.ProviderId = Convert.ToInt32(Provider.Finicity);
                        bankAccount.OwnerName = null;

                        this.BankAccountRepository.Add(bankAccount);
                        Logger.Info($"bankAccount data inserted for the accountId {item.Id.ToString()}");
                        this.BankAccountHistoryRepository.Add(bankAccount);
                        Logger.Info($"bankAccount history data inserted for the accountId {item.Id.ToString()}");

                        bankLinkdata.ProviderBankId = item.InstitutionId;
                        bankLinkdata.ProviderBankName = item.InstitutionName;
                        this.BankLinkRepository.Update(bankLinkdata);
                    }
                }
            }

        }

        public async Task<ICustomerAccountsResponse> GetCustomerAccounts(string entityType, string entityId, ICustomerDetailRequest request)
        {
            Logger.Info("Processing the GetCustomerAccounts method in Finicity service");

           
            var accountType = Configuration.AccountTypes;

            var response = Proxy.GetCustomerAccounts(entityType, entityId, request);
            var resultresponse = new CustomerAccountsResponse(response);

            Logger.Info($"Finicity Service getting response successfully");

            var linkdata = await BankLinkRepository.GetBankLinkData(request.CustomerId);
            if (linkdata == null)
                throw new ArgumentException($"bank link id does not exist for CustomerId: {request.CustomerId}");

            await BankAccountConversionForFinicity(linkdata, resultresponse, accountType);
            Logger.Info($"Accounts added successfully");

            Logger.Info($"Processing the BankLinkCreated Event request payload: entityId { entityId} , entityType: { entityType}");
            await EventHub.Publish(new BankLinkCreated()
            {
                EntityId = entityId,
                BankLinkNumber = linkdata.Id,
                EntityType = entityType,
                ReferenceNumber = Guid.NewGuid().ToString("N")
            });

            //// bank link trigger for remove tag from borrower
            await EventHub.Publish("BankLinked", new
            {
                EntityId = entityId,
                EntityType = entityType,
                ReferenceNumber = Guid.NewGuid().ToString("N")
            });

            return resultresponse;
        }

        public async Task<ITransactionsResponse> GetTransactionsByAccount(string entityType, string entityId, Request.ITransactionRequest request)
        {
            Logger.Info("Processing the GetCustomerAccounts method in Finicity service");

            var response = Proxy.GetTransactionsByAccount(entityType, entityId, request);
            var resultresponse = new TransactionsResponse(response);

            Logger.Info($"Finicity Service getting response successfully");

            var linkdata = await BankLinkRepository.GetBankLinkData(request.CustomerId);
            if (linkdata == null)
                throw new ArgumentException($"bank link id does not exist for CustomerId: {request.CustomerId}");

            var accountIds = await TransactionConversionForFinicity(resultresponse, request.CustomerId);
            Logger.Info($"transactions added successfully");
            accountIds = accountIds.Distinct().ToList();
            if (linkdata != null && accountIds.Any())
            {
                Logger.Info("Processing the GenerateFinicityCashflow event trigger for the cashflow");
                Logger.Info($"Event publish request payload : entityId {linkdata.EntityId} , Response :{accountIds}");

                await EventHub.Publish(new GenerateCashflow()
                {
                    EntityId = linkdata.EntityId,
                    EntityType = "application",
                    Response = accountIds,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });

                await EventHub.Publish(new WebhookTrigger()
                {
                    EntityId = linkdata.EntityId,
                    BankLinkNumber = linkdata.Id,
                    EntityType = "application",
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
            }
            else
            {
                Logger.Info("Event trigger for the cashflow is not executed...");
            }
            return resultresponse;
        }

        public async Task<ITxPushResponse> EnableTxPush(string entityType, string entityId, ITxPushRequest txPushRequest)
        {
            Logger.Info("Processing the EnableTxPush method in Finicity service");

            var response = Proxy.EnableTxPush(entityType, entityId, txPushRequest);
            var resultresponse = new TxPushResponse(response);

            Logger.Info($"Finicity Service getting response successfully");

            return resultresponse;
        }

        private async Task<List<string>> TransactionConversionForFinicity(ITransactionsResponse response, string customerId)
        {
            Logger.Info($"Processing the TransactionConversionForFinicity method");

            var accountIds = new List<string>();
            var transactionsList = new List<IBankTransaction>();

            foreach (var item in response.Transactions)
            {
                IBankTransaction bankTransaction = new BankTransaction();
                accountIds.Add(item.AccountId.ToString());
                bankTransaction.ProviderBankLinkId = customerId;
                bankTransaction.TransactionId = item.Id.ToString();
                bankTransaction.TransactionType = item.Type;
                bankTransaction.TransactionDate = new DateTime(1970, 1, 1).AddSeconds(item.TransactionDate);
                bankTransaction.Description = item.Description;
                bankTransaction.Type = item.Amount > 0 ? "Credit" : "Debit";
                bankTransaction.Amount = item.Amount * (-1);
                if (item.Categorization != null && !string.IsNullOrWhiteSpace(item.Categorization.Category))
                {
                    string[] category = new string[1];
                    category[0] = item.Categorization.Category;
                    bankTransaction.Category = category;
                    bankTransaction.CategoryId = item.Categorization.Category;
                }
                bankTransaction.CreatedDate = TenantTime.Now;
                bankTransaction.ProviderAccountId = item.AccountId.ToString();
                //bankTransaction.Location = Conversion(item.);
                //bankTransaction.PaymentMeta = Conversion(item.);
                if (item.Status.ToLower() == "pending")
                    bankTransaction.Pending = true;
                bankTransaction.ProviderId = Convert.ToInt32(Provider.Finicity);
                transactionsList.Add(bankTransaction);
            }
            // Commented as we need to compute RunningBalance and then store the data
            // await BankTransactionRepository.AddTransactions(transactionsList);
            this.Logger.Info("item transaction are added");

            /// Compute Running Balance and store TransactionData to database. 
            await ComputeRunningBalance(transactionsList);

            return accountIds;


        }

        private async Task ComputeRunningBalance(List<IBankTransaction> transactionsList)
        {
            Logger.Info("ComputeRunningBalance started for finicity....");

            if (transactionsList == null || transactionsList.Count < 1)
            {
                Logger.Info("... no transaction data to process. EXIT with no errors.");
                return;
            }

            Logger.Info($".... processing transactions.... ");
            var accountList = transactionsList.Select(x => x.ProviderAccountId).Distinct().ToList<string>();


            foreach (var account in accountList)
            {
                Logger.Info($".... processing for account {account}");
                var accountdetails = await BankAccountRepository.GetAccountByProviderAccountId(account);
                var trans = transactionsList.Where(x => x.ProviderAccountId == account).OrderByDescending(x => x.TransactionDate);
                var accountBalance = accountdetails.CurrentBalance;

                if (accountBalance != null && accountBalance.HasValue)
                {
                    double bal = accountBalance.Value;
                    foreach (var record in trans)
                    {
                        record.RunningBalance = bal;
                        bal = bal + (record.Amount);
                    }
                }
                Logger.Info($".... invoke BankTransactionRepository");
                await BankTransactionRepository.AddTransactions(trans);
            }


            Logger.Info("ComputeRunningBalance ended....");
        }

        private async Task<string> BankLinkConversionForFinicity(string entityType, string entityId, IAddCustomerRequest request, IAddCustomerResponse response)
        {
            Logger.Info("Processing BankLinkConversionForFinicity method");
            IBankLink bankLink = new BankLink();
            bankLink.EntityId = entityId;
            bankLink.CreatedDate = new TimeBucket(TenantTime.Now);
            bankLink.IsDeleted = false;
            bankLink.FirstName = request.FirstName;
            bankLink.LastName = request.LastName;
            bankLink.UserMailId = "finicity@fin.com"; //TODO
            //  bankLink.ProviderToken = response.AccessToken;
            bankLink.ProviderBankLinkId = response.CustomerId;
            bankLink.ProviderId = Convert.ToInt32(Provider.Finicity);
            //bankLink.ProviderBankId = response.InstitutionId;
            bankLink.ProviderBankName = null;
            bankLink.Links = new List<ILink>();
            bankLink.Links.Add(new Link { EntityType = entityType, Products = null });
            this.BankLinkRepository.Add(bankLink);
            Logger.Info($"BankLink data inserted successfully , id : {bankLink.Id}");
            return await Task.Run(() =>
            {
                return bankLink.Id;
            });
        }

        private async Task<IResponseConnection> CreatingConnectionResponse(string token, string providerLinkId, string bankLinkId)
        {
            return await Task.Run(() =>
            {
                return new ResponseConnection
                {
                    BankLinkId = bankLinkId,
                    Token = token,
                    ProviderLinkId = providerLinkId
                };
            });
        }

        #region InBound Hook Invoked Handler

        public async Task<bool> InBoundHookInvokedHandler(string entityType, string entityId, IEventRequest request)
        {
            bool result = false;
            try
            {
                Logger.Info("Started Execution for InBoundHookInvokedHandler Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                if (String.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (request == null)
                    throw new ArgumentNullException(nameof(request));
                if (request.data == null)
                    throw new ArgumentNullException(nameof(request.data));

                var ruleResult = DecisionEngineService.Execute<dynamic>(request.DERuleName, new { input = request.data });
                if (ruleResult != null)
                {
                    await UpdateAccountAndSaveTransaction(entityType, entityId, ruleResult);
                }

                result = true;
            }
            catch (Exception ex)
            {
                Logger.Error($"InBoundHookInvokedHandler Error For : {entityId} , Error : {ex.Message + ex.StackTrace}");
            }
            Logger.Info("Completed Execution for InBoundHookInvokedHandler Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            return result;
        }


        public async Task UpdateAccountAndSaveTransaction(string entityType, string entityId, dynamic ruleResult)
        {
            List<string> lstAccounts = new List<string>();
            AccountsAndTransactions objInput = new AccountsAndTransactions();
            objInput = ExecuteRequest<AccountsAndTransactions>(ruleResult + "");

            var objAccountTransaction = new List<Transaction>();
            var bankLinkId = string.Empty;

            if (objInput.Accounts != null && objInput.Accounts.Count > 0)
            {
                foreach (var account in objInput.Accounts)
                {
                    var objAccount = await BankAccountRepository.GetAccountByProviderAccountId(account.ProviderAccountId);
                    bankLinkId = objAccount.BankLinkId;
                    if (objAccount != null)
                    {
                        objAccount.CurrentBalance = account.CurrentBalance;
                        objAccount.AvailableBalance = account.AvailableBalance;
                        var accountId = await BankAccountRepository.UpdateAccount(objAccount);
                    }
                }
            }

            if (objInput.Transactions != null && objInput.Transactions.Count > 0)
            {
                var accountIds = await AddTransactions(objInput.Transactions);

                if (accountIds.Any())
                {
                    Logger.Info("Processing the GenerateFinicityCashflow event trigger for the cashflow");
                    Logger.Info($"Event publish request payload : entityId {entityId} , Response :{accountIds}");
                    await EventHub.Publish(new GenerateCashflow()
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = accountIds,
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });

                    await EventHub.Publish(new WebhookTrigger()
                    {
                        EntityId = entityId,
                        BankLinkNumber = bankLinkId,
                        EntityType = "application",
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });
                }
                else
                {
                    Logger.Info("Event trigger for the cashflow is not executed...");
                }
            }

        }


        private async Task<List<string>> AddTransactions(List<TransactionDetails> Transactions)
        {
            Logger.Info($"Processing the TransactionConversionForFinicity method");

            var transactions = new List<IBankTransaction>();

            var acountIds = new List<string>();
            foreach (var item in Transactions)
            {
                var bankaccount = await BankAccountRepository.GetAccountByProviderAccountId(item.ProviderAccountId);

                if (bankaccount != null)
                {

                    IBankTransaction bankTransaction = new BankTransaction();
                    bankTransaction.ProviderBankLinkId = bankaccount.BankLinkId;
                    acountIds.Add(item.ProviderAccountId);
                    bankTransaction.TransactionId = item.TransactionId.ToString();
                    //bankTransaction.TransactionType = item.EntityType;
                    bankTransaction.TransactionDate = Convert.ToDateTime(item.TransactionDate);
                    bankTransaction.Description = item.Description;
                    bankTransaction.Type = item.Amount > 0 ? "Credit" : "Debit";
                    bankTransaction.Amount = item.Amount * (-1); //muptiplied by -1 to keep the credit and debit in same format as plaid
                    if (item.Categories != null && item.Categories.Count() > 0)
                    {
                        bankTransaction.Category = item.Categories.ToArray();

                    }
                    bankTransaction.CategoryId = item.CategoryId;
                    bankTransaction.CreatedDate = TenantTime.Now;
                    bankTransaction.ProviderAccountId = item.ProviderAccountId.ToString();
                    //bankTransaction.Location = Conversion(item.);
                    //bankTransaction.PaymentMeta = Conversion(item.);
                    if (item.Pending.ToLower() == "true")
                        bankTransaction.Pending = true;
                    bankTransaction.ProviderId = Convert.ToInt32(Provider.Finicity);
                    transactions.Add(bankTransaction);
                }
            }

            var accounts = new List<IRunningBalanceDetails>();
            var updateAccounts = transactions.GroupBy(x => x.ProviderAccountId).Select(grouping => grouping.FirstOrDefault()).ToList();

            foreach (var item in updateAccounts)
            {
                var account = new RunningBalanceDetails();
                account.ProviderAccountId = item.ProviderAccountId;
                account.ProviderBankLinkId = item.ProviderBankLinkId;
                account.TenantId = item.TenantId;
                account.IsProcessed = false;
                account.CreatedDate = TenantTime.Now;
                account.EntityId = "";
                accounts.Add(account);
            }
            await BalanceRepository.AddAccountDetails(accounts);
            await BankTransactionRepository.AddTransactions(transactions);
            this.Logger.Info("item transaction are added");
            return acountIds;

        }
        #endregion


        public async Task<bool> VerifyFinicityEvents(string entityType, string entityId, string eventName)
        {
            var finicityRequest = string.Empty;
            var finicityResponse = string.Empty;
            if (eventName == "INBOUND_HOOK_INVOKED")
            {
                finicityRequest = ReadJSONFile(@"InBoundHookInvoked - Request.json") + "";
                finicityResponse = ReadJSONFile(@"InBoundHookInvoked - Response.json") + "";

                await EventHub.Publish(eventName, new
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = finicityResponse,
                    Request = finicityRequest,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
            }

            return true;
        }

        public async Task<IAddTransactionDetailResponse> AddTransaction(IAddTransactionDetailRequest transactionDetailRequest)
        {

            Logger.Info("Processing the AddTransaction method in Finicity service");

            var response = Proxy.AddTransaction(transactionDetailRequest);

            Logger.Info($"Finicity Service getting response successfully");

            return response;
        }


        public async Task<dynamic> LoadHistoricTransactions(string entityType, string entityId, ICustomerDetailRequest customerDetailRequest)
        {
            return await Task.Run(async () =>
            {

                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (string.IsNullOrWhiteSpace(entityType))
                    throw new InvalidArgumentException("Invalid Entity Type");
                if (customerDetailRequest == null)
                    throw new ArgumentNullException(nameof(customerDetailRequest));
                try
                {
                    Logger.Info("Processing LoadHistoricTransactions Request in IBV-Finicity Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                    var linkdata = await BankLinkRepository.GetBankLinkData(customerDetailRequest.CustomerId);
                    if (linkdata == null || string.IsNullOrEmpty(linkdata.Id))
                    {
                        Logger.Info($"bankLinkId is null or white space");
                        throw new InvalidOperationException($"bankLinkId is null or white space");
                    }

                    var accounts = await BankAccountRepository.GetAccountIds(linkdata.Id);
                    List<dynamic> response = new List<dynamic>();
                    if (accounts.Any())
                    {
                        foreach (var item in accounts)
                        {
                            customerDetailRequest.AccountId = item;
                            dynamic result = Proxy.LoadHistoricTransactions(entityType, entityId, customerDetailRequest);

                        }

                    }
                    Logger.Info("Completed Execution for LoadHistoricTransactions Request in IBV-Finicity Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: LoadHistoricTransactions");

                    return true;

                }
                catch (Exception exception)
                {
                    Logger.Error("Error While Processing LoadHistoricTransactions Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: LoadHistoricTransactions");

                    throw exception;
                }
            });
        }

        private dynamic ReadJSONFile(string fileName)
        {
            var json = string.Empty;
            var folderPath = @"E:\CashFlow\lf_cashflow\src\LendFoundry.Cashflow.Api\";
            try
            {
                using (var r = new StreamReader(folderPath + fileName))
                {
                    json = r.ReadToEnd();
                }
            }
            catch (Exception)
            {
                return "";
            }
            return JsonConvert.DeserializeObject<dynamic>(json);
        }


        #region Execute Request
        private T ExecuteRequest<T>(string response) where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(response);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response, exception);
            }
        }
        #endregion
    }
}

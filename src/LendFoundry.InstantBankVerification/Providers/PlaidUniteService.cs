﻿using LendFoundry.Clients.DecisionEngine;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Syndication.PlaidIBV;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class PlaidUniteService : IPlaidUniteService
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PlaidUniteService"/> class.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="bankLinkRepository"></param>
        /// <param name="bankAccountRepository"></param>
        /// <param name="bankTransactionRepository"></param>
        /// <param name="PlaidClient"></param>
        public PlaidUniteService(ILogger logger,
            IConfiguration configuration,
            IBankLinkRepository bankLinkRepository,
            IBankAccountRepository bankAccountRepository,
            IBankAccountHistoryRepository bankAccountHistoryRepo,
            IBankTransactionRepository bankTransactionRepository,
            IEventHubClient eventHubClient,
            IClientTrackRepository clientTrackRepository,
            ITenantTime tenantTime,
            IPlaidService PlaidClient,
            IDecisionEngineService decisionEngineService,
            IBalanceRepository balanceRepository)
        {
            if (configuration == null) throw new ArgumentException($"{nameof(configuration)} is mandatory");

            Logger = logger;
            Configuration = configuration;
            TenantTime = tenantTime;
            PlaidService = PlaidClient;
            BankLinkRepository = bankLinkRepository;
            BankAccountRepository = bankAccountRepository;
            BankAccountHistoryRepository = bankAccountHistoryRepo;
            BankTransactionRepository = bankTransactionRepository;
            EventHub = eventHubClient;
            ClientTrackRepository = clientTrackRepository;
            DecisionEngineService = decisionEngineService;
            BalanceRepository = balanceRepository;
        }

        #endregion

        #region Variable Declaration

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Gets Logger
        /// </summary>
        private ILogger Logger { get; }

        /// <summary>
        /// Gets TenantTime
        /// </summary>
        private ITenantTime TenantTime { get; }

        /// <summary>
        /// PlaidService
        /// </summary>  
        private IPlaidService PlaidService { get; }

        /// <summary>
        /// Gets BankLinkRepository
        /// </summary>
        private IBankLinkRepository BankLinkRepository { get; }

        /// <summary>
        /// Gets BankAccountRepository
        /// </summary>
        private IBankAccountRepository BankAccountRepository { get; }

        /// <summary>
        /// Gets BankAccountHistoryRepository
        /// </summary>
        private IBankAccountHistoryRepository BankAccountHistoryRepository { get; }

        /// <summary>
        /// Gets BankTransactionRepository
        /// </summary>
        private IBankTransactionRepository BankTransactionRepository { get; }

        /// <summary>
        /// Gets EventHubClient
        /// </summary>
        private IEventHubClient EventHub { get; }

        /// <summary>
        /// ClientTrackRepository
        /// </summary>
        private IClientTrackRepository ClientTrackRepository { get; }

        /// <summary>
        /// PlaidPagingLimit
        /// </summary>
        public const int PlaidPagingLimit = 500;

        /// <summary>
        /// DecisionEngineService
        /// </summary>
        private IDecisionEngineService DecisionEngineService { get; }

        private IBalanceRepository BalanceRepository { get; }
        

        #endregion

        #region Public methods for Plaid Generic

        /// <summary>
        /// AddBankLinkAndAccounts
        /// </summary>
        /// <returns></returns>
        public async Task<IResponseConnection> AddBankLinkAndAccounts(string entityType, string entityId, IRequestConnection request)
        {
            Logger.Info("Processing the AddBankLinkAndAccounts method in Plaid Unite service");
            IBankLinkRequest data = new BankLinkRequest
            {
                PublicToken = request.ProviderToken
            };
            Logger.Info($"PublicToken as ProviderToken : {request.ProviderToken}");
            var accountType = Configuration.AccountTypes;

            var response = await PlaidService.GetAccountsDetails(data, request.BankId);

            if (response.HasError)
            {
                Logger.Info($"Plaid Service issue :{response.Error}");
                throw new ArgumentException($"{response.Error}");
            }

            Logger.Info($"Plaid Service getting response successfully");

            var bankLinkId = await BankLinkConversionForPlaid(entityType, entityId, request, response);

            if (string.IsNullOrWhiteSpace(bankLinkId))
            {
                Logger.Info($"bankLinkId is null or white space");
                throw new InvalidOperationException($"bankLinkId is null or white space");
            }

            await BankAccountConversionForPlaid(bankLinkId, response, accountType);
            Logger.Info($"bankaccount and bankaccounthistory data inserted");

            await EventHub.Publish(new BankLinkCreated()
            {
                EntityId = entityId,
                BankLinkNumber = bankLinkId,
                EntityType = entityType,
                ReferenceNumber = Guid.NewGuid().ToString("N")
            });

            //// bank link trigger for remove tag from borrower
            await EventHub.Publish("BankLinked", new
            {
                EntityId = entityId,
                EntityType = entityType,
                ReferenceNumber = Guid.NewGuid().ToString("N")
            });

            await EventHub.Publish("PlaidBankLinked", new
            {
                EntityId = entityId,
                EntityType = entityType,
                ReferenceNumber = Guid.NewGuid().ToString("N")
            });


            var result = await CreatingConnectionResponse(response.AccessToken, response.PlaidItemId, bankLinkId);
            return result;
        }

        /// <summary>
        /// AddBankLinkAndAccounts
        /// </summary>
        /// <returns></returns>
        public async Task<object> AccoutSync(string entityType, string entityId, IRequestConnection request)
        {
            Logger.Info("Processing the AccoutSync method in Plaid service");
            IBankLinkRequest data = new BankLinkRequest
            {
                AccessToken = request.ProviderToken
            };

            var accountType = Configuration.AccountTypes;

            var response = await PlaidService.AccountSync(data);
            if (response.HasError)
            {
                Logger.Info($"In PlaidService getting error : {response.Error} ");
                throw new ArgumentException($"{response.Error}");
            }
            Logger.Info($"In PlaidService getting success");
            return response.Accounts;
        }

        /// <summary>
        /// DeleteConnection
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<bool> DeleteConnection(string entityType, string entityId, IRequestConnection request)
        {
            Logger.Info("Processing the DeleteConnection method in Plaid Unite service");
            IBankLinkRequest data = new BankLinkRequest
            {
                AccessToken = request.ProviderToken
            };
            var ack = await PlaidService.DeleteItemConnection(data);

            if (ack.Deleted)
            {
                Logger.Info("PlaidService getting success ,delinking done");
                var bankLinkId = await this.BankLinkRepository.GetBankLinkId(request.ProviderToken);

                if (!string.IsNullOrWhiteSpace(bankLinkId))
                {
                    var accountUpdate = await this.BankAccountRepository.UpdateBankAccount(bankLinkId);
                    Logger.Info("bank account deleted");
                    if (accountUpdate)
                    {
                        await this.BankLinkRepository.UpdateBankLink(request.ProviderToken);
                        Logger.Info("bank link deleted");

                        await EventHub.Publish(new DeleteLinking()
                        {
                            EntityId = entityId,
                            BankLinkNumber = bankLinkId,
                            EntityType = entityType,
                            ReferenceNumber = Guid.NewGuid().ToString("N")
                        });
                    }
                }
            }
            return ack.Deleted;
        }

        /// <summary>
        /// PullTransaction
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task PullTransaction(IRequestTransaction request)
        {
            Logger.Info("Processing the PullTransaction method in Plaid Unite service");
            if (UtilityHelper.TryParseEnumCheck<WebhookCodes>(request.WebhookCode))
            {
                var bankLinkData = await this.BankLinkRepository.GetBankLinkData(request.ItemId);
                this.Logger.Info("Get EntityId  : " + bankLinkData.EntityId);

                if (bankLinkData == null && string.IsNullOrWhiteSpace(bankLinkData.ProviderToken))
                {
                    throw new ArgumentException($"ItemId_Invalid");
                }

                //// Insert transactions
                var accountIds = await InsertTransactions(bankLinkData, request);

                if (bankLinkData != null && accountIds.Any())
                {
                    if (request.WebhookCode.ToUpper() == WebhookCodes.Historical_Update.ToString().ToUpper())
                    {
                        Logger.Info("Processing the GeneratePlaidCashflow event trigger for the cashflow");
                        Logger.Info($"Event publish request payload : entityId {bankLinkData.EntityId} , Response :{accountIds}");
                        await EventHub.Publish(new GenerateCashflow()
                        {
                            EntityId = bankLinkData.EntityId,
                            EntityType = "application",
                            Response = accountIds,
                            ReferenceNumber = Guid.NewGuid().ToString("N")
                        });
                    }

                    //// For filter_view
                    await EventHub.Publish(new WebhookTrigger()
                    {
                        EntityId = bankLinkData.EntityId,
                        BankLinkNumber = bankLinkData.Id,
                        EntityType = "application",
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });
                }
                else
                {
                    Logger.Info("Event trigger for the cashflow is not executed...");
                }
            }
        }

        /// <summary>
        /// PlaidErrorHandling
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task PlaidErrorHandling(string ItemId, IEventRequest request)
        {
            var bankLinkData = await this.BankLinkRepository.GetBankLinkData(ItemId);

            if (bankLinkData == null)
            {
                throw new ArgumentException($"ItemId_Invalid");
            }

            var ruleResult = DecisionEngineService.Execute<dynamic>(request.DERuleName, new { payload = request.data });
            IPlaidErrorDetails objInput = new PlaidErrorDetails();

            if (ruleResult != null)
            {               
                try
                {
                    objInput = JsonConvert.DeserializeObject<PlaidErrorDetails>(ruleResult + "");
                }
                catch (Exception exception)
                {
                    throw new Exception("Unable to deserialize:" + ruleResult, exception);
                }
            }

            var refreshFlag = false;

            if (objInput.ErrorCode.ToUpper() == "ITEM_LOGIN_REQUIRED")
            {
                refreshFlag = true;
                await EventHub.Publish("ItemLoginRequired", new
                {
                    EntityId = bankLinkData.EntityId,
                    EntityType = "application",
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
            }
            else if (objInput.ErrorCode.ToUpper() == "INVALID_CREDENTIALS")
            {
                refreshFlag = true;
                await EventHub.Publish("CredentialsChanged", new
                {
                    EntityId = bankLinkData.EntityId,
                    EntityType = "application",
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
            }
            else
            {
                await EventHub.Publish("PlaidException", new
                {
                    EntityId = bankLinkData.EntityId,
                    EntityType = "application",
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
                //// bank link trigger for re-linking tag from borrower
                await EventHub.Publish("PlaidBankLinkingReInit", new
                {
                    EntityId = bankLinkData.EntityId,
                    EntityType = "application",
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });

            }

            if (refreshFlag)
            {
                //// bank link trigger for refresh linking tag from borrower
                await EventHub.Publish("PlaidBankLinkingRefresh", new
                {
                    EntityId = bankLinkData.EntityId,
                    EntityType = "application",
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
            }
        }

        /// <summary>
        /// PlaidRefreshLinking
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<IPlaidRefreshResponse> PlaidRefreshLinking(string entityId)
        {
            var bankLinkData = await this.BankLinkRepository.GetBankLinkDataByEntityId(entityId);

            if (bankLinkData == null)
            {
                throw new ArgumentException($"Plaid is not linked with this application : {entityId}");
            }

            IBankLinkRequest data = new BankLinkRequest
            {
                AccessToken = bankLinkData.ProviderToken
            };

            var response = await PlaidService.CreatePublicTokenForRefresh(data);

            if(response != null)
            {
                await EventHub.Publish("BankLinked", new
                {
                    EntityId = entityId,
                    EntityType = "application",
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
            }

            return new PlaidRefreshResponse(response.PublicToken, response.RequestId);
        }

        /// <summary>
        /// PlaidClientDataEventHandler
        /// </summary>
        /// <param name="ClientTrackRequest"></param>
        /// <returns></returns>
        public async Task PlaidClientDataEventHandler(IClientTrack ClientTrackRequest)
        {
            if (ClientTrackRequest == null)
                throw new ArgumentNullException(nameof(ClientTrackRequest));

            if (String.IsNullOrWhiteSpace(ClientTrackRequest.EntityId))
                throw new ArgumentNullException(nameof(ClientTrackRequest.EntityId));
            IClientTrack clientTrack = new ClientTrack(ClientTrackRequest);
            var result = await ClientTrackRepository.AddItem(clientTrack);
        }

        #endregion

        #region PBGP

        /// <summary>
        /// DeleteConnection
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAccount(IRequestAddBank request)
        {
            Logger.Info("Processing the DeleteAccount method in Plaid Unite service");
            var bankLinkData = await BankLinkRepository.GetBankLinkData(request.ItemId);

            if (bankLinkData == null || string.IsNullOrWhiteSpace(bankLinkData.ProviderToken))
            {
                throw new ArgumentException($"ItemId_Invalid : {request.ItemId}");
            }

            var accountUpdate = await this.BankAccountRepository.UpdateBankAccount(bankLinkData.Id);
            if (accountUpdate)
            {
                return await this.BankLinkRepository.UpdateBankLink(bankLinkData.ProviderToken);
            }

            return false;
        }

        /// <summary>
        /// AddBank
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entityType"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<bool> AddBank(string entityType, string entityId, IRequestAddBank request)
        {
            Logger.Info("Processing the AddBank method in Plaid Unite service");
            var result = false;
            IBankLinkRequest data = new BankLinkRequest
            {
                AccessToken = request.Token
            };
            var response = await PlaidService.AddBank(data);

            if (response.HasError)
            {
                throw new ArgumentException($"{response.Error}");
            }

            var bankLinkId = await BankLinkConversionForPlaid(entityType, entityId, null, response);
            if (string.IsNullOrWhiteSpace(bankLinkId))
            {
                throw new ArgumentException($"Banklinking_Insertion_Issue");
            }
            await BankAccountConversionForPlaid(bankLinkId, response, request.AccountType);
            result = true;
            return result;
        }

        /// <summary>
        /// PullTransaction
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task PullTransactionsForPbgp(IRequestTransaction request)
        {
            Logger.Info("Processing the PullTransaction method in Plaid Unite service");
            if (UtilityHelper.TryParseEnumCheck<WebhookCodes>(request.WebhookCode))
            {
                var bankLinkData = await this.BankLinkRepository.GetBankLinkData(request.ItemId);
                this.Logger.Info("Get EntityId : " + bankLinkData.EntityId);

                if (string.IsNullOrWhiteSpace(bankLinkData.ProviderToken))
                {
                    throw new ArgumentException($"ItemId_Invalid");
                }

                //// Insert transactions
                await InsertTransactions(bankLinkData, request);
                this.Logger.Info("Transaction are inserted successfully");
            }
        }

        public async Task<List<string>> DeleteToken()
        {

            var delinkedAccessToken = new List<string>();
            var result = new List<string>();
            Logger.Info("started Get Access Token's EntityId");
            //var entityIds = new FilterApplicationList()
            var accessTokenEntityIds = await BankLinkRepository.GetAllBankLinkData();
            if (accessTokenEntityIds == null || accessTokenEntityIds.Count <= 0)
                throw new ArgumentNullException($"{nameof(accessTokenEntityIds)} no access token found from items to perform delete token");
            Logger.Info("ended GetAccessTokenEntityId total:" + accessTokenEntityIds.Count + " found access token.");
       
           var entityIds = accessTokenEntityIds.Select(x => x.EntityId).ToList();
            Logger.Info("started ApplicationFilterService Get GetApplications Details By Entity Ids");
    
            Logger.Info("started" + Configuration.DeleteTokenRule + " rule execution");
            var ruleResult = DecisionEngineService.Execute<dynamic, dynamic>(Configuration.DeleteTokenRule, new { input = new { ApplicationNumber= entityIds} });
            Logger.Info("Ended" + Configuration.DeleteTokenRule + " rule execution");
            IFilterApplicationsResult FilterobjAccounts = ExecuteRequest<FilterApplicationsResult>(ruleResult + "");
            if (FilterobjAccounts == null || FilterobjAccounts.filterApplicationsResult.Count <= 0)
                throw new ArgumentNullException($"no access token EntityId found from Application Filter to perform delete token");
            Logger.Info("ended Get Applications Details By Entity Ids total:" + FilterobjAccounts.filterApplicationsResult.Count() + " found from application filter to perform delete access token.");
            var objAccessTokenList = from itemdata in accessTokenEntityIds
                                     join filterappdata in FilterobjAccounts.filterApplicationsResult on itemdata.EntityId equals filterappdata.EntityId
                                     select new FilterApplications()
                                     {
                                         EntityId = itemdata.EntityId,
                                         AccessToken = itemdata.ProviderToken,
                                         ItemCreateOn = itemdata.CreatedDate,
                                         ApplicationSubmitedDate = filterappdata.ApplicationSubmitedDate,
                                         Status = filterappdata.Status,
                                         StatusCode = filterappdata.StatusCode,
                                         StatusDate = filterappdata.StatusDate
                                     };
            IFilterApplicationsResult filterListApp = new FilterApplicationsResult();
            filterListApp.filterApplicationsResult = objAccessTokenList.ToList<IFilterApplications>();
            if (filterListApp == null && filterListApp.filterApplicationsResult.Count <= 0)
                throw new ArgumentNullException($"{nameof(filterListApp.filterApplicationsResult)}rule result: no application eligible to delete token");
           
            if (filterListApp != null && filterListApp.filterApplicationsResult.Count > 0)
            {
                foreach (var accesToken in filterListApp.filterApplicationsResult)
                {
                    if (string.IsNullOrEmpty(accesToken.AccessToken))
                        continue;
                    //// delink item from Plaid
                    var response = await PlaidService.DeleteItemConnection(new BankLinkRequest(){ AccessToken = accesToken.AccessToken});
                   if(response.Deleted)
                   {                   
                    await BankLinkRepository.UpdateBankLink(accesToken.AccessToken);
                    result.Add(accesToken.EntityId);
                   }
                   await EventHub.Publish("plaidDelinkPerformed", new
                    {
                        EntityId = accesToken.EntityId,
                        Deleted = response.Deleted,
                        Response = response.error,
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });

                }
              
            }
            Logger.Info("Ended delete token process..deleted token performed" + result.Count);
            return result;
        }

        #endregion


        #region Private methods
        /// <summary>
        /// InsertTransactions
        /// </summary>
        /// <param name="bankLinkData"></param>
        /// <param name="newTransactionCount"></param>
        /// <returns></returns>
        private async Task<List<string>> InsertTransactions(IBankLink bankLinkData, IRequestTransaction request)
        {
            Logger.Info("InsertTransactions starts...");

            //// Store Transaction Data that is pulled from Plaid in transactionList variable 
            var transactionsList = new List<IBankTransaction>();
            IPlaidResponse plaidresponse = null;

            int newTransactionCount = request.TransactionCount;
            Logger.Info($"Pulling new Transactions {newTransactionCount}");

            var startDate = GetStartDateByWebhookCode(request.WebhookCode);
            var endDate = TenantTime.Now.Date.ToString(Configuration.DateFormat);

            var transactionLimits = TransactionPaging(newTransactionCount);
            var count = 0;
            var accountIds = new List<string>();
            var accounts = await this.BankAccountRepository.GetAccounts(bankLinkData.Id);

            var accountsForSimulations = new List<SimulationAccounts>();   
            foreach (var item in accounts)
            {
                var simulationAccount = new SimulationAccounts();
                simulationAccount.SimulationAccountId = item.ProviderAccountId;
                simulationAccount.AccountType = item.AccountType;
                accountsForSimulations.Add(simulationAccount);
            }

            foreach (var dicItem in transactionLimits)
            {
                //// Add new transactions
                ITransactionRequest transRequest = new TransactionRequest
                {
                    AccessToken = bankLinkData.ProviderToken,
                    Count = dicItem.Value,
                    Offset = dicItem.Key,
                    StartDate = startDate,
                    EndDate = endDate,
                    SimulationAccounts = accountsForSimulations
                    
                };

                var response = await PlaidService.GetTransactions(transRequest, bankLinkData.ProviderBankId);

                if (response.HasError)
                {
                    throw new ArgumentException($"{response.Error}");
                }

                if (plaidresponse == null)
                {
                    plaidresponse = new PlaidResponse();
                    plaidresponse.Accounts = response.Accounts;
                    plaidresponse.PlaidItemId = response.PlaidItemId;
                }


                /// Account Update
                if (count == 0)
                {
                    foreach (var account in response.Accounts)
                    {
                        var matchAccount = accounts.FirstOrDefault(x => x.ProviderAccountId == account.PlaidAccountId);

                        if (matchAccount != null)
                        {
                            accountIds.Add(matchAccount.ProviderAccountId);
                            matchAccount.AvailableBalance = account.Balances != null ? account.Balances.Available : null;
                            matchAccount.CurrentBalance = account.Balances != null ? account.Balances.Current : null;
                            matchAccount.UpdatedDate = new TimeBucket(TenantTime.Now);
                            var flag = await this.BankAccountRepository.UpdateAccount(matchAccount);

                            if (flag)
                            {
                                matchAccount.Id = null;
                                matchAccount.CreatedDate = new TimeBucket(TenantTime.Now);
                                matchAccount.UpdatedDate = null;
                                BankAccountHistoryRepository.Add(matchAccount);
                            }
                        }
                    }
                }

                //var transactions = new List<IBankTransaction>();

                var updateAccount = response.Transactions.GroupBy(x => x.PlaidAccountId).Select(y => y);


                ////TODO: Ignore Transactions for accounts that are not linked.
                foreach (var item in response.Transactions)
                {
                    IBankTransaction bankTransaction = new BankTransaction();
                    bankTransaction.ProviderBankLinkId = bankLinkData.ProviderBankLinkId;
                    bankTransaction.TransactionId = item.PlaidTransactionId;
                    bankTransaction.TransactionType = item.Type;
                    bankTransaction.TransactionDate = item.Date;
                    bankTransaction.Description = item.Description;
                    bankTransaction.Amount = item.Amount;
                    bankTransaction.Type = item.Amount > 0 ? "Debit" : "Credit";
                    bankTransaction.Category = item.Category;
                    bankTransaction.CategoryId = item.CategoryId;
                    bankTransaction.CreatedDate = TenantTime.Now;
                    bankTransaction.ProviderAccountId = item.PlaidAccountId;
                    bankTransaction.Location = Conversion(item.Location);
                    bankTransaction.PaymentMeta = Conversion(item.PaymentMeta);
                    bankTransaction.Pending = item.Pending;
                    bankTransaction.ProviderId = Convert.ToInt32(Provider.Plaid);
                    transactionsList.Add(bankTransaction);
                    //transactions.Add(bankTransaction); //// Commented as we need to compute RunningBalance and then store the data
                }

                //// Commented as we need to compute RunningBalance and then store the data
                //await BankTransactionRepository.AddTransactions(transactions);
                this.Logger.Info("item transaction are added");

                count++;
            }

            //// Compute Running Balance and store TransactionData to database. 
            await ComputeRunningBalance(plaidresponse, request, transactionsList);


            return accountIds;
        }

        private async Task ComputeRunningBalance(IPlaidResponse plaidresponse, IRequestTransaction request, List<IBankTransaction> transactionsList)
        {
            Logger.Info("ComputeRunningBalance started....");

            if (transactionsList == null || transactionsList.Count < 1)
            {
                Logger.Info("... no transaction data to process. EXIT with no errors.");
                return;
            }

            if (request.WebhookCode.ToUpper().Equals(WebhookCodes.Initial_Update.ToString().ToUpper()))
            {
                Logger.Info($".... processing transactions for {request.WebhookCode.ToUpper()}");
                var accList = transactionsList.GroupBy(x => x.ProviderAccountId).Select(s => s.Key).ToList<string>();

                foreach (var account in accList)
                {
                    Logger.Info($".... processing for account {account}");

                    var trans = transactionsList.Where(x => x.ProviderAccountId == account).OrderByDescending(x => x.TransactionDate);
                    var accountBalance = plaidresponse.Accounts.FirstOrDefault(w => w.PlaidAccountId == account);

                    if (accountBalance != null && accountBalance.Balances != null && accountBalance.Balances.Current.HasValue)
                    {
                        //// For Initial Update get latest balances from Plaid
                        double bal = accountBalance.Balances.Current.Value;
                        foreach (var record in trans)
                        {
                            record.RunningBalance = bal;
                            bal = bal + (record.Amount);
                        }
                    }
                    Logger.Info($".... invoke BankTransactionRepository");
                    await BankTransactionRepository.AddTransactions(trans);
                }
            }
            else if (request.WebhookCode.ToUpper().Equals(WebhookCodes.Historical_Update.ToString().ToUpper()))
            {
                Logger.Info($".... processing transactions for {request.WebhookCode.ToUpper()}");
                var providerBankLinkId = transactionsList.Select(x => x.ProviderBankLinkId).FirstOrDefault<string>();

                Logger.Info($"(Get list of TransactionId for ItemId {providerBankLinkId}");
                var transactionIds = await BankTransactionRepository.GetTransactionIdsByProviderBankLinkId(providerBankLinkId);
                //// Remove transactions that are already in database
                var transactionsListfiltered = transactionsList.Where(x => !(transactionIds.Any(p => x.TransactionId == p))).ToList();
                Logger.Info($"Exists {transactionIds.Count} ; New {transactionsList.Count}; Duplicates {transactionsListfiltered.Count}");

                //// Get List of Accounts 
                //// TODO: This accounts should be narrowed down to only active accounts.
                var accList = transactionsListfiltered.GroupBy(x => x.ProviderAccountId).Select(s => s.Key).ToList<string>();

                foreach (var account in accList)
                {
                    //// Filter for single account in decendingorder
                    var transListSingleAccount = transactionsListfiltered
                        .Where(x => x.ProviderAccountId == account)
                        .OrderByDescending(x => x.TransactionDate);

                    ///// Get last transaction running balance
                    double? bal = await BankTransactionRepository.GetFirstTransactionRunningBalance(account);
                    Logger.Info($".... Update running balance");
                    if (!bal.HasValue)
                    {
                        var accountBalance = plaidresponse.Accounts.FirstOrDefault(w => w.PlaidAccountId == account);
                        if (accountBalance != null)
                            bal = accountBalance.Balances.Current ?? (accountBalance.Balances.Available ?? 0d);
                        else
                            bal = 0d;

                    }
                    foreach (var record in transListSingleAccount)
                    {
                        record.RunningBalance = bal;
                        bal = bal + (record.Amount);
                    }
                    Logger.Info($".... invoke BankTransactionRepository");
                    await BankTransactionRepository.AddTransactions(transListSingleAccount);
                }
            }
            else if (request.WebhookCode.ToUpper().Equals(WebhookCodes.Default_Update.ToString().ToUpper()))
            {
                //// DONE : set task for the default update for the time period (endpoint is done)
                //// TODO: create new collection to store cuurent data and update the transaction 
                Logger.Info($".... processing transactions for {request.WebhookCode.ToUpper()}");
                var accounts = new List<IRunningBalanceDetails>();
                var updateAccounts = transactionsList.GroupBy(x => x.ProviderAccountId).Select(grouping => grouping.FirstOrDefault()).ToList();

                foreach (var item in updateAccounts)
                {
                    var account= new RunningBalanceDetails();
                    account.ProviderAccountId = item.ProviderAccountId;
                    account.ProviderBankLinkId = item.ProviderBankLinkId;
                    account.TenantId= item.TenantId;
                    account.IsProcessed = false;
                    account.CreatedDate = TenantTime.Now;
                    account.EntityId = "";
                    accounts.Add(account);
                }
                await BalanceRepository.AddAccountDetails(accounts);
                await BankTransactionRepository.AddTransactions(transactionsList);
            }

            Logger.Info("ComputeRunningBalance ended....");
        }

        /// <summary>
        /// Convertion
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private Location Conversion(Syndication.PlaidIBV.Location data)
        {
            return new Location
            {
                Address = data.Address,
                City = data.City,
                Lat = data.Lat,
                Lon = data.Lon,
                State = data.State,
                Store_number = data.StoreNumber,
                Zip = data.Zip
            };
        }


        /// <summary>
        /// Convertion
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private PaymentMeta Conversion(Syndication.PlaidIBV.PaymentMeta data)
        {
            return new PaymentMeta
            {
                ByOrderOf = data.ByOrderOf,
                Payee = data.Payee,
                Payer = data.Payer,
                PaymentMethod = data.PaymentMethod,
                PaymentProcessor = data.PaymentProcessor,
                PpdId = data.PpdId,
                Reason = data.Reason,
                ReferenceNumber = data.ReferenceNumber
            };
        }

        /// <summary>
        /// BankLinkConversionForPlaid
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entityType"></param>
        /// <param name="response"></param>

        /// <returns></returns>
        private async Task<string> BankLinkConversionForPlaid(string entityType, string entityId, IRequestConnection request, IPlaidResponse response)
        {
            Logger.Info("Processing BankLinkConversionForPlaid method");
            IBankLink bankLink = new BankLink();

            if(request != null)
            {
                bankLink.FirstName = request.FirstName;
                bankLink.LastName = request.LastName;
                bankLink.UserMailId = request.UserMailId;

                bankLink.ProviderLinkReferenceId = request.LinkReferenceId;
                bankLink.ProviderLinkSessionId = request.LinkSessionId;
                bankLink.ProviderLinkTempToken = request.ProviderToken;
            }

            bankLink.EntityId = entityId;
            bankLink.CreatedDate = new TimeBucket(TenantTime.Now);
            bankLink.IsDeleted = false;
            bankLink.ProviderToken = response.AccessToken;
            bankLink.ProviderBankLinkId = response.PlaidItemId;
            bankLink.ProviderId = Convert.ToInt32(Provider.Plaid);
            bankLink.ProviderBankId = response.InstitutionId;
            bankLink.ProviderBankName = response.InstitutionName;
            bankLink.Links = new List<ILink>();
            bankLink.Links.Add(new Link { EntityType = entityType, Products = null });
            await this.BankLinkRepository.UpdateBankLink(entityId , Convert.ToInt32(Provider.Plaid));
            this.BankLinkRepository.Add(bankLink);

            Logger.Info($"BankLink data inserted successfully , id : {bankLink.Id}");
            return await Task.Run(() =>
            {
                return bankLink.Id;
            });
        }

        /// <summary>
        /// BankAccountConversionForPlaid
        /// </summary>
        /// <param name="bankLinkId"></param>
        /// <param name="response"></param>
        private async Task BankAccountConversionForPlaid(string bankLinkId, IPlaidResponse response, List<string> accountTypes)
        {
            Logger.Info($"Processing the BankAccountConversionForPlaid method");
            await Task.Run(() =>
            {
                foreach (var item in response.Accounts)
                {
                    var ownerName = response.Owners != null ? response.Owners.First() : null;
                    if (accountTypes.Contains(item.SubType))
                    {
                        IBankAccount bankAccount = new BankAccount();
                        bankAccount.NameOnAccount = item.AccountName;
                        bankAccount.OfficialAccountName = item.OfficialName;
                        bankAccount.AccountNumberMask = item.Mask;
                        bankAccount.AccountType = item.SubType;
                        bankAccount.AvailableBalance = item.Balances != null ? item.Balances.Available : null;
                        bankAccount.CurrentBalance = item.Balances != null ? item.Balances.Current : null;
                        bankAccount.BankLinkId = bankLinkId;
                        bankAccount.IsDeleted = false;
                        bankAccount.IsActive = true;
                        bankAccount.RoutingNumber = null;
                        bankAccount.WireRoutingNumber = null;
                        bankAccount.ProviderAccountId = item.PlaidAccountId;
                        bankAccount.CreatedDate = new TimeBucket(TenantTime.Now);
                        bankAccount.UpdatedDate = new TimeBucket(TenantTime.Now);
                        bankAccount.ProviderId = Convert.ToInt32(Provider.Plaid);
                        bankAccount.OwnerName = ownerName;
                        var numberResult = response.Numbers != null ? response.Numbers.FirstOrDefault(x => x.Account_Id == item.PlaidAccountId) : null;
                        if (numberResult != null)
                        {
                            bankAccount.AccountNumber = numberResult.AccountNumber;
                            bankAccount.RoutingNumber = numberResult.Routing;
                            bankAccount.WireRoutingNumber = numberResult.Wire_Routing;
                        }

                        this.BankAccountRepository.Add(bankAccount);
                        Logger.Info($"bankAccount data inserted for the accountId {item.PlaidAccountId}");
                        this.BankAccountHistoryRepository.Add(bankAccount);
                        Logger.Info($"bankAccount history data inserted for the accountId {item.PlaidAccountId}");
                    }
                }
            });
        }

        /// <summary>
        /// CreatingConnectionResponse
        /// </summary>
        /// <param name="token"></param>
        /// <param name="providerLinkId"></param>
        /// <param name="bankLinkId"></param>
        /// <returns></returns>
        private async Task<IResponseConnection> CreatingConnectionResponse(string token, string providerLinkId, string bankLinkId)
        {
            return await Task.Run(() =>
            {
                return new ResponseConnection
                {
                    BankLinkId = bankLinkId,
                    Token = token,
                    ProviderLinkId = providerLinkId
                };
            });
        }

        /// <summary>
        /// CreatingStartDate
        /// </summary>
        /// <returns>return startdate</returns>
        private string CalculationForStartDate()
        {
            var days = Configuration.TransactionDays;
            return TenantTime.Now.Date.AddDays(-days).ToString(Configuration.DateFormat);
        }

        private string GetStartDateByWebhookCode(string webhookCode)
        {
            if (webhookCode.ToUpper().Equals(WebhookCodes.Default_Update.ToString().ToUpper()))
            {
                return TenantTime.Now.Date.AddDays(-1).ToString(Configuration.DateFormat);
            }
            else if(webhookCode.ToUpper().Equals(WebhookCodes.Initial_Update.ToString().ToUpper()))
            {
                return TenantTime.Now.Date.AddDays(-30).ToString(Configuration.DateFormat);
            }
            else
            {
                return TenantTime.Now.Date.AddDays(-Configuration.TransactionDays).ToString(Configuration.DateFormat);
            }
        }

        /// <summary>
        /// TransactionPaging
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        private Dictionary<int, int> TransactionPaging(int existCount, int count)
        {
            var PlaidPagingLimit = 500;
            var range = new Dictionary<int, int>();
            var start = existCount;
            var end = 0;
            var fixedCount = PlaidPagingLimit;
            var iteration = 0;

            if (count > 0 && count < PlaidPagingLimit)
            {
                end = count;
                range.Add(start, end);
            }
            else
            {
                if ((count % PlaidPagingLimit) == 0)
                {
                    iteration = count / PlaidPagingLimit;
                }
                else
                {
                    iteration = (count / PlaidPagingLimit) + 1;
                }
                for (int i = 0; i < iteration; i++)
                {
                    start = existCount + end;
                    end = end + PlaidPagingLimit;
                    range.Add(start, fixedCount);
                }
            }
            return range;
        }

        /// <summary>
        /// TransactionPaging
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        private Dictionary<int, int> TransactionPaging(int count)
        {
            var range = new Dictionary<int, int>();
            var start = 0;
            var end = 0;
            var fixedCount = PlaidPagingLimit;
            var iteration = 0;

            if (count > 0 && count < PlaidPagingLimit)
            {
                //start = 0;
                //end = count;
                end = PlaidPagingLimit;
                range.Add(start, end);
            }
            else
            {
                if ((count % PlaidPagingLimit) == 0)
                {
                    iteration = count / PlaidPagingLimit;
                }
                else
                {
                    iteration = (count / PlaidPagingLimit) + 1;
                }
                for (int i = 0; i < iteration; i++)
                {
                    start = end;
                    end = end + PlaidPagingLimit;
                    range.Add(start, fixedCount);
                }
            }
            return range;
        }

         private T ExecuteRequest<T>(string response) where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(response);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response, exception);
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.InstantBankVerification.Model;
#if DOTNET2
using LendFoundry.Syndication.PerfectAudit.Abstractions;
using LendFoundry.Syndication.PerfectAudit;
#else
using LendFoundry.Syndication.PerfectAudit.Abstractions;
using LendFoundry.Syndication.PerfectAudit;
#endif

namespace LendFoundry.InstantBankVerification
{
    public class OcrolusPerfectAuditService : IOcrolusPerfectAuditService
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FinicityService"/> class.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="bankLinkRepository"></param>
        /// <param name="bankAccountRepository"></param>
        /// <param name="bankTransactionRepository"></param>
        /// <param name="PlaidClient"></param>
        public OcrolusPerfectAuditService(ILogger logger,
            IConfiguration configuration,
            IBankLinkRepository bankLinkRepository,
            IBankAccountRepository bankAccountRepository,
            IBankAccountHistoryRepository bankAccountHistoryRepo,
            IBankTransactionRepository bankTransactionRepository,
            IEventHubClient eventHubClient,
            ITenantTime tenantTime,
            IFinicityProxy proxy,
            IPerfectAuditService perfectAuditService)
        {
            if (configuration == null) throw new ArgumentException($"{nameof(configuration)} is mandatory");

            Logger = logger;
            Configuration = configuration;
            TenantTime = tenantTime;
            BankLinkRepository = bankLinkRepository;
            BankAccountRepository = bankAccountRepository;
            BankAccountHistoryRepository = bankAccountHistoryRepo;
            BankTransactionRepository = bankTransactionRepository;
            EventHub = eventHubClient;
            Proxy = proxy;
            PerfectAuditService = perfectAuditService;

        }

        #endregion

        #region Variable Declaration

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Gets Logger
        /// </summary>
        private ILogger Logger { get; }

        /// <summary>
        /// Gets TenantTime
        /// </summary>
        private ITenantTime TenantTime { get; }

        /// <summary>
        /// Gets BankLinkRepository
        /// </summary>
        private IBankLinkRepository BankLinkRepository { get; }

        /// <summary>
        /// Gets BankAccountRepository
        /// </summary>
        private IBankAccountRepository BankAccountRepository { get; }

        /// <summary>
        /// Gets BankAccountHistoryRepository
        /// </summary>
        private IBankAccountHistoryRepository BankAccountHistoryRepository { get; }

        /// <summary>
        /// Gets BankTransactionRepository
        /// </summary>
        private IBankTransactionRepository BankTransactionRepository { get; }

        /// <summary>
        /// Gets EventHubClient
        /// </summary>
        private IEventHubClient EventHub { get; }

        private IFinicityProxy Proxy { get; }

        private IPerfectAuditService PerfectAuditService { get; }

        #endregion

        public async Task<IResponseConnection> CreateBook(string entityType, string entityId, RequestConnection connection)
        {
            Logger.Info("Processing the AddBankLinkAndAccounts method in PErfectAudit service");

            var response = new CreateBook();
            response = await PerfectAuditService.CreateBook(entityType, entityId);

            Logger.Info($"Perfect Audit Service getting response successfully");

            var bankLinkId = await BankLinkConversionForPerfectAudit(entityType, entityId, response, connection);

            if (string.IsNullOrWhiteSpace(bankLinkId))
            {
                Logger.Info($"bankLinkId is null or white space");
                throw new InvalidOperationException($"bankLinkId is null or white space");
            }

            Logger.Info($"bank link id created successfully");

            Logger.Info($"Processing the BankLinkCreated Event request payload: entityId { entityId} , entityType: { entityType}");
            //await EventHub.Publish(new BankLinkCreated()
            //{
            //    EntityId = entityId,
            //    BankLinkNumber = bankLinkId,
            //    EntityType = entityType,
            //    ReferenceNumber = Guid.NewGuid().ToString("N")
            //});

            return await Task.Run(() =>
            {
                return new ResponseConnection
                {
                    BankLinkId = bankLinkId,
                    ProviderLinkId = response.response.pk.ToString()
                };
            });

        }

        private async Task<string> BankLinkConversionForPerfectAudit(string entityType, string entityId, CreateBook response, RequestConnection request)
        {
            Logger.Info($"Processing BankLinkConversionForPerfectAudit method request data: {request}");
            IBankLink bankLink = new BankLink();
            bankLink.EntityId = entityId;
            bankLink.CreatedDate = new TimeBucket(TenantTime.Now);
            bankLink.IsDeleted = false;
            //  bankLink.ProviderToken = response.AccessToken;
            bankLink.ProviderBankLinkId = response.response.pk.ToString();
            bankLink.ProviderId = Convert.ToInt32(Provider.Ocrolus);
            //bankLink.ProviderBankId = response.InstitutionId;
            bankLink.ProviderBankName = request.BankName;
            bankLink.FirstName = request.FirstName;
            bankLink.LastName = request.LastName;
            bankLink.UserMailId = request.UserMailId;
            bankLink.Links = new List<ILink>();
            bankLink.Links.Add(new Link { EntityType = entityType, Products = null });
            this.BankLinkRepository.Add(bankLink);
            Logger.Info($"BankLink data inserted successfully for Ocrolus-Perfect Audit , id : {bankLink.Id}");
            return await Task.Run(() =>
            {
                return bankLink.Id;
            });
        }

        public async Task<dynamic> GetTransactions(string book_pk)
        {
            Logger.Info("Processing the GetTransactions method in OcrolusPerfectAudit service");


            var response = await PerfectAuditService.GetTransactionByBookId(book_pk);
            var resultresponse = new TransactionData();
            if (response != null && response.response != null)
                resultresponse = response.response;

            Logger.Info($"Ocrolus Syndication getting response successfully");

            var accountType = Configuration.AccountTypes;
            var linkdata = await BankLinkRepository.GetBankLinkData(book_pk);
            if (linkdata == null)
                throw new ArgumentException($"bank link id does not exist for EntityId: {book_pk}");

            await BankAccountConversionForOcrolus(linkdata, resultresponse, accountType);
            Logger.Info($"Accounts added successfully");


            var accountIds = await TransactionConversionForOcrolus(resultresponse, linkdata);
            accountIds = accountIds.Distinct().ToList();
            Logger.Info($"transactions added successfully");

            if (linkdata != null && accountIds.Any())
            {
                Logger.Info("Processing the GenerateOcrolusCashflow event trigger for the cashflow");
                Logger.Info($"Event publish request payload : entityId {linkdata.EntityId} , Response :{accountIds}");
                await EventHub.Publish("GenerateCashflow", new
                {
                    EntityId = linkdata.EntityId,
                    EntityType = "application",
                    Response = accountIds,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });

                await EventHub.Publish(new WebhookTrigger()
                {
                    EntityId = linkdata.EntityId,
                    BankLinkNumber = linkdata.Id,
                    EntityType = "application",
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
            }
            else
            {
                Logger.Info("Event trigger for the cashflow is not executed...");
            }
            return resultresponse;
        }

        public async Task<CommonResponse> UploadStatement(string entityType, string entityId, List<FileuploadData> statement, string bookPk)
        {
            Logger.Info("Processing the UploadStatement method in OcrolusPerfectAudit service");

            var files = new UploadStatementDetails();

            files.Filedetails = new List<FileuploadData>();
            files.Filedetails = statement;

            var response = await PerfectAuditService.UploadStatement(entityType, entityId, files, bookPk);

            Logger.Info($"Ocrolus Syndication getting response successfully");

            return response;
        }

        public async Task<CommonResponse> UploadStatement(string entityType, string entityId, byte[] statement, string name, string bookPk)
        {
            Logger.Info("Processing the single UploadStatement method in PerfectAudit service");

            var response = await PerfectAuditService.UploadStatement(entityType, entityId, statement,name, bookPk);

            Logger.Info($"UploadStatement (Ocrolus Syndication) getting response successfully");

            return response;
        }
        public async Task<Analytics> GetSummaryDataByBookId(string bookId)
        {
            if (bookId == null)
            {
                throw new PerfectAuditException("BookId is null :" + bookId);
            }
            var response = await PerfectAuditService.GetSummaryDataByBookId(bookId);

            Logger.Info("Summary data:", response);

            return response;
        }

        private async Task BankAccountConversionForOcrolus(IBankLink bankLinkdata, TransactionData response, List<string> accountTypes)
        {
            Logger.Info($"Processing the BankAccountConversionForOcrolus method");

            foreach (var itemdetails in response.bank_accounts)
            {
                var item = itemdetails.Value;
                if (item != null)
                {
                    if (accountTypes.Contains(item.account_type.ToLower()))
                    {
                        var balances = new BalancePeriod();
                        var balancedetails = new List<BalancePeriod>();
                        foreach (var itemperiod in item.periods)
                        {
                            var balanceperiod = new BalancePeriod(itemperiod);
                            balancedetails.Add(balanceperiod);
                        }
                        if (balancedetails.Any())
                        {
                            balances = balancedetails.OrderByDescending(x => x.EndDate).FirstOrDefault();
                        }

                        var accountdetails = await BankAccountRepository.GetAccountByProviderAccountId(Convert.ToString(item.pk));
                        if (accountdetails == null)
                        {
                            IBankAccount bankAccount = new BankAccount();
                            bankAccount.NameOnAccount = item.name;
                            bankAccount.AccountNumber = item.account_number.ToString();
                            if (item.account_number.Length >= 4)
                                bankAccount.AccountNumberMask = item.account_number.Substring(item.account_number.Length - 4, 4);
                            bankAccount.AccountType = item.account_type;
                            bankAccount.AvailableBalance = Convert.ToDouble(balances.EndBalance);
                            bankAccount.CurrentBalance = Convert.ToDouble(balances.EndBalance);
                            bankAccount.BankLinkId = bankLinkdata.Id;
                            bankAccount.IsDeleted = false;
                            bankAccount.IsActive = true;
                            bankAccount.RoutingNumber = null;
                            bankAccount.WireRoutingNumber = null;
                            bankAccount.ProviderAccountId = Convert.ToString(item.pk);
                            bankAccount.CreatedDate = new TimeBucket(TenantTime.Now);
                            bankAccount.UpdatedDate = new TimeBucket(TenantTime.Now);
                            bankAccount.ProviderId = Convert.ToInt32(Provider.Ocrolus);
                            bankAccount.OwnerName = item.account_holder;

                            this.BankAccountRepository.Add(bankAccount);
                            Logger.Info($"bankAccount data inserted for the accountId {item.pk.ToString()}");
                            this.BankAccountHistoryRepository.Add(bankAccount);
                            Logger.Info($"bankAccount history data inserted for the accountId {item.pk.ToString()}");

                            bankLinkdata.ProviderBankId = null;
                            this.BankLinkRepository.Update(bankLinkdata);
                        }
                    }
                }
            }

        }

        private async Task<List<string>> TransactionConversionForOcrolus(TransactionData response, IBankLink banklinkdata)
        {
            Logger.Info($"Processing the TransactionConversionForOcrolus method");

            var accountIds = new List<string>();
            var transactionsList = new List<IBankTransaction>();

            foreach (var item in response.txns)
            {
                IBankTransaction bankTransaction = new BankTransaction();
                accountIds.Add(item.bank_account_pk.ToString());
                bankTransaction.ProviderBankLinkId = banklinkdata.ProviderBankLinkId;
                bankTransaction.TransactionId = item.pk.ToString();
                bankTransaction.TransactionType = null;
                if (!string.IsNullOrEmpty(item.txn_date))
                    bankTransaction.TransactionDate = Convert.ToDateTime(item.txn_date);
                bankTransaction.Description = item.description;
                bankTransaction.Type = Convert.ToDouble(item.amount) > 0 ? "Credit" : "Debit";
                bankTransaction.Amount = Convert.ToDouble(item.amount) * (-1);
                //if (item.Categorization != null && !string.IsNullOrWhiteSpace(item.Categorization.Category))
                //{
                //    string[] category = new string[1];
                //    category[0] = item.Categorization.Category;
                //    bankTransaction.Category = category;
                //    bankTransaction.CategoryId = item.Categorization.Category;
                //}
                bankTransaction.CreatedDate = TenantTime.Now;
                bankTransaction.ProviderAccountId = item.bank_account_pk.ToString();
                bankTransaction.Pending = false;
                bankTransaction.ProviderId = Convert.ToInt32(Provider.Ocrolus);
                transactionsList.Add(bankTransaction);
            }
            // Commented as we need to compute RunningBalance and then store the data
            // await BankTransactionRepository.AddTransactions(transactions);
            this.Logger.Info("item transaction are added");

            /// Compute Running Balance and store TransactionData to database. 
            await ComputeRunningBalance(transactionsList, response, banklinkdata);

            return accountIds;


        }

        private async Task ComputeRunningBalance(List<IBankTransaction> transactionsList, TransactionData response, IBankLink banklinkdata)
        {
            Logger.Info("ComputeRunningBalance started for Ocrolus....");

            if (transactionsList == null || transactionsList.Count < 1)
            {
                Logger.Info("... no transaction data to process. EXIT with no errors.");
                return;
            }

            Logger.Info($".... processing transactions.... ");
            var accountList = transactionsList.Select(x => x.ProviderAccountId).Distinct().ToList<string>();

            var accountdetails = await BankAccountRepository.GetAccounts(banklinkdata.Id);
            foreach (var account in accountList)
            {
                Logger.Info($".... processing for account {account}");

                var trans = transactionsList.Where(x => x.ProviderAccountId == account).OrderByDescending(x => x.TransactionDate);
                
                var accountBalance = accountdetails.FirstOrDefault(x => x.ProviderAccountId == account);
                if(accountBalance == null)
                {
                    Logger.Info($"account is not found for providerAcocountId in transaction in PerfectAuditService");
                    throw new InvalidOperationException($"account is not found for providerAcocountId in transaction ");
                }

                if (accountBalance != null && accountBalance.AvailableBalance != null && accountBalance.AvailableBalance.HasValue)
                {
                    double bal = accountBalance.AvailableBalance.Value;
                    foreach (var record in trans)
                    {
                        record.RunningBalance = bal;
                        bal = bal + (record.Amount);
                    }
                }
                Logger.Info($".... invoke BankTransactionRepository");
                await BankTransactionRepository.AddTransactions(trans);
            }


            Logger.Info("ComputeRunningBalance ended....");
        }

    }
}

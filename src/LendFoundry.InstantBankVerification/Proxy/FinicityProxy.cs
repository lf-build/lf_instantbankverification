﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.InstantBankVerification
{
    public class FinicityProxy : IFinicityProxy
    {

        #region Private Properties

        private IConfiguration Configuration { get; }

        #endregion

        #region Public Constructor

        public FinicityProxy(IConfiguration configuration, ILogger logger)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (configuration.FinicityConfiguration == null)
                throw new ArgumentNullException(nameof(configuration.FinicityConfiguration));
            if (string.IsNullOrWhiteSpace(configuration.FinicityConfiguration.FinicitySyndicationURL))
                throw new ArgumentNullException("finicitySyndicationURL is required", nameof(configuration.FinicityConfiguration.FinicitySyndicationURL));
            Configuration = configuration;
            Logger = logger;
        }
        #endregion

        private ILogger Logger { get; set; }

        public IAccessTokenResponse GetAccessToken(string entityType, string entityId)
        {
            Logger.Info("Inside GetAccessToken");

            if (string.IsNullOrWhiteSpace(Configuration.FinicityConfiguration.GetAccessTokenEndPoint))
                throw new ArgumentNullException("GetAccessTokenEndPoint is required", nameof(Configuration.FinicityConfiguration.GetAccessTokenEndPoint));

            var urlendpoint = Configuration.FinicityConfiguration
                                    .GetAccessTokenEndPoint.Replace("{entitytype}", entityType)
                                    .Replace("{entityid}", entityId);
            var request = new RestRequest(urlendpoint, Method.GET);

            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", Configuration.FinicityConfiguration.AuthorizationToken);

            return ExecuteRequest<AccessTokenResponse>(request);
        }

        public IAddCustomerResponse AddCustomer(string entityType, string entityId, IAddCustomerRequest addCustomerRequest)
        {
            var client = new RestClient(Configuration.FinicityConfiguration.FinicitySyndicationURL);
            if (string.IsNullOrWhiteSpace(Configuration.FinicityConfiguration.AddCustomerEndPoint))
                throw new ArgumentNullException("AddCustomerEndPoint is required", nameof(Configuration.FinicityConfiguration.AddCustomerEndPoint));
            var urlendpoint = Configuration.FinicityConfiguration.AddCustomerEndPoint.Replace("{entitytype}", entityType).Replace("{entityid}", entityId);

            var request = new RestRequest(urlendpoint, Method.POST);

            //  RestClient client = new RestClient(nwuri);
            //  var request = new RestRequest("{entitytype}/{entityid}/accesstoken", Method.POST);
            request.AddUrlSegment(nameof(entityType), entityType);
            request.AddUrlSegment(nameof(entityId), entityId);

            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", Configuration.FinicityConfiguration.AuthorizationToken);
            request.AddJsonBody(addCustomerRequest);
            return ExecuteRequest<AddCustomerResponse>(request);
        }

        public IConnectLinkResponse GenerateConnectLink(string entityType, string entityId, IConnectLinkRequest connectrequest)
        {
            var client = new RestClient(Configuration.FinicityConfiguration.FinicitySyndicationURL);
            if (string.IsNullOrWhiteSpace(Configuration.FinicityConfiguration.GenerateConnectLinkEndPoint))
                throw new ArgumentNullException("GenerateConnectLinkEndPoint is required", nameof(Configuration.FinicityConfiguration.GenerateConnectLinkEndPoint));
            var urlendpoint = Configuration.FinicityConfiguration.GenerateConnectLinkEndPoint.Replace("{entitytype}", entityType).Replace("{entityid}", entityId);

            var request = new RestRequest(urlendpoint, Method.POST);


            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", Configuration.FinicityConfiguration.AuthorizationToken);
            request.AddJsonBody(connectrequest);
            return ExecuteRequest<ConnectLinkResponse>(request);
        }

        public Proxy.Response.ICustomerAccountsResponse GetCustomerAccounts(string entityType, string entityId, ICustomerDetailRequest customerrequest)
        {
            var client = new RestClient(Configuration.FinicityConfiguration.FinicitySyndicationURL);
            if (string.IsNullOrWhiteSpace(Configuration.FinicityConfiguration.GetCustomerAccountsEndPoint))
                throw new ArgumentNullException("GetCustomerAccountsEndPoint is required", nameof(Configuration.FinicityConfiguration.GetCustomerAccountsEndPoint));
            var urlendpoint = Configuration.FinicityConfiguration.GetCustomerAccountsEndPoint.Replace("{entitytype}", entityType).Replace("{entityid}", entityId);

            var request = new RestRequest(urlendpoint, Method.POST);


            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", Configuration.FinicityConfiguration.AuthorizationToken);
            request.AddJsonBody(customerrequest);
            return ExecuteRequest<Proxy.Response.CustomerAccountsResponse>(request);
        }


        public Proxy.Response.ITransactionsResponse GetTransactionsByAccount(string entityType, string entityId, Request.ITransactionRequest transactionrequest)
        {
            var client = new RestClient(Configuration.FinicityConfiguration.FinicitySyndicationURL);
            if (string.IsNullOrWhiteSpace(Configuration.FinicityConfiguration.GetTransactionforAllAccountEndPoint))
                throw new ArgumentNullException("GetTransactionforAllAccountEndPoint is required", nameof(Configuration.FinicityConfiguration.GetTransactionforAllAccountEndPoint));
            var urlendpoint = Configuration.FinicityConfiguration.GetTransactionforAllAccountEndPoint.Replace("{entitytype}", entityType).Replace("{entityid}", entityId);

            var request = new RestRequest(urlendpoint, Method.POST);


            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", Configuration.FinicityConfiguration.AuthorizationToken);
            request.AddJsonBody(transactionrequest);
            return ExecuteRequest<Proxy.Response.TransactionsResponse>(request);
        }

        public Proxy.Response.ITxPushResponse EnableTxPush(string entityType, string entityId, ITxPushRequest txPushrequest)
        {
            var client = new RestClient(Configuration.FinicityConfiguration.FinicitySyndicationURL);
            if (string.IsNullOrWhiteSpace(Configuration.FinicityConfiguration.EnableTxPushEndPoint))
                throw new ArgumentNullException("EnableTxPushEndPoint is required", nameof(Configuration.FinicityConfiguration.EnableTxPushEndPoint));
            var urlendpoint = Configuration.FinicityConfiguration.EnableTxPushEndPoint.Replace("{entitytype}", entityType).Replace("{entityid}", entityId);

            var request = new RestRequest(urlendpoint, Method.POST);


            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", Configuration.FinicityConfiguration.AuthorizationToken);
            request.AddJsonBody(txPushrequest);
            return ExecuteRequest<Proxy.Response.TxPushResponse>(request);
        }

        public IAddTransactionDetailResponse AddTransaction(IAddTransactionDetailRequest transactionDetailRequest)
        {
            if (string.IsNullOrWhiteSpace(Configuration.FinicityConfiguration.AddTestTransactionEndPoint))
                throw new ArgumentNullException("AddCustomerEndPoint is required", nameof(Configuration.FinicityConfiguration.AddTestTransactionEndPoint));
            var urlendpoint = Configuration.FinicityConfiguration.AddTestTransactionEndPoint;

            var request = new RestRequest(urlendpoint, Method.POST);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", Configuration.FinicityConfiguration.AuthorizationToken);
            request.AddJsonBody(transactionDetailRequest);
            return ExecuteRequest<AddTransactionDetailResponse>(request);
        }

        public dynamic LoadHistoricTransactions(string entityType, string entityId, ICustomerDetailRequest customerrequest)
        {

            if (string.IsNullOrWhiteSpace(Configuration.FinicityConfiguration.LoadHistoricTransactionEndPoint))
                throw new ArgumentNullException("AddCustomerEndPoint is required", nameof(Configuration.FinicityConfiguration.LoadHistoricTransactionEndPoint));
            var urlendpoint = Configuration.FinicityConfiguration.LoadHistoricTransactionEndPoint.Replace("{entitytype}", entityType).Replace("{entityid}", entityId);

            var request = new RestRequest(urlendpoint, Method.POST);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", Configuration.FinicityConfiguration.AuthorizationToken);
            request.AddJsonBody(customerrequest);
           
            return ExecuteRequest<dynamic>(request);
        }

        //public IAddCustomerResponse AddCustomer(string finicityAppToken, AddCustomerRequest addCustomerRequest)
        //{
        //    var client = new RestClient(Configuration.APIURL);

        //    string resource = string.Empty;
        //    if (Configuration.IsLive)
        //    {
        //        resource = Configuration.AddCustomerProductionEndpoint;
        //    }
        //    else
        //    {
        //        resource = Configuration.AddCustomerStagingEndpoint;
        //    }
        //    var request = new RestRequest(resource, Method.POST);

        //    request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
        //    request.AddHeader("Finicity-App-Token", finicityAppToken);
        //    request.AddHeader("Content-Type", "application/json");
        //    request.AddHeader("Accept", "application/json");

        //    request.AddParameter("application/json", JsonConvert.SerializeObject(addCustomerRequest), ParameterType.RequestBody);

        //    return ExecuteRequest<AddCustomerResponse>(request);
        //}

        private IRestResponse ExecuteRequest(IRestRequest request)
        {
            Logger.Info("Execute Request - Start");
            Uri baseUri = new Uri(Configuration.FinicityConfiguration.FinicitySyndicationURL);
            Uri myUri = new Uri(baseUri, request.Resource.ToString());
            Logger.Info("===>" + myUri.AbsoluteUri);
            RestClient client;
            request.Resource = string.Empty;

            //if (!string.IsNullOrWhiteSpace(Configuration.ProxyUrl))
            //{
            //    var uri = new Uri($"{Configuration.ProxyUrl}{baseUri.PathAndQuery}");
            //    client = new RestClient(uri);
            //    request.AddHeader("Host", baseUri.Host);
            //}
            //else
            //{
            client = new RestClient(myUri);
           
            if (baseUri.Scheme.ToLower() == "https")
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            }
            //}

            request.RequestFormat = DataFormat.Json;

            var response = client.Execute(request);
            Logger.Info($"Execute Request - Complete with status -{response.StatusCode}");

            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new Exception("Service call failed", response.ErrorException);

            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new Exception(response.Content);

            if ((response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.Created &&
                    response.StatusCode != HttpStatusCode.NoContent && response.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
                    || response.ResponseStatus != ResponseStatus.Completed)
            {
                try
                {
                    Logger.Info($"Inside Error");
                    var error = JsonConvert.DeserializeObject<dynamic>(response.Content);
                    throw new Exception(error.code.ToString() + ":" + error.message.ToString());
                }
                catch (Exception ex)
                {
                    if (ex is Exception)
                    {
                        throw;
                    }
                    throw new Exception(
                        $"Service Call failed. Status {response.ResponseStatus} . Response: {response.Content ?? ""}");
                }
            }

            return response;
        }

        private T ExecuteRequest<T>(IRestRequest request) where T : class
        {
            Logger.Info("Finicity Proxy ExecuteRequest");
            var response = ExecuteRequest(request);

            try
            {
                Logger.Info(response.Content);
                var result = JsonConvert.DeserializeObject<T>(response.Content);
                Logger.Info("Return result");
                return result;
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response.Content, exception);
            }
        }

        private T ExecuteRequestForMFA<T>(IRestRequest request, out string mfaSessionId) where T : class
        {
            var response = ExecuteRequest(request);
            try
            {
                mfaSessionId = string.Empty;

                #region Extract mfaSessionId

                var mfaParameter = response.Headers.FirstOrDefault(x => x.Name.ToLower().Equals("mfa-session"));
                if (mfaParameter != null)
                {
                    mfaSessionId = mfaParameter.Value.ToString();
                }
                #endregion

                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response.Content, exception);
            }
        }


    }
}

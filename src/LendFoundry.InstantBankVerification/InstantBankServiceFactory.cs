﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.Syndication.PlaidIBV;
using LendFoundry.Foundation.Date;
using LendFoundry.Configuration;
using LendFoundry.Syndication.PlaidIBV.Client;
using LendFoundry.EventHub;
using LendFoundry.Clients.DecisionEngine;

namespace LendFoundry.InstantBankVerification
{
    public class InstantBankServiceFactory : IInstantBankServiceFactory
    {
        public InstantBankServiceFactory(IServiceProvider provider) { Provider = provider; }

        private IServiceProvider Provider { get; }

        public IInstantBankVerificationService Create(ITokenReader reader, ITokenHandler handler, ILogger logger)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var configurationService = configurationServiceFactory.Create<Configuration>(Settings.ServiceName, reader);
            var configuration = configurationService.Get();

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var repositoryFactory = Provider.GetService<IIBSRepositoryFactory>();
            var bankLinkRepo = repositoryFactory.CreateBankLinkRepository(reader);
            var bankAccountRepo = repositoryFactory.CreateBankAccountRepository(reader);
            var bankAccountHistoryRepo = repositoryFactory.CreateBankAccountHistoryRepository(reader);
            var bankTransactionRepo = repositoryFactory.CreateBankTransactionRepository(reader);
            var balanceRepo = repositoryFactory.CreateBalanceRepository(reader);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHub = eventHubFactory.Create(reader);

            var tokenReader = Provider.GetService<ITokenReader>();
            var tokenParser = Provider.GetService<ITokenHandler>();

            var decisionServiceFactory = Provider.GetService<IDecisionEngineClientFactory>();
            var decisionService = decisionServiceFactory.Create(reader);

            var plaidUniteServiceFactory = Provider.GetService<IPlaidUniteServiceFactory>();
            var plaidUniteService = plaidUniteServiceFactory.Create(reader, handler, logger);

            var finicityFactory = Provider.GetService<IFinicityServiceFactory>();
            var finicityService = finicityFactory.Create(reader,handler,logger);

            var ocrolusPerfectAuditServiceFactory = Provider.GetService<IOcrolusPerfectAuditServiceFactory>();
            var ocrolusPerfectAuditService = ocrolusPerfectAuditServiceFactory.Create(reader, handler, logger);

            return new InstantBankVerificationService(configuration,
                bankLinkRepo,
                bankAccountRepo,
                bankAccountHistoryRepo,
                bankTransactionRepo,
                logger,
                tenantTime,
                decisionService,
                plaidUniteService,
                eventHub, 
                finicityService,
                ocrolusPerfectAuditService,
                balanceRepo
                );
        }
    }
}

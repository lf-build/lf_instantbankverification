﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.Foundation.Date;
using LendFoundry.Configuration;
using LendFoundry.Syndication.PlaidIBV.Client;
using LendFoundry.EventHub;
using LendFoundry.Clients.DecisionEngine;
#if DOTNET2
using LendFoundry.Syndication.PerfectAudit.Abstractions;
using LendFoundry.Syndication.PerfectAudit.Client;
#else
using LendFoundry.Syndication.PerfectAudit.Abstractions;
using LendFoundry.Syndication.PerfectAudit.Client;
#endif

namespace LendFoundry.InstantBankVerification
{
    public class OcrolusPerfectAuditServiceFactory : IOcrolusPerfectAuditServiceFactory
    {
        public OcrolusPerfectAuditServiceFactory(IServiceProvider provider) { Provider = provider; }

        private IServiceProvider Provider { get; }

        public IOcrolusPerfectAuditService Create(ITokenReader reader, ITokenHandler handler, ILogger logger)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var configurationService = configurationServiceFactory.Create<Configuration>(Settings.ServiceName, reader);
            var configuration = configurationService.Get();

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var repositoryFactory = Provider.GetService<IIBSRepositoryFactory>();
            var bankLinkRepo = repositoryFactory.CreateBankLinkRepository(reader);
            var bankAccountRepo = repositoryFactory.CreateBankAccountRepository(reader);
            var bankAccountHistoryRepo = repositoryFactory.CreateBankAccountHistoryRepository(reader);
            var bankTransactionRepo = repositoryFactory.CreateBankTransactionRepository(reader);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHub = eventHubFactory.Create(reader);

            var tokenReader = Provider.GetService<ITokenReader>();
            var tokenParser = Provider.GetService<ITokenHandler>();

            var plaidClientServiceFactory = Provider.GetService<IPlaidClientFactory>();
            var plaidClientService = plaidClientServiceFactory.Create(reader);

            var decisionEngineFactory = Provider.GetService<IDecisionEngineClientFactory>();
            var decisionEngineService = decisionEngineFactory.Create(reader);

            var finicityproxyFactory = Provider.GetService<IFinicityProxyFactory>();
            var finicityproxy = finicityproxyFactory.Create(reader, logger);

            var perfectAuditClientServiceFactory = Provider.GetService<IPerfectAuditServiceClientFactory>();
            var perfectAuditClientService = perfectAuditClientServiceFactory.Create(reader);
            return new OcrolusPerfectAuditService(logger,
                configuration,
                bankLinkRepo,
                bankAccountRepo,
                bankAccountHistoryRepo,
                bankTransactionRepo,
                eventHub,
                tenantTime,
                finicityproxy, 
                perfectAuditClientService
                );

        }
    }
}

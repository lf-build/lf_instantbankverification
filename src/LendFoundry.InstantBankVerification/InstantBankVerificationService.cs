﻿using LendFoundry.Clients.DecisionEngine;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#if DOTNET2
using LendFoundry.Syndication.PerfectAudit.Abstractions;
#else
using LendFoundry.Syndication.PerfectAudit.Abstractions;
#endif

namespace LendFoundry.InstantBankVerification
{
    #region Constructor

    /// <summary>
    /// InstantBankVerificationService
    /// </summary>
    public class InstantBankVerificationService : IInstantBankVerificationService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InstantBankVerificationService"/> class.
        /// </summary>
        /// <param name="repository">repository</param>
        /// <param name="logger">logger</param>
        /// <param name="tenantTime">tenantTime</param>
        public InstantBankVerificationService
        (
            IConfiguration configuration,
            IBankLinkRepository bankLinkRepository,
            IBankAccountRepository bankAccountRepository,
            IBankAccountHistoryRepository bankAccountHistoryRepository,
            IBankTransactionRepository bankTransactionRepository,
            ILogger logger,
            ITenantTime tenantTime,
            IDecisionEngineService decisionEngineService,
            IPlaidUniteService plaidUniteService,
            IEventHubClient eventHubClient,
            IFinicityService finicityService,
            IOcrolusPerfectAuditService ocrolusPerfectAuditService,
            IBalanceRepository balanceRepository
        )
        {
            if (logger == null) throw new ArgumentException($"{nameof(logger)} is mandatory");
            if (tenantTime == null) throw new ArgumentException($"{nameof(tenantTime)} is mandatory");
            if (configuration == null) throw new ArgumentException($"{nameof(configuration)} is mandatory");

            Configuration = configuration;
            BankLinkRepository = bankLinkRepository;
            BankAccountRepository = bankAccountRepository;
            BankAccountHistoryRepository = bankAccountHistoryRepository;
            BankTransactionRepository = bankTransactionRepository;
            Logger = logger;
            TenantTime = tenantTime;
            PlaidUniteService = plaidUniteService;
            DecisionEngineService = decisionEngineService;
            EventHub = eventHubClient;
            FinicityService = finicityService;
            OcrolusPerfectAuditService = ocrolusPerfectAuditService;
            BalanceRepository = balanceRepository;
        }

        #endregion

        #region Variable Declaration

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Gets BankLinkRepository
        /// </summary>
        private IBankLinkRepository BankLinkRepository { get; }

        /// <summary>
        /// Gets BankAccountRepository
        /// </summary>
        private IBankAccountRepository BankAccountRepository { get; }

        /// <summary>
        /// Gets BankAccountHistoryRepository
        /// </summary>
        private IBankAccountHistoryRepository BankAccountHistoryRepository { get; }


        /// <summary>
        /// Gets BankTransactionRepository
        /// </summary>
        private IBankTransactionRepository BankTransactionRepository { get; }

        /// <summary>
        /// Gets Logger
        /// </summary>
        private ILogger Logger { get; }

        /// <summary>
        /// Gets TenantTime
        /// </summary>
        private ITenantTime TenantTime { get; }

        /// <summary>
        /// Gets plaidUniteService
        /// </summary>
        private IPlaidUniteService PlaidUniteService { get; }

        /// <summary>
        /// Gets DecisionEngineService
        /// </summary>
        private IDecisionEngineService DecisionEngineService { get; }

        /// <summary>
        /// Gets FinicityService
        /// </summary>
        private IFinicityService FinicityService { get; }

        /// <summary>
        /// Gets EventHub 
        /// </summary>
        private IEventHubClient EventHub { get; }

        private IOcrolusPerfectAuditService OcrolusPerfectAuditService { get; }
        private IBalanceRepository BalanceRepository { get; }

        #endregion

        #region Generic
        public async Task<IResponseConnection> BankLinking(string entityType, string entityId, IRequestConnection request)
        {
            Logger.Info($"Processing the method BankLinking in service");
            IResponseConnection result = null;

            if (string.IsNullOrWhiteSpace(request.ProviderName))
            {
                Logger.Info($"Provider Name is null or whiteSpace : {request.ProviderName}");
                throw new ArgumentException($"Provider Name is null or whiteSpace");
            }

            var provider = UtilityHelper.TryParseEnumValue<Provider>(request.ProviderName);
            Logger.Info($"Provider Name : {request.ProviderName}");
            switch (provider)
            {
                case Provider.Plaid:
                    Logger.Info($"In Plaid Provider...");
                    result = await PlaidUniteService.AddBankLinkAndAccounts(entityType, entityId, request);
                    break;
                default:
                    break;
            }



            return result;
        }

        /// <summary>
        /// GetAccounts
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entityType"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<object> GetAccounts(string entityType, string entityId, IRequestConnection request)
        {
            Logger.Info($"Processing the method GetAccounts...");
            var bankLinkId = await this.BankLinkRepository.GetBankLinkId(request.ProviderToken);
            if (string.IsNullOrWhiteSpace(bankLinkId))
            {
                Logger.Info($"ProviderToken is Invalid or Not exists, not getting bankLinkId");
                throw new ArgumentException($"ProviderToken is Invalid or Not exists : {request.ProviderToken}");
            }

            return await this.BankAccountRepository.GetAccounts(bankLinkId);
        }

        /// <summary>
        /// AccountSync
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entityType"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<object> AccountSync(string entityType, string entityId, IRequestConnection request)
        {
            Logger.Info($"Processing the method AccountSync in service...");
            if (string.IsNullOrWhiteSpace(request.ProviderName))
            {
                Logger.Info($"Provider Name is null or whiteSpace");
                throw new ArgumentException($"Provider Name is null or whiteSpace");
            }

            Logger.Info($"Provider Name {request.ProviderName}");
            var provider = UtilityHelper.TryParseEnumValue<Provider>(request.ProviderName);
            switch (provider)
            {
                case Provider.Plaid:
                    Logger.Info($"In Plaid Service call...");
                    return await PlaidUniteService.AccoutSync(entityType, entityId, request);
                    break;
                default:
                    break;
            }
            return new object();
        }

        /// <summary>
        /// GetCashflowPayload
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entityType"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public async Task<ITransactionCashflowResponse> GetTransactionForCashflow(string entityType, string entityId, string filter, string accountId)
        {
            Logger.Info($"Processing the method GetCashflowPayload...");
            if (string.IsNullOrWhiteSpace(accountId))
            {
                throw new ArgumentException($"AccountId is null or empty");
            }

            if (!await this.BankAccountRepository.AccountExists(accountId))
            {
                throw new ArgumentException($"Account is not exists");
            }

            ITransactionCashflowResponse response = new TransactionCashflowResponse
            {
                EntityId = entityId,
                EntityType = entityType,
                FilterType = filter,
                EventData = new EventData
                {
                    Accounts = null,
                    ReferenceNumber = null,
                    Request = null,
                    Transactions = await GetTransactionInformation(entityType, entityId, filter, accountId)
                }
            };

            return response;
        }

        /// <summary>
        /// GetTransactionForCashflow
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entityType"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public async Task<ITransactionCashflowResponse> GetTransactionForCashflow(string entityType, string entityId, string accountId, string fromDate, string toDate)
        {
            Logger.Info($"Processing the method GetTransactionForCashflow with Fromdate and Todate...");
            if (string.IsNullOrWhiteSpace(accountId))
            {
                throw new ArgumentException($"AccountId is null or empty");
            }

            if (!await this.BankAccountRepository.AccountExists(accountId))
            {
                throw new ArgumentException($"Account is not exists");
            }

            ITransactionCashflowResponse response = new TransactionCashflowResponse
            {
                EntityId = entityId,
                EntityType = entityType,
                FilterType = string.Empty,
                EventData = new EventData
                {
                    Accounts = null,
                    ReferenceNumber = null,
                    Request = null,
                    Transactions = await GetTransactionInformation(entityType, entityId, accountId, fromDate, toDate)
                }
            };

            return response;
        }

        /// <summary>
        /// GetCashflowPayload
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entityType"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public async Task<ICashflowPayload> GetCashflowPayload(string entityType, string entityId, string accountId)
        {
            Logger.Info($"Processing the method GetCashflowPayload...");
            if (string.IsNullOrWhiteSpace(accountId))
            {
                throw new ArgumentException($"AccountId is null or empty");
            }

            if (!await this.BankAccountRepository.AccountExists(accountId))
            {
                throw new ArgumentException($"Account is not exists");
            }

            ICashflowPayload response = new CashflowPayload
            {
                EntityId = entityId,
                EntityType = entityType,
                FilterType = null,
                FromDate = null,
                ToDate = null,
                EventData = new EventData
                {
                    Accounts = await GetAccountInformation(entityType, entityId, accountId),
                    ReferenceNumber = null,
                    Request = null,
                    Transactions = null
                }
            };
            return response;

        }

        /// <summary>
        /// GetBankAccountInformation
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public async Task<IBankAccountResponse> GetBankAccountInformation(string entityType, string entityId, string accountId)
        {
            Logger.Info($"Processing the method GetBankAccountInformation...");

            if (string.IsNullOrWhiteSpace(entityId))
            {
                throw new ArgumentException($"entityId is null or empty");
            }
            if (string.IsNullOrWhiteSpace(accountId))
            {
                throw new ArgumentException($"AccountId is null or empty");
            }

            if (!await this.BankAccountRepository.AccountExists(accountId))
            {
                throw new ArgumentException($"Account is not exists");
            }

            var accountData = await this.BankAccountRepository.GetAccountByProviderAccountId(accountId);
            var bankLinkData = await this.BankLinkRepository.GetBankLinkingData(entityId, accountData.BankLinkId);

            return new BankAccountResponse
            {
                AccountId = accountData.ProviderAccountId,
                AccountNumber = accountData.AccountNumber,
                AccountType = accountData.AccountType,
                AvailableBalance = accountData.AvailableBalance,
                BalanceAsOfDate = accountData.UpdatedDate,
                BankName = bankLinkData.ProviderBankName,
                CurrentBalance = accountData.CurrentBalance,
                RoutingNumber = accountData.RoutingNumber,
                Source = Enum.GetName(typeof(Provider), accountData.ProviderId)
            };

        }

        public async Task<IBankAccountWrapperResponse> GetAccount(string entityType, string entityId, string accountId)
        {
            Logger.Info($"Processing the method GetCashflowPayload...");
            if (string.IsNullOrWhiteSpace(accountId))
            {
                throw new ArgumentException($"AccountId is null or empty");
            }

            if (!await this.BankAccountRepository.AccountExists(accountId))
            {
                throw new ArgumentException($"Account is not exists");
            }

            var response = await this.BankAccountRepository.GetAccountByProviderAccountId(accountId);
            return new BankAccountWrapperResponse
            {
                AccountId = response.ProviderAccountId,
                AvailableBalance = response.AvailableBalance,
                CurrentBalance = response.CurrentBalance,
                Mask = response.AccountNumberMask
            };
        }

        public async Task<List<string>> GetAccountIds(string entityId, string bankLinkId)
        {
            Logger.Info($"Processing the method GetAccountIds...");
            if (string.IsNullOrWhiteSpace(entityId))
            {
                throw new ArgumentException($"entityId is null or empty");
            }

            var bankLinkData = await this.BankLinkRepository.GetBankLinkingData(entityId, bankLinkId);
            if (bankLinkData == null)
            {
                throw new ArgumentException($"BankLink data is not exists");
            }

            return await this.BankAccountRepository.GetAccountIds(bankLinkId);
        }

        public async Task<List<IBankTransaction>> GetTransactions(string accountId)
        {
            Logger.Info($"Processing the method GetTransactions...");
            if (string.IsNullOrWhiteSpace(accountId))
            {
                throw new ArgumentException($"accountId is null or empty");
            }
            var result = await this.BankTransactionRepository.GetTransactions(accountId);

            return result.ToList<IBankTransaction>();
        }


        /// <summary>
        /// GetCashflowPayload
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entityType"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        //public async Task GetCashflowResult(string entityId, string entityType, string accountId)
        //{
        //    await CashflowService.GetCashflowResult(entityId, entityType, accountId);
        //}

        /// <summary>
        /// DeleteConnection
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entityType"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<bool> DeleteConnection(string entityType, string entityId, IRequestConnection request)
        {
            Logger.Info($"Processing the method DeleteConnection...");
            var result = false;

            if (string.IsNullOrWhiteSpace(request.ProviderName))
            {
                Logger.Error("Provider Name is null or whiteSpace");
                throw new ArgumentException($"Provider Name is null or whiteSpace");
            }

            Logger.Error($"Provider Name : {request.ProviderName}");
            var provider = UtilityHelper.TryParseEnumValue<Provider>(request.ProviderName);
            switch (provider)
            {
                case Provider.Plaid:
                    Logger.Info($"Processing the Plaid service for DeleteConnection");
                    result = await PlaidUniteService.DeleteConnection(entityType, entityId, request);
                    break;
                default:
                    break;
            }
            return result;
        }

        /// <summary>
        /// AddManualDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task AddManualDetails(string entityType, string entityId, IRequestManualBankLink request)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            if (request == null)
                throw new ArgumentNullException($"{nameof(request)} is nullable");

            var bankLinkResult = await ConvertManualData(entityType, entityId, request);

            if (bankLinkResult == null && string.IsNullOrWhiteSpace(bankLinkResult.Id))
                throw new ArgumentException($"Insertion Issue on banklink");

            var accountData = await ConversionManualAccount(bankLinkResult.Id, request);

            if (request.ManualType.ToUpper() == ManualType.CSVUplaod.ToString().ToUpper())
            {
                await ExtractCSVData(entityId, accountData, bankLinkResult, request);

                //// action trigger for Bank statement upload
                await EventHub.Publish("BankStatementUploaded", new
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
            }

            if (!string.IsNullOrWhiteSpace(bankLinkResult.Id) && accountData != null)
            {
                await EventHub.Publish(new GenerateCashflow()
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = new List<string>() { accountData.ProviderAccountId },
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });

                await EventHub.Publish(new ManualUploadTrigger()
                {
                    EntityId = entityId,
                    BankLinkNumber = bankLinkResult.Id,
                    EntityType = entityType,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
            }
        }

        //// NOT IN USE
        /// <summary>
        /// UploadManualCsv
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entityType"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        public async Task<object> UploadManualCsv(string entityType, string entityId, PostedFileDetails file)
        {
            EnsureFileTypeIsValid(file);

            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            var bankLinkData = await this.BankLinkRepository.GetBankLinkDataByEntityId(entityId);
            if (string.IsNullOrWhiteSpace(bankLinkData.Id))
                throw new ArgumentException($"{nameof(bankLinkData.Id)} is not found or Exists");

            var accountData = await this.BankAccountRepository.GetAccount(bankLinkData.Id);

            if (accountData == null)
                throw new ArgumentException($"{nameof(accountData)} is not found or Exists");

            var CSVCashflowRequest = new CSVRequest
            {
                providerAccountId = accountData.ProviderAccountId,
                accountNumber = accountData.AccountNumber,
                accountType = accountData.AccountType,
                fileContent = "",
                fileName = file.FileName,
                instituteName = bankLinkData.ProviderBankName
            };

            var lstBankStatement = this.FileConversionToBankStatements(file);

            var payload = new { data = lstBankStatement, details = CSVCashflowRequest };

            var ruleResult = DecisionEngineService.Execute<dynamic, ManualTemplateResponse>("ExtractCSVData", new { payload = payload });
            if (ruleResult != null)
            {
                // Insert Transaction into db and Update accounts 
                //await UpdateManualAccounts(bankLinkData, ruleResult);
                await ManualTransactionInsertion(bankLinkData, entityId, ruleResult);

                await EventHub.Publish(new ManualUploadTrigger()
                {
                    EntityId = entityId,
                    BankLinkNumber = bankLinkData.Id,
                    EntityType = entityType,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
            }

            return ruleResult;
        }

        #endregion


        /// <summary>
        /// GetGivenAccountDetails
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entityType"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<bool> AddBank(string entityType, string entityId, IRequestAddBank request)
        {
            Logger.Info($"Processing the method AddBank...");
            if (string.IsNullOrWhiteSpace(request.Token))
            {
                throw new ArgumentException($"Token_Required");
            }

            if (string.IsNullOrWhiteSpace(request.ItemId))
            {
                throw new ArgumentException($"ItemId_Required");
            }

            if (await BankLinkRepository.IsBankLinked(request.ItemId, request.Token))
            {
                throw new ArgumentException($"Duplicate_Token");
            }

            if (request.AccountType == null)
            {
                request.AccountType = this.Configuration.AccountTypes;
            }

            var result = await PlaidUniteService.AddBank(entityType, entityId, request);
            return result;
        }

        /// <summary>
        /// GetGivenAccountDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAccount(string entityType, string entityId, IRequestAddBank request)
        {
            Logger.Info($"Processing the method DeleteAccount...");
            if (string.IsNullOrWhiteSpace(request.ItemId))
            {
                throw new ArgumentException($"ItemId_Required");
            }
            return await PlaidUniteService.DeleteAccount(request);
        }

        /// <summary>
        /// PullTransaction
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task PullTransaction(IRequestTransaction request)
        {
            Logger.Info($"Processing the method PullTransaction...");
            if (string.IsNullOrWhiteSpace(request.ItemId))
            {
                throw new ArgumentException($"ItemId_Required");
            }

            if (string.IsNullOrWhiteSpace(request.WebhookCode))
            {
                throw new ArgumentException($"WebhookCode_Required");
            }

            await PlaidUniteService.PullTransactionsForPbgp(request);
        }

        public async Task<List<IBankTransaction>> GetTransactions(string entityType, string entityId, string accountId, string fromDate, string toDate)
        {
            DateTime fDate = DateTime.Parse(fromDate);
            DateTime tDate = Convert.ToDateTime(toDate).AddDays(1); //added 1 day as it is not taking the last day transaction when time is greater than 00:00
            return await BankTransactionRepository.GetTransactions(entityType, entityId, accountId, fDate, tDate.Date);
        }

        /// <summary>
        /// UpdateBankAccount
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task UpdateBankAccount(string entityType, string entityId, IBankAccountRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            if (request == null)
                throw new ArgumentNullException($"{nameof(request)} is nullable");

            //if (request.BalanceAsOfDate == null)
            //{
            //    request.BalanceAsOfDate = new TimeBucket(TenantTime.Now.UtcDateTime);
            //}

            var bankAccount = await BankAccountRepository.GetAccountByProviderAccountId(request.AccountId);

            if (bankAccount != null)
            {
                bankAccount.AccountNumber = request.AccountNumber;
                bankAccount.AccountNumberMask = FormatAccountNumber(request.AccountNumber);
                bankAccount.AccountType = request.AccountType;
                bankAccount.AvailableBalance = request.AvailableBalance;
                bankAccount.CurrentBalance = request.CurrentBalance;
                bankAccount.UpdatedDate = new TimeBucket(TenantTime.Now);

                BankAccountRepository.Update(bankAccount);

                await EventHub.Publish("UpdateCashFlow",new 
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = new List<string>() { bankAccount.ProviderAccountId ,request.Version},
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
            }
        }

        /// <summary>
        /// UpdateRunningBalance
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="ProviderAccountId"></param>
        /// <returns></returns>
        public async Task UpdateRunningBalance()
        {           

            Logger.Info("UpdateRunningBalance started in ibv service....");

            var updatedetails = await BalanceRepository.GetProviderAccountIds();

            foreach (var item in updatedetails)
            {
                var bankAccount = await BankAccountRepository.GetAccountByProviderAccountId(item.ProviderAccountId);

                if (bankAccount == null)
                {
                    //throw new ArgumentException($"bank account is not found for {nameof(item.ProviderAccountId)}");
                    Logger.Info($"bank account is not found for {item.ProviderAccountId}");
                }
                else
                {

                    var transactionsList = await BankTransactionRepository.GetTransactions(item.ProviderAccountId);

                    if (transactionsList == null || transactionsList.Count < 1)
                    {
                        Logger.Info("... no transaction data to process. EXIT with no errors.");
                        // return;
                    }
                    else
                    {

                        Logger.Info($".... processing UpdateRunningBalance for account {bankAccount.ProviderAccountId}");

                        var trans = transactionsList.OrderByDescending(x => x.TransactionDate);
                        var accountBalance = bankAccount.CurrentBalance;

                        if (accountBalance != null && accountBalance.HasValue)
                        {
                            double bal = accountBalance.Value;
                            foreach (var record in trans)
                            {
                                record.RunningBalance = bal;
                                bal = bal + (record.Amount);
                                await BankTransactionRepository.UpdateTransaction(record);
                            }
                            await BalanceRepository.UpdateAccountDetails(bankAccount.ProviderAccountId);
                        }
                        Logger.Info($".... processing end for account {bankAccount.ProviderAccountId}");
                    }
                    
                }

               
            }
            

            Logger.Info("UpdateRunningBalance ended....");
        }

        /// <summary>
        /// GetBankAccountDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public async Task<IBankAccount> GetBankAccountDetails(string entityType, string entityId, string accountId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            var bankLinks = await this.BankLinkRepository.GetBankLinksByEntityId(entityId);

            if (bankLinks == null)
            {
                return null;
            }

            var bankLinkIds = bankLinks.Select(x => x.Id).ToList<string>();
            return await BankAccountRepository.GetAccountDetails(bankLinkIds, accountId);
        }

        /// <summary>
        /// GetBankAccountDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public async Task<IList<IBankAccount>> GetAllAccountDetails(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            var bankLinks = await this.BankLinkRepository.GetBankLinksByEntityId(entityId);

            if (bankLinks == null)
            {
                return null;
            }

            var bankLinkIds = bankLinks.Select(x => x.Id).ToList<string>();
            return await BankAccountRepository.GetAccountsByEntityId(bankLinkIds);
        }


        /// <summary>
        /// AddAccountPreference
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="accountPreference"></param>
        /// <returns></returns>
        public async Task<bool> AddAccountPreference(string entityType, string entityId, IAccountPreferenceRequest accountPreference)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (accountPreference == null)
                throw new ArgumentNullException(nameof(accountPreference));
            try
            {
                Logger.Info("Started Execution for AddAccountPreference at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                var data = new AccountPreference(accountPreference);
                var bankLinks = await this.BankLinkRepository.GetBankLinksByEntityId(entityId);

                if (bankLinks == null)
                {
                    return false;
                }

                var bankLinkIds = bankLinks.Select(x => x.Id).ToList<string>();
                
                if (data.IsCashflowAccount.HasValue && data.IsCashflowAccount.Value == true)
                {
                   
                    var ack = await BankAccountRepository.ResetAccountPreference(bankLinkIds, AccountType.Cashflow);

                    await EventHub.Publish("CashflowAccountSelected", new
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = new { dataAttributeName = accountPreference.AccountID },
                        Request = entityId,
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });
                }
                else if (data.IsFundingAccount.HasValue && data.IsFundingAccount.Value == true)
                {
                    var account = await BankAccountRepository.GetAccountDetails(bankLinkIds, data.AccountID);
                    //await DataAttributesService.SetAttribute(entityType, entityId, "fundingAccount", account);
                    await BankAccountRepository.ResetAccountPreference(bankLinkIds, AccountType.Funding);
                    await EventHub.Publish("FundingAccountSelected", new
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = new { dataAttributeName = accountPreference.AccountID },
                        Request = entityId,
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });
                }
                Logger.Info("Completed Execution for AddAccountPreference at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                return await BankAccountRepository.UpdateAccountPreference(data, accountPreference.AccountVersion);
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing AddAccountPreference Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message);
                throw;
            }
        }

        public async Task<IAccountTypeResponse> GetAccountByType(string entityType, string entityId, IAccountTypeRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var lstResult = new AccountTypeResponse();
            var bankLinks = await this.BankLinkRepository.GetBankLinksByEntityId(entityId);

            if (bankLinks == null)
            {
                return null;
            }

            //// TODO : once the cashflow manual trigger done : pass accountId
            var bankLinkFirstData = bankLinks.FirstOrDefault();
            if (bankLinkFirstData != null)
            {
                lstResult.BankName = bankLinkFirstData.ProviderBankName;
                lstResult.NameOnAccount = $"{bankLinkFirstData.FirstName} {bankLinkFirstData.LastName}";
            }

            var bankLinkIds = bankLinks.Select(x => x.Id).ToList<string>();
            try
            {
                Logger.Info("Started Execution for GetAccountByType at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);

                lstResult.accounts = new List<IBankAccount>();
                if (request.IsCashflowAccount.HasValue && request.IsCashflowAccount.Value == true)
                {
                    try
                    {
                        var cashflowAccount = await BankAccountRepository.GetCashflowAccount(bankLinkIds);
                        if (cashflowAccount != null)
                        {
                            lstResult.accounts.Add(cashflowAccount);
                        }
                    }
                    catch
                    {
                    }
                }
                if (request.IsFundingAccount.HasValue && request.IsFundingAccount.Value == true)
                {
                    try
                    {
                        var fundingAccount = await BankAccountRepository.GetFundingAccount(bankLinkIds);
                        if (fundingAccount != null)
                        {
                            lstResult.accounts.Add(fundingAccount);
                        }
                    }
                    catch { }
                }
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetAccountByType Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + exception.StackTrace);
            }
            Logger.Info("Completed Execution for GetAccountByType at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            return lstResult;
        }

        /// <summary>
        /// AddBankLink
        /// </summary>
        /// <param name="bankLinkId"></param>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public async Task AddBankLink(string bankLinkId, string entityType, string entityId)
        {
            var bankLinkData = await BankLinkRepository.Get(bankLinkId);

            if (bankLinkData == null)
            {
                Logger.Info($"BankLink data not found for : {bankLinkId}");
                return;
            }

            if (bankLinkData.Links.Any())
            {
                bankLinkData.Links.Add(new Link
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Products = null
                });
            }
            else
            {
                bankLinkData.Links = new List<ILink>();
                bankLinkData.Links.Add(new Link
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Products = null
                });
            }

            BankLinkRepository.Update(bankLinkData);
            Logger.Info($"BankLink data added for : {bankLinkId}");
        }

        #region Filter Methods

        /// <summary>
        /// GetBankLinkData
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entityType"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<IFilterBankLinkView> GetBankLinkFilterViewData(string entityId, string bankLinkId)
        {
            Logger.Info($"Processing the method GetBankLinkData...");
            var bankLinkData = await this.BankLinkRepository.GetBankLinkingData(entityId, bankLinkId);

            if (bankLinkData == null)
            {
                throw new ArgumentException($"bankLinkData not found for : {entityId}");
            }

            var name = bankLinkData.FirstName;
            if (!string.IsNullOrWhiteSpace(bankLinkData.LastName))
            {
                name = ($"{bankLinkData.FirstName} {bankLinkData.LastName}");
            }

            return new FilterBankLinkView
            {
                Name = name,
                UserMailId = bankLinkData.UserMailId,
                BankLinkId = bankLinkData.Id,
                EntityId = bankLinkData.EntityId,
                IsDeleted = bankLinkData.IsDeleted,
                ProviderBankId = bankLinkData.ProviderBankId,
                ProviderBankName = bankLinkData.ProviderBankName,
                ProviderId = bankLinkData.ProviderId,
                LinkedDate = bankLinkData.CreatedDate
            };
        }

        /// <summary>
        /// GetAccounts
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entityType"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<List<IFilterAccountDataView>> GetAccoutFilterData(string bankLinkId)
        {
            Logger.Info($"Processing the method GetAccoutData...");
            var accountData = await this.BankAccountRepository.GetAccounts(bankLinkId);

            var filterAccountDataViewList = new List<IFilterAccountDataView>();

            foreach (var account in accountData)
            {
                var filterAccountDataView = new FilterAccountDataView();

                filterAccountDataView.ProviderAccountId = account.ProviderAccountId;
                filterAccountDataView.IsActive = account.IsActive;
                filterAccountDataView.IsDeleted = account.IsDeleted;
                filterAccountDataView.OwnerName = account.OwnerName;
                filterAccountDataView.RoutingNumber = account.RoutingNumber;
                filterAccountDataView.WireRoutingNumber = account.WireRoutingNumber;
                filterAccountDataView.CurrentBalance = account.CurrentBalance;
                filterAccountDataView.AccountNumberMask = account.AccountNumberMask;
                filterAccountDataView.NameOnAccount = account.NameOnAccount;
                filterAccountDataView.AccountNumber = account.AccountNumber;
                filterAccountDataView.AccountType = account.AccountType;
                filterAccountDataView.AvailableBalance = account.AvailableBalance;
                filterAccountDataView.BankTransactions = await this.BankTransactionRepository.GetTransactions(account.ProviderAccountId);

                filterAccountDataViewList.Add(filterAccountDataView);
            }
            return filterAccountDataViewList;
        }

        #endregion


        #region Private Methods

        private T ExecuteRequest<T>(string response) where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(response);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response, exception);
            }
        }
        private async Task<IBankAccount> ConversionManualAccount(string bankLinkId, IRequestManualBankLink request)
        {
            Logger.Info($"Processing the BankAccountConversionForPlaid method");
            return await Task.Run(() =>
            {
                IBankAccount bankAccount = new BankAccount();
                bankAccount.NameOnAccount = null;
                bankAccount.AccountNumberMask = FormatAccountNumber(request.AccountNumber);
                bankAccount.AccountType = request.AccountType;
                bankAccount.AvailableBalance = request.AvailableBalance;
                bankAccount.CurrentBalance = request.CurrentBalance;
                bankAccount.BankLinkId = bankLinkId;
                bankAccount.IsDeleted = false;
                bankAccount.IsActive = true;
                bankAccount.ProviderAccountId = Guid.NewGuid().ToString("N");
                bankAccount.CreatedDate = new TimeBucket(TenantTime.Now);
                bankAccount.UpdatedDate = new TimeBucket(TenantTime.Now);
                bankAccount.ProviderId = Convert.ToInt32(Provider.Manual);
                bankAccount.OwnerName = request.FirstName;
                bankAccount.AccountNumber = request.AccountNumber;
                bankAccount.RoutingNumber = request.RoutingNumber;
                bankAccount.WireRoutingNumber = request.WireRoutingNumber;

                this.BankAccountRepository.Add(bankAccount);
                Logger.Info($"bankAccount data inserted for the accountId {request.AccountNumber}");
                return bankAccount;
            });
        }

        private void EnsureFileTypeIsValid(PostedFileDetails file)
        {
            if (file == null)
            {
                throw new ArgumentException($"file not found");
            }

            string FileExtension = Path.GetExtension(file.FileName);

            if (!Array.Exists(Configuration.AcceptedFileIn, ext => ext == FileExtension))
            {
                throw new ArgumentException($"CSV file not found");
            }

            if (file.Content.Length > Configuration.MaxUploadFileSizeLimit * 1048576)
            {
                throw new ArgumentException($"file size should be less than {Configuration.MaxUploadFileSizeLimit} MB");
            }
        }


        /// <summary>
        /// ExtractCSVData
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="accountData"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        private async Task ExtractCSVData(string entityId, IBankAccount accountData, IBankLink bankLinkData, IRequestManualBankLink request)
        {
            try
            {
                var CSVCashflowRequest = new CSVRequest
                {
                    providerAccountId = accountData.ProviderAccountId,
                    accountNumber = accountData.AccountNumber,
                    accountType = accountData.AccountType,
                    fileContent = "",
                    fileName = request.FileName,
                    instituteName = request.BankName
                };

                var lstBankStatement = this.Base64ToBankStatements(request.FileContent);

                var payload = new { data = lstBankStatement, details = CSVCashflowRequest };

                var ruleResult = DecisionEngineService.Execute<dynamic, ManualTemplateResponse>("ExtractCSVDataIbv", new { payload = payload });
                if (ruleResult != null)
                {
                    Logger.Info($"ExtractCSVData Rule is executed successfully");
                    await UpdateManualAccounts(bankLinkData, ruleResult);
                    await ManualTransactionInsertion(bankLinkData, entityId, ruleResult);
                }
            }
            catch (Exception ex)
            {
                Logger.Error($"Error in the method ExtractCSVData Rule : {ex}");
                throw new ArgumentException($"{ex}");
            }
        }

        private async Task<IBankLink> ConvertManualData(string entityType, string entityId, IRequestManualBankLink request)
        {
            IBankLink bankLink = new BankLink();

            bankLink.FirstName = request.FirstName;
            bankLink.LastName = request.LastName;
            bankLink.UserMailId = request.UserMailId;
            bankLink.EntityId = entityId;
            bankLink.ProviderBankName = request.BankName;
            bankLink.ProviderId = Convert.ToInt32(Provider.Manual);
            bankLink.ProviderBankId = null;
            bankLink.ProviderBankLinkId = $"{entityId}_{Guid.NewGuid().ToString("N")}";
            bankLink.ProviderToken = Guid.NewGuid().ToString("N");
            bankLink.CreatedDate = new TimeBucket(TenantTime.Now);
            ILink link = new Link { EntityType = entityType, Products = null };
            bankLink.Links = new List<ILink>();
            bankLink.Links.Add(link);
            this.BankLinkRepository.Add(bankLink);
            Logger.Info($"BankLink data inserted successfully , id : {bankLink.Id}");
            return await Task.Run(() =>
            {
                return bankLink;
            });

        }

        /// <summary>
        /// ManualTransactionInsertion
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="manualResponse"></param>
        /// <returns></returns>
        private async Task ManualTransactionInsertion(IBankLink bankLinkData, string entityId, IManualTemplateResponse manualResponse)
        {
            if (manualResponse != null)
            {
                var transactions = new List<IBankTransaction>();
                foreach (var item in manualResponse.Transactions)
                {
                    IBankTransaction bankTransaction = new BankTransaction();
                    bankTransaction.ProviderBankLinkId = bankLinkData.ProviderBankLinkId;
                    bankTransaction.TransactionId = Guid.NewGuid().ToString("N");
                    bankTransaction.TransactionType = item.Description;
                    bankTransaction.TransactionDate = (DateTime)item.TransactionDate;
                    bankTransaction.Description = item.Description;
                    bankTransaction.Amount = item.Amount;
                    bankTransaction.Type = item.Amount > 0 ? "Debit" : "Credit";
                    bankTransaction.Category = item.Categories;
                    bankTransaction.CategoryId = item.CategoryId;
                    bankTransaction.CreatedDate = TenantTime.Now;
                    bankTransaction.ProviderAccountId = item.ProviderAccountId;
                    bankTransaction.Location = null;
                    bankTransaction.PaymentMeta = null;
                    bankTransaction.Pending = item.Pending;
                    bankTransaction.ProviderId = Convert.ToInt32(Provider.Manual);
                    transactions.Add(bankTransaction);
                }

                Logger.Info($".... processing transactions for Compute Running balance");

                var trans = transactions.OrderByDescending(x => x.TransactionDate);

                foreach (var accountItem in manualResponse.Accounts)
                {
                    var accountBalance = manualResponse.Accounts.FirstOrDefault(x => x.ProviderAccountId == accountItem.ProviderAccountId);

                    if (accountBalance != null && accountBalance.AvailableBalance != null)
                    {
                        //// For Initial Update get latest balances from Plaid
                        double? bal = accountBalance.AvailableBalance;
                        foreach (var record in trans)
                        {
                            record.RunningBalance = bal;
                            bal = bal + (record.Amount);
                        }
                    }
                }

                Logger.Info($".... invoke BankTransactionRepository");
                await BankTransactionRepository.AddTransactions(trans);
                this.Logger.Info("item transaction are added");
            }
        }

        /// <summary>
        /// UpdateManualAccounts
        /// </summary>
        /// <param name="bankLinkData"></param>
        /// <param name="manualResponse"></param>
        /// <returns></returns>
        private async Task UpdateManualAccounts(IBankLink bankLinkData, IManualTemplateResponse manualResponse)
        {
            if (manualResponse.Accounts != null)
            {
                var accounts = await this.BankAccountRepository.GetAccounts(bankLinkData.Id);
                foreach (var account in manualResponse.Accounts)
                {
                    var matchAccount = accounts.FirstOrDefault(x => x.ProviderAccountId == account.ProviderAccountId);

                    if (matchAccount != null)
                    {
                        matchAccount.AvailableBalance = account.AvailableBalance;
                        matchAccount.CurrentBalance = account.CurrentBalance;
                        matchAccount.UpdatedDate = new TimeBucket(TenantTime.Now); ;
                        var flag = await this.BankAccountRepository.UpdateAccount(matchAccount);

                        if (!flag)
                        {
                            matchAccount.Id = null;
                            matchAccount.CreatedDate = new TimeBucket(TenantTime.Now);
                            matchAccount.UpdatedDate = null;
                            BankAccountHistoryRepository.Add(matchAccount);
                        }
                    }
                }
            }
        }


        //// NOT IN USE 
        /// <summary>
        /// FileConversionToBankStatements
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private List<BankStatement> FileConversionToBankStatements(PostedFileDetails file)
        {
            StreamReader reader = new StreamReader(file.Content);
            string objCSVJson = reader.ReadToEnd();

            var allLines = objCSVJson.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            var header = allLines[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            var csvData = allLines.Skip(1)
                           .Select(l => l.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                         .Select((s, i) => new { s, i })
                                         .ToDictionary(x => header[x.i], x => x.s));

            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;
            settings.StringEscapeHandling = StringEscapeHandling.Default;

            var objCSVResponse = JsonConvert.SerializeObject(csvData, settings);

            var lstBankStatement = ExecuteRequest<List<BankStatement>>(objCSVResponse);
            return lstBankStatement;
        }

        /// <summary>
        /// FileConversionToBankStatements
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private List<BankStatement> Base64ToBankStatements(string fileContent)
        {
            byte[] data = Convert.FromBase64String(fileContent);
            string objCSVJson = Encoding.UTF8.GetString(data);

            var allLines = objCSVJson.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            var header = allLines[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            var csvData = allLines.Skip(1)
                           .Select(l => l.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                         .Select((s, i) => new { s, i })
                                         .ToDictionary(x => header[x.i], x => x.s));

            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;
            settings.StringEscapeHandling = StringEscapeHandling.Default;

            var objCSVResponse = JsonConvert.SerializeObject(csvData, settings);

            var lstBankStatement = ExecuteRequest<List<BankStatement>>(objCSVResponse);
            return lstBankStatement;
        }

        /// <summary>
        /// GetAccountInformation
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entityType"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        private async Task<Account> GetAccountInformation(string entityType, string entityId, string accountId)
        {
            var accountData = await this.BankAccountRepository.GetAccountByProviderAccountId(accountId);

            var bankLinkData = await this.BankLinkRepository.GetBankLinkingData(entityId, accountData.BankLinkId);
            var account = new Account
            {
                BankName = bankLinkData != null ? bankLinkData.ProviderBankName : "Test-Bank",
                ProviderAccountId = accountData.ProviderAccountId,
                AccountNumber = accountData.AccountNumberMask,
                AccountType = accountData.AccountType,
                AvailableBalance = accountData.AvailableBalance,
                CurrentBalance = accountData.CurrentBalance,
                Id = accountData.ProviderAccountId,
                NameOnAccount = accountData.NameOnAccount,
                OfficialAccountName =accountData.OfficialAccountName,
                Source = Enum.GetName(typeof(Provider), accountData.ProviderId)
            };

            return account;
        }

        /// <summary>
        /// FormatAccountNumber
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <returns></returns>
        private string FormatAccountNumber(string accountNumber)
        {
            if (!string.IsNullOrWhiteSpace(accountNumber) && accountNumber.Length > 4)
            {
                return accountNumber.Substring(accountNumber.Length - 4);
            }
            else
            {
                return accountNumber;
            }            
        }


        /// <summary>
        /// GetAccountInformation
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entityType"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        private async Task<List<Transaction>> GetTransactionInformation(string entityType, string entityId, string filter, string accountId)
        {
            var transactionData = new List<IBankTransaction>();
            if (filter.ToUpper().Equals(FilterType.Latest.ToString().ToUpper()))
            {
                transactionData = (List<IBankTransaction>)await this.BankTransactionRepository.GetFilterTransactionData(accountId, TenantTime.Now.UtcDateTime, Configuration.FilterInMonths);
            }
            else
            {
                transactionData = (List<IBankTransaction>)await this.BankTransactionRepository.GetTransactions(accountId);
            }

            var transactionList = new List<Transaction>();

            foreach (var tran in transactionData)
            {
                var transaction = new Transaction();

                transaction.ProviderAccountId = tran.ProviderAccountId;
                transaction.Id = tran.Id;
                transaction.Meta = null;
                transaction.Pending = tran.Pending;
                transaction.TransactionDate = tran.TransactionDate;
                transaction.UpdatedOn = null;
                transaction.AccountId = tran.ProviderAccountId;
                transaction.Amount = tran.Amount;
                transaction.Categories = tran.Category;
                transaction.CategoryId = tran.CategoryId;
                transaction.CreatedOn = tran.CreatedDate;
                transaction.Description = tran.Description;
                transaction.EntityId = entityId;
                transaction.EntityType = entityType;
                transaction.RunningBalance = tran.RunningBalance != null? tran.RunningBalance.Value : 0;
                transactionList.Add(transaction);
            }

            return transactionList;
        }

        /// <summary>
        /// GetTransactionInformation
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="accountId"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        private async Task<List<Transaction>> GetTransactionInformation(string entityType, string entityId, string accountId, string fromDate, string toDate)
        {
            DateTime fDate;
            DateTime tDate;
            var transactionData = new List<IBankTransaction>();
            if (string.IsNullOrWhiteSpace(fromDate) || string.IsNullOrWhiteSpace(toDate))
            {
                tDate = TenantTime.Now.Date;
                fDate = tDate.AddDays(-Configuration.TransactionDays);
                transactionData = (List<IBankTransaction>)await BankTransactionRepository.GetTransactions(accountId);
            }
            else
            {
                fDate = DateTime.Parse(fromDate);
                tDate = DateTime.Parse(toDate).AddDays(1); //added 1 day as it is not taking the last day transaction when time is greater than 00:00
                transactionData = await BankTransactionRepository.GetTransactions(entityType, entityId, accountId, fDate, tDate.Date);
            }

            var transactionList = new List<Transaction>();

            foreach (var tran in transactionData)
            {
                var transaction = new Transaction();

                transaction.ProviderAccountId = tran.ProviderAccountId;
                transaction.Id = tran.Id;
                transaction.Meta = null;
                transaction.Pending = tran.Pending;
                transaction.TransactionDate = tran.TransactionDate;
                transaction.UpdatedOn = null;
                transaction.AccountId = tran.ProviderAccountId;
                transaction.Amount = tran.Amount;
                transaction.Categories = tran.Category;
                transaction.CategoryId = tran.CategoryId;
                transaction.CreatedOn = tran.CreatedDate;
                transaction.Description = tran.Description;
                transaction.EntityId = entityId;
                transaction.EntityType = entityType;
                transaction.RunningBalance = tran.RunningBalance != null? tran.RunningBalance.Value : 0;
                transactionList.Add(transaction);
            }

            return transactionList;
        }

        #endregion

        #region Finicity methods
        public async Task<IAccessTokenResponse> GetAccessToken(string entityType, string entityId)
        {
            return await FinicityService.GetAccessToken(entityType, entityId);
        }

        public async Task<IResponseConnection> AddCustomer(string entityType, string entityId, IAddCustomerRequest request)
        {
            Logger.Info($"Processing the AddCustomer in InstantBankVerificationService...");

            return await FinicityService.AddCustomer(entityType, entityId, request);
        }

        public async Task<IConnectLinkResponse> GenerateConnectLink(string entityType, string entityId, IConnectLinkRequest request)
        {
            return await FinicityService.GenerateConnectLink(entityType, entityId, request);
        }

        public async Task<ICustomerAccountsResponse> GetCustomerAccounts(string entityType, string entityId, ICustomerDetailRequest request)
        {
            return await FinicityService.GetCustomerAccounts(entityType, entityId, request);
        }

        public async Task<ITransactionsResponse> GetTransactionsByAccount(string entityType, string entityId, Request.ITransactionRequest request)
        {
            return await FinicityService.GetTransactionsByAccount(entityType, entityId, request);
        }

        public async Task<ITxPushResponse> EnableTxPush(string entityType, string entityId, ITxPushRequest txPushRequest)
        {
            return await FinicityService.EnableTxPush(entityType, entityId, txPushRequest);
        }

        public async Task<bool> VerifyFinicityEvents(string entityType, string entityId, string eventName)
        {
            return await FinicityService.VerifyFinicityEvents(entityType, entityId, eventName);
        }

        public async Task<IAddTransactionDetailResponse> AddFinicityTransaction(IAddTransactionDetailRequest transactionDetailRequest)
        {
            return await FinicityService.AddTransaction(transactionDetailRequest);
        }

        public async Task<dynamic> LoadHistoricTransactions(string entityType, string entityId, ICustomerDetailRequest customerDetailRequest)
        {
            return await FinicityService.LoadHistoricTransactions(entityType, entityId, customerDetailRequest);
        }

        public async Task<ICustomerAccountsResponse> ProcessRemaining(string entityType, string entityId, ICustomerDetailRequest customerDetailRequest)
        {
            Logger.Info($"Processing the remaining steps for finicity in InstantBankVerificationService...");

            Logger.Info($"Processing the GetcustomerAccounts in InstantBankVerificationService...");

            var accountresult = await FinicityService.GetCustomerAccounts(entityType, entityId, customerDetailRequest);

            if (accountresult == null)
            {
                Logger.Info($"bank account is not linked from finicity");
                throw new InvalidOperationException($"bank account result is empty");
            }

            Logger.Info($"Getting customerAccounts finished...");
            Logger.Info($"Processing the LoadHistoricTransactions in InstantBankVerificationService...");

            var loadresult = await FinicityService.LoadHistoricTransactions(entityType, entityId, customerDetailRequest);

            if (loadresult == false)
            {
                Logger.Info($"Transactions are not loaded...");
                throw new InvalidOperationException($"Transactions are not loaded...");
            }

            Logger.Info($"Transactions are loaded...");

            var request = new Request.TransactionRequest();
            request.CustomerId = customerDetailRequest.CustomerId;
            request.FinicityAppToken = customerDetailRequest.FinicityAppToken;
             var accountsForSimulations = new List<Request.SimulationAccounts>();   
            foreach (var item in accountresult.CustomerAccountsDetails)
            {
                var simulationAccount = new Request.SimulationAccounts();
                simulationAccount.SimulationAccountId = item.Id;
                simulationAccount.AccountType = item.Type;
                accountsForSimulations.Add(simulationAccount);
            }
            request.SimulationAccounts = accountsForSimulations;
            Logger.Info($"Processing the GetTransactionsByAccount in InstantBankVerificationService...");
            var transactionresult = await FinicityService.GetTransactionsByAccount(entityType, entityId, request);

            Logger.Info($"Getting GetTransactionsByAccount finished...");

            return accountresult;
        }

        #endregion

        #region Ocrolus

        public async Task<IResponseConnection> CreateBook(string entityType, string entityId, RequestConnection connection)
        {
            Logger.Info($"Processing the method CreateBook...");

            var result = await OcrolusPerfectAuditService.CreateBook(entityType, entityId, connection);
            return result;
        }

        public async Task<dynamic> GetPerfectAuditTransactionData(string book_pk)
        {
            return await OcrolusPerfectAuditService.GetTransactions(book_pk);

        }

        public async Task<CommonResponse> UploadStatement(string entityType, string entityId, List<FileuploadData> statement, /*string name,*/ string bookPk)
        {
            if (string.IsNullOrEmpty(bookPk))
            {
                throw new ArgumentException("BookId is null or empty :" + bookPk);
            }
            Logger.Info($"Processing the method UploadStatement in IBV service...");
            //var bookinfo = await PerfectAuditRepository.GetByEntityId(entityType, entityId);

            //if (bookinfo == null)
            //{
            //    throw new ArgumentException("bookinfo is not found for bookId :" + bookPk);
            //}


            if (statement.Count == 0)
                throw new ArgumentNullException("File length can not be zero");


            var uploadresponse = await OcrolusPerfectAuditService.UploadStatement(entityType, entityId, statement, bookPk);
            return uploadresponse;




        }

        public async Task<CommonResponse> UploadStatement(string entityType, string entityId, byte[] statement, string name, string bookPk)
        {
            if (string.IsNullOrEmpty(bookPk))
            {
                throw new ArgumentException("BookId is null or empty :" + bookPk);
            }
            Logger.Info($"Processing the method single UploadStatement in IBV service...");

            if (statement.Length == 0)
                throw new ArgumentNullException("File length can not be zero");


            var uploadresponse = await OcrolusPerfectAuditService.UploadStatement(entityType, entityId, statement, name, bookPk);
            return uploadresponse;

        }
        #endregion

        #region InBound Hook Invoked Handler
        public async Task<bool> InBoundHookInvokedHandler(string entityType, string entityId, IEventRequest request)
        {
            bool result = false;
            try
            {
                Logger.Info("Started Execution for InBoundHookInvokedHandler Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                if (String.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (request == null)
                    throw new ArgumentNullException(nameof(request));
                if (request.data == null)
                    throw new ArgumentNullException(nameof(request.data));
                if (entityId == "cGJncC1maW5pY2l0eQ==") // Finicity
                {
                    var ruleResult = DecisionEngineService.Execute<dynamic>(request.DERuleName, new { input = request.data });
                    if (ruleResult != null)
                    {
                        await FinicityService.UpdateAccountAndSaveTransaction(entityType, entityId, ruleResult);
                    }

                }
                if (entityId == "cGJncC1wZXJmZWN0YXVkaXQ=") // Ocrolus
                {
                    await OcrolusPerfectAuditService.GetTransactions(entityId);
                    if (request.data != null)
                    {
                        dynamic objdata = request.data;
                        await OcrolusPerfectAuditService.GetSummaryDataByBookId(objdata.book_pk);
                    }
                }


                result = true;
            }
            catch (Exception ex)
            {
                Logger.Error($"InBoundHookInvokedHandler Error For : {entityId} , Error : {ex.Message + ex.StackTrace}");
            }
            Logger.Info("Completed Execution for InBoundHookInvokedHandler Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            return result;
        }
        #endregion

        #region Plaid Refresh

        public async Task<IPlaidRefreshResponse> PlaidRefreshLinking(string entityId)
        {
            Logger.Info($"Processing the method PlaidRefreshLinking...");
            if (string.IsNullOrWhiteSpace(entityId))
            {
                throw new ArgumentException($"entityId is null or empty");
            }

            return await PlaidUniteService.PlaidRefreshLinking(entityId);
        }

        #endregion

        /// <summary>
        /// GetBankLinkAccountDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public async Task<IBankAccount> GetBankLinkAccountDetails(string entityType, string entityId, string accountId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            var bankLinks = await this.BankLinkRepository.GetBankLinksByEntityId(entityId);

            if (bankLinks == null)
            {
                return null;
            }

            var bankLinkIds = bankLinks.Select(x => x.Id).ToList<string>();
            return await BankAccountRepository.GetAccountDetails(bankLinkIds, accountId);
        }


        /// <summary>
        /// DeleteToken
        /// </summary>        
        /// <returns></returns>
        public async Task<List<string>> DeleteToken()
        {
           return await PlaidUniteService.DeleteToken();   
        
        }

        
    }
}


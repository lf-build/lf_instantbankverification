﻿using System;
using System.Collections.Generic;

namespace LendFoundry.InstantBankVerification
{
    public static class UtilityHelper
    {
        /// <summary>
        /// TryParseEnum
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="compareString"></param>
        /// <returns></returns>
        public static bool TryParseEnumCheck<TEnum>(string compareString) where TEnum : struct
        {
            TEnum enumValue;
            try
            {
                enumValue = (TEnum)Enum.Parse(typeof(TEnum), compareString, true);
                return true;
            }
            catch
            {
                enumValue = default(TEnum);
                return false;
            }
        }

        /// <summary>
        /// TryParseEnumValue
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="compareString"></param>
        /// <returns></returns>
        public static TEnum TryParseEnumValue<TEnum>(string compareString) where TEnum : struct
        {
            TEnum enumValue;
            try
            {
                enumValue = (TEnum)Enum.Parse(typeof(TEnum), compareString, true);
                return enumValue;
            }
            catch
            {
                enumValue = default(TEnum);
                return enumValue;
            }
        }
    }
}

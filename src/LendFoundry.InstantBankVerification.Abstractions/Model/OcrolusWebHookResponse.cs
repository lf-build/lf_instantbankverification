﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification.Model
{
    public class OcrolusWebHookResponse
    {
        public OcrolusWebHookResponse()
        {

        }

        public string status { get; set; }
        public string book_pk { get; set; }
        public string uploaded_doc_pk { get; set; }
    }

    public interface IOcrolusWebHookResponse
    {
        string status { get; set; }
        string book_pk { get; set; }
        string uploaded_doc_pk { get; set; }
    }
}

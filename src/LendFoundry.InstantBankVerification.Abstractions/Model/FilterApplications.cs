﻿using LendFoundry.Foundation.Date;
using Newtonsoft.Json;

namespace LendFoundry.InstantBankVerification
{


    public class FilterApplications : IFilterApplications
    {
        public FilterApplications()
        {

        }
        public FilterApplications(IFilterApplicationsResult request)
        {
            foreach (var data in request.filterApplicationsResult)
            {
                AccessToken = data.AccessToken;
                ApplicationSubmitedDate = data.ApplicationSubmitedDate;
                EntityId = data.EntityId;
                ItemCreateOn = data.ItemCreateOn;
                Status = data.Status;
                StatusCode = data.StatusCode;
                StatusDate = data.StatusDate;
            }
        }
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public TimeBucket ItemCreateOn { get; set; }
        public string StatusCode { get; set; }
        public string Status { get; set; }
        public TimeBucket StatusDate { get; set; }
        public TimeBucket ApplicationSubmitedDate { get; set; }
        public string AccessToken { get; set; }
    }
}


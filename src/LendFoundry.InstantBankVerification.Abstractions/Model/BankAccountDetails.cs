﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.InstantBankVerification.Request;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification.Model
{
    public class BankAccountDetails : Aggregate, IBankAccountDetails
    {
        public BankAccountDetails() { }
        public BankAccountDetails(IBankAccountRequest request)
        {
            RoutingNumber = request.RoutingNumber;
            BankName = request.BankName;
            AccountNumber = request.AccountNumber;
            CurrentBalance = request.CurrentBalance;
            AvailableBalance = request.AvailableBalance;
            AccountType = request.AccountType;
            Source = request.Source;
            NameOnAccount = request.NameOnAccount;
            if (!string.IsNullOrEmpty(request.BalanceAsOfDate))
            {
                BalanceAsOfDate = DateTimeOffset.Parse(request.BalanceAsOfDate);
            }
            Id = request.AccountId;
            ProviderAccountId = request.ProviderAccountId;
        }
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string RoutingNumber { get; set; }
        public string BankName { get; set; }
        public string ProviderAccountId { get; set; }
        public string AccountNumber { get; set; }
        public double CurrentBalance { get; set; }
        public double AvailableBalance { get; set; }
        public string AccountType { get; set; }
        public string Source { get; set; }
        public DateTimeOffset? BalanceAsOfDate { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string NameOnAccount { get; set; }
        public bool IsCashflowAccount { get; set; }
        public bool IsFundingAccount { get; set; }
        [JsonProperty]
        private string BalanceDate { get; set; }
        public bool Refreshstatus { get; set; }
        public DateTimeOffset? LastRefreshDate { get; set; }
        public string ProviderErrorCode { get; set; }
        public string ProviderErrorDescription { get; set; }

    }

    public interface IBankAccountDetails : IAggregate
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        string RoutingNumber { get; set; }
        string BankName { get; set; }
        string ProviderAccountId { get; set; }
        string AccountNumber { get; set; }
        double CurrentBalance { get; set; }
        double AvailableBalance { get; set; }
        string AccountType { get; set; }
        string Source { get; set; }
        DateTimeOffset? BalanceAsOfDate { get; set; }
        DateTimeOffset? CreatedOn { get; set; }
        DateTimeOffset? UpdatedOn { get; set; }
        string CreatedBy { get; set; }
        string UpdatedBy { get; set; }
        string NameOnAccount { get; set; }
        bool IsCashflowAccount { get; set; }
        bool IsFundingAccount { get; set; }
        bool Refreshstatus { get; set; }
        DateTimeOffset? LastRefreshDate { get; set; }
        string ProviderErrorCode { get; set; }
        string ProviderErrorDescription { get; set; }
    }
}

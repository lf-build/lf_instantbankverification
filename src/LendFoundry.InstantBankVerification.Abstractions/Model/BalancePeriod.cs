﻿#if DOTNET2
using LendFoundry.Syndication.PerfectAudit.Abstractions;
#else
using LendFoundry.Syndication.PerfectAudit.Abstractions;
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification.Model
{
    public class BalancePeriod : IBalancePeriod
    {
        public BalancePeriod()
        {

        }
        public BalancePeriod(Period period)
        {
            BeginBalance = string.IsNullOrEmpty(period.begin_balance) ? 0.0 : Convert.ToDouble(period.begin_balance);
            EndBalance = string.IsNullOrEmpty(period.end_balance) ? 0.0 : Convert.ToDouble(period.end_balance);
            if (!string.IsNullOrEmpty(period.begin_date))
            {
                BeginDate = Convert.ToDateTime(period.begin_date);
            }
            if (!string.IsNullOrEmpty(period.end_date))
            {
                EndDate = Convert.ToDateTime(period.end_date);
            }
        }

        public double BeginBalance { get; set; }
        public DateTime BeginDate { get; set; }
        public double EndBalance { get; set; }
        public DateTime EndDate { get; set; }
        public int pk { get; set; }

    }

    public interface IBalancePeriod
    {
        double BeginBalance { get; set; }
        DateTime BeginDate { get; set; }
        double EndBalance { get; set; }
        DateTime EndDate { get; set; }
        int pk { get; set; }

    }
}

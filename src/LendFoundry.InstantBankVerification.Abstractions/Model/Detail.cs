﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class Detail : IDetail
    {
        public double? AvailableBalanceAmount { get; set; }
        public double? PeriodInterestRate { get; set; }
        public long? PostedDate { get; set; }
        public long? OpenDate { get; set; }
        public long? PeriodStartDate { get; set; }
        public long? PeriodEndDate { get; set; }
        public double? PeriodDepositAmount { get; set; }
        public double? PeriodInterestAmount { get; set; }
        public double? InterestYtdAmount { get; set; }
        public double? InterestPriorYtdAmount { get; set; }
        public long? MaturityDate { get; set; }
        public long? CreatedDate { get; set; }

    }

    public interface IDetail
    {
        double? AvailableBalanceAmount { get; set; }
        double? PeriodInterestRate { get; set; }
        long? PostedDate { get; set; }
        long? OpenDate { get; set; }
        long? PeriodStartDate { get; set; }
        long? PeriodEndDate { get; set; }
        double? PeriodDepositAmount { get; set; }
        double? PeriodInterestAmount { get; set; }
        double? InterestYtdAmount { get; set; }
        double? InterestPriorYtdAmount { get; set; }
        long? MaturityDate { get; set; }
        long? CreatedDate { get; set; }
    }
}

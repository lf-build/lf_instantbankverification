﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public interface ICSVRequest
    {
        string providerAccountId { get; set; }
        string accountNumber { get; set; }
        string instituteName { get; set; }
        string accountType { get; set; }
        string fileContent { get; set; }
        string fileName { get; set; }
    }
    public class CSVRequest : ICSVRequest
    {
        public string providerAccountId { get; set; }
        public string accountNumber { get; set; }
        public string instituteName { get; set; }
        public string accountType { get; set; }
        public string fileContent { get; set; }
        public string fileName { get; set; }
    }
}

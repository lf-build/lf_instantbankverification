﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class CustomerAccountsDetails: ICustomerAccountsDetails
    {
     public  long Id { get; set; }
     public  string Number { get; set; }
     public  string Name { get; set; }
     public  double Balance { get; set; }
     public  string Type { get; set; }
     public  int? AggregationStatusCode { get; set; }
     public  string Status { get; set; }
     public  string CustomerId { get; set; }
     public  string InstitutionId { get; set; }
     public  long? BalanceDate { get; set; }
     public  long? AggregationSuccessDate { get; set; }
     public  long? AggregationAttemptDate { get; set; }
     public  long? CreatedDate { get; set; }
     public  long? LastUpdatedDate { get; set; }
     public  string Currency { get; set; }
     public  long? LastTransactionDate { get; set; }
     public  long? InstitutionLoginId { get; set; }
     public  IDetail Detail { get; set; }
     public  int? DisplayPosition { get; set; }
     public  string RealAccountNumber { get; set; }
     public  string RoutingNumber { get; set; }
     public  string InstitutionName { get; set; }

    }

    public interface ICustomerAccountsDetails
    {
        long Id { get; set; }
        string Number { get; set; }
        string Name { get; set; }
        double Balance { get; set; }
        string Type { get; set; }
        int? AggregationStatusCode { get; set; }
        string Status { get; set; }
        string CustomerId { get; set; }
        string InstitutionId { get; set; }
        long? BalanceDate { get; set; }
        long? AggregationSuccessDate { get; set; }
        long? AggregationAttemptDate { get; set; }
        long? CreatedDate { get; set; }
        long? LastUpdatedDate { get; set; }
        string Currency { get; set; }
        long? LastTransactionDate { get; set; }
        long? InstitutionLoginId { get; set; }
        IDetail Detail { get; set; }
        int? DisplayPosition { get; set; }
        string RealAccountNumber { get; set; }
        string RoutingNumber { get; set; }
        string InstitutionName { get; set; }
    }
}

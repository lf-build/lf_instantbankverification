﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification.Model
{
    public class AccountsAndTransactions: IAccountsAndTransactions
    {
        public AccountsAndTransactions() { }

        public List<BankAccountDetails> Accounts { get; set; }
        public List<TransactionDetails> Transactions { get; set; }

    }

    public interface IAccountsAndTransactions
    {
        List<BankAccountDetails> Accounts { get; set; }
        List<TransactionDetails> Transactions { get; set; }

    }
}

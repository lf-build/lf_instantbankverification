﻿using Newtonsoft.Json;

namespace LendFoundry.InstantBankVerification
{
    public interface ICashflowPayload
    {
         string EntityId { get; set; }

         string EntityType { get; set; }

        string FilterType { get; set; }

         string FromDate { get; set; }

         string ToDate { get; set; }

        EventData EventData { get; set; }
    }
    public class CashflowPayload : ICashflowPayload
    {
        [JsonProperty("entityId")]
        public string EntityId { get; set; }

        [JsonProperty("entityType")]
        public string EntityType { get; set; }

        [JsonProperty("filterType")]
        public string FilterType { get; set; }

        [JsonProperty("fromDate")]
        public string FromDate { get; set; }

        [JsonProperty("toDate")]
        public string ToDate { get; set; }

        [JsonProperty("eventData")]
        public EventData EventData { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public interface IPlaidErrorDetails
    {
        string EntityId { get; set; }

        string EntityType { get; set; }

        string ErrorCode { get; set; }
        string ErrorMessage { get; set; }
        string ErrorType { get; set; }
        string Status { get; set; }
        string ItemId { get; set; }
    }

    public class PlaidErrorDetails : IPlaidErrorDetails
    {
       public string EntityId { get; set; }
       public string EntityType { get; set; }
       public string ErrorCode { get; set; }
       public string ErrorMessage { get; set; }
       public string ErrorType { get; set; }
       public string Status { get; set; }
       public string ItemId { get; set; }
    }
}

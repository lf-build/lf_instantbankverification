﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.InstantBankVerification.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification.Model
{
    public class TransactionDetails : Aggregate, ITransactionDetails
    {
        public TransactionDetails() { }
        public TransactionDetails(IBankTransactionRequest request)
        {
            AccountId = request.AccountId;
            ProviderAccountId = request.ProviderAccountId;
            Amount = request.Amount;
            TransactionDate = request.TransactionDate;
            Description = request.Description;
            Pending = request.Pending;
            CategoryId = request.CategoryId;
            Categories = request.Categories;
            Meta = request.Meta;
            TransactionId = request.TransactionId;
            CustomerId = request.CustomerId;

    }
    public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string AccountId { get; set; }
        public string TransactionId { get; set; }
        public string ProviderAccountId { get; set; }
        public double Amount { get; set; }
        public string TransactionDate { get; set; }
        public string Description { get; set; }
        public string Pending { get; set; }
        public string CategoryId { get; set; }
        public string CustomerId { get; set; }

        public IList<string> Categories { get; set; }
        public string Meta { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public DateTimeOffset UpdatedOn { get; set; }

    }

    public interface ITransactionDetails : IAggregate
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        string AccountId { get; set; }
        string TransactionId { get; set; }
        string CustomerId { get; set; }
        string ProviderAccountId { get; set; }
        double Amount { get; set; }
        string TransactionDate { get; set; }
        string Description { get; set; }
        string Pending { get; set; }
        string CategoryId { get; set; }
        IList<string> Categories { get; set; }
        string Meta { get; set; }
        DateTimeOffset CreatedOn { get; set; }
        DateTimeOffset UpdatedOn { get; set; }
    }
}

﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.InstantBankVerification
{
   

    public class FilterApplicationsResult  : IFilterApplicationsResult
    {
        public FilterApplicationsResult() { }

        [JsonConverter(typeof(InterfaceListConverter<IFilterApplications, FilterApplications>))]
        public List<IFilterApplications> filterApplicationsResult { get; set; }

    }
}


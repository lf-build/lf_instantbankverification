﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification.Model
{
    public class Subscription : ISubscription
    {
        #region Public Properties

        public long Id { get; set; }
        public long AccountId { get; set; }
        public string Type { get; set; }
        public string CallbackUrl { get; set; }
        public string SigningKey { get; set; }

        #endregion
    }

    public interface ISubscription
    {
        long Id { get; set; }
        long AccountId { get; set; }
        string Type { get; set; }
        string CallbackUrl { get; set; }
        string SigningKey { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class PostedFileDetails
    {
        public string FileName { get; set; }
        public Stream Content { get; set; }
    }
}

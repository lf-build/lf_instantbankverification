﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.InstantBankVerification
{
    public interface IAccountTypeResponse
    {
         List<IBankAccount> accounts { get; set; }

         string BankName { get; set; }

         string NameOnAccount { get; set; }
    }
    public class AccountTypeResponse : IAccountTypeResponse
    {
        [JsonConverter(typeof(InterfaceListConverter<IBankAccount, BankAccount>))]
        public List<IBankAccount> accounts { get; set; }

        public string BankName { get; set; }

        public string NameOnAccount { get; set; }
    }
}

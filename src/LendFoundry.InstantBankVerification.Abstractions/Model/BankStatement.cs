﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class BankStatement
    {
        public string Amount { get; set; }
        public string Date { get; set; }
        public string Name { get; set; }
        public string CategoryId { get; set; }
        public string Available { get; set; }
    }
}

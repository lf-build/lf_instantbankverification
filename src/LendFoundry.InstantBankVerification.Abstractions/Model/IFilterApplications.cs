using LendFoundry.Foundation.Date;

namespace LendFoundry.InstantBankVerification
{
    public interface IFilterApplications
    {
        string EntityId { get; set; }
        string EntityType { get; set; }       
        TimeBucket ItemCreateOn{ get; set; }
        string StatusCode { get; set; }
        string Status { get; set; }
        TimeBucket StatusDate { get; set; }
        TimeBucket ApplicationSubmitedDate { get; set; }
        string AccessToken { get; set; }
    }
}
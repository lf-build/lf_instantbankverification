﻿using Newtonsoft.Json;

namespace LendFoundry.InstantBankVerification
{
    public interface IAccountPreferenceRequest 
    {
         string AccountID { get; set; }
         string AccountType { get; set; }
         int? AccountVersion { get; set; }

    }
    public class AccountPreferenceRequest : IAccountPreferenceRequest
    {
        [JsonProperty("accountID")]
        public string AccountID { get; set; }

        [JsonProperty("accountType")]
        public string AccountType { get; set; }

        [JsonProperty("accountVersion")]
        public int? AccountVersion { get; set; }
    }
}

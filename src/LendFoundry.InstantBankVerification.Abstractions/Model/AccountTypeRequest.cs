﻿namespace LendFoundry.InstantBankVerification
{
    public interface IAccountTypeRequest 
    {
         bool? IsFundingAccount { get; set; }
         bool? IsCashflowAccount { get; set; }
    }
    public class AccountTypeRequest : IAccountTypeRequest
    {
        public bool? IsFundingAccount { get; set; }
        public bool? IsCashflowAccount { get; set; }
    }
}

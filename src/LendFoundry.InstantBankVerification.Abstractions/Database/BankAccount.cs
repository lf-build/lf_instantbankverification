﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class BankAccount : Aggregate, IBankAccount
    {
        public string BankLinkId { get; set; }
        public string RoutingNumber { get; set; }

        public string WireRoutingNumber { get; set; }
        public string NameOnAccount { get; set; }
        public string OfficialAccountName { get; set; }

        public string ProviderAccountId { get; set; }

        public string AccountNumberMask { get; set; }

        public string AccountNumber { get; set; }

        public double? CurrentBalance { get; set; }

        public double? AvailableBalance { get; set; }
        public string AccountType { get; set; }

        public string OwnerName { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public int ProviderId { get; set; }

        public bool IsCashflowAccount { get; set; }
        public bool IsFundingAccount { get; set; }

        public TimeBucket CreatedDate { get; set; }

        public TimeBucket UpdatedDate { get; set; }

        public int? CashflowAccountVersion { get; set; }
        public int? FundingAccountVersion { get; set; }
    }
}

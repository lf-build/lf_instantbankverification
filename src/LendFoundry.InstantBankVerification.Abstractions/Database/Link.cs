﻿namespace LendFoundry.InstantBankVerification
{
    public class Link : ILink
    {
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string[] Products { get; set; }
    }
}

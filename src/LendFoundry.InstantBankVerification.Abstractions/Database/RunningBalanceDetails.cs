﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class RunningBalanceDetails : Aggregate,  IRunningBalanceDetails
    {
        public string EntityId { get; set; }
        public string ProviderBankLinkId { get; set; }
        public string ProviderAccountId { get; set; }
        public bool IsProcessed { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public DateTimeOffset UpdatedDate { get; set; }

    }
}

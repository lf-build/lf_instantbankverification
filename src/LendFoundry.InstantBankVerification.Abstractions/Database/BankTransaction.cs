﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.InstantBankVerification
{
    public class BankTransaction : Aggregate, IBankTransaction
    {
        public string TransactionId { get; set; }

        public int ProviderId { get; set; }
        public string ProviderAccountId { get; set; }

        public string ProviderBankLinkId { get; set; }
        
        public string TransactionType { get; set; }

        public double Amount { get; set; }
        public double? RunningBalance { get; set; }
        public DateTime TransactionDate { get; set; }

        public string Description { get; set; }

        public string CategoryId { get; set; }

        public string[] Category { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public bool Pending { get; set; }

        public Location Location { get; set; }

        public PaymentMeta PaymentMeta { get; set; }
        public string Type { get; set; }

    }

    public class Location
    {
        public string Address { get; set; }
        public string City { get; set; }
        public string Lat { get; set; }
        public string Lon { get; set; }
        public string State { get; set; }
        public string Store_number { get; set; }
        public string Zip { get; set; }
    }


    public class PaymentMeta
    {
        public string ByOrderOf { get; set; }
        public string Payee { get; set; }
        public string Payer { get; set; }
        public string PaymentMethod { get; set; }
        public string PaymentProcessor { get; set; }
        public string PpdId { get; set; }
        public string Reason { get; set; }
        public string ReferenceNumber { get; set; }
    }

}

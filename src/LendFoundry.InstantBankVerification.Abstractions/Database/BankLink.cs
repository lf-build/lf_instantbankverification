﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.InstantBankVerification
{
    public class BankLink : Aggregate, IBankLink
    {
        public string EntityId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserMailId { get; set; }

        public string ProviderBankLinkId { get; set; }
        public string ProviderToken { get; set; }
        public string ProviderBankId { get; set; }
        public string ProviderBankName { get; set; }
        public string ProviderLinkSessionId { get; set; }
        public string ProviderLinkTempToken { get; set; }
        public string ProviderLinkReferenceId { get; set; }
        public TimeBucket CreatedDate { get; set; }
        public TimeBucket UpdatedDate { get; set; }
        public bool IsDeleted { get; set; }
        public int ProviderId { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ILink, Link>))]
        public List<ILink> Links { get; set; }
    }
}

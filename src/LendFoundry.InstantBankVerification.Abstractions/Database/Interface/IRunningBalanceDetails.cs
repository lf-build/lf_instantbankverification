﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public interface IRunningBalanceDetails : IAggregate
    {
        string EntityId { get; set; }
        string ProviderBankLinkId { get; set; }
        string ProviderAccountId { get; set; }
        bool IsProcessed { get; set; }
       DateTimeOffset CreatedDate { get; set; }
        DateTimeOffset UpdatedDate { get; set; }

    }
}

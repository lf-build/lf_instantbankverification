﻿namespace LendFoundry.InstantBankVerification
{
    public interface ILink
    {
        string EntityId { get; set; }
        string EntityType { get; set; }

        string[] Products { get; set; }
    }
}

﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public interface IClientTrack : IAggregate
    {
        //Institutions Institutions { get; set; }
        string LinkRequestId { get; set; }
        string LinkSessionId { get; set; }
        string PlaidApiRequestId { get; set; }
        string Status { get; set; }
        string EntityId { get; set; }
        string EntityType { get; set; }
        TimeBucket CreatedDate { get; set; }
    }
}

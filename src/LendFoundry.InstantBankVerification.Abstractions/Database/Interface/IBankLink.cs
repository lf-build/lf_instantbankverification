﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LendFoundry.InstantBankVerification
{
    public interface IBankLink : IAggregate
    {
        string EntityId { get; set; }

         string FirstName { get; set; }

         string LastName { get; set; }

         string UserMailId { get; set; }

        string ProviderBankLinkId { get; set; }
        string ProviderToken { get; set; }
        string ProviderBankId { get; set; }
        string ProviderBankName { get; set; }
        string ProviderLinkSessionId { get; set; }
        string ProviderLinkTempToken { get; set; }
        string ProviderLinkReferenceId { get; set; } 
        TimeBucket CreatedDate { get; set; }
        TimeBucket UpdatedDate { get; set; }
        bool IsDeleted { get; set; }
        int ProviderId { get; set; }
        List<ILink> Links { get; set; }
    }
}

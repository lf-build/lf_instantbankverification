﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.InstantBankVerification
{
    public interface IBankAccount : IAggregate
    {
        string BankLinkId { get; set; }
        string RoutingNumber { get; set; }

        string WireRoutingNumber { get; set; }
        string NameOnAccount { get; set; }
        string OfficialAccountName { get; set; }
        

        string ProviderAccountId { get; set; }

        string AccountNumberMask { get; set; }
        string AccountNumber { get; set; }

         string OwnerName { get; set; }
        double? CurrentBalance { get; set; }

        double? AvailableBalance { get; set; }
        string AccountType { get; set; }

        int ProviderId { get; set; }
       

        bool IsActive { get; set; }

        bool IsDeleted { get; set; }

         bool IsCashflowAccount { get; set; }
         bool IsFundingAccount { get; set; }
        TimeBucket CreatedDate { get; set; }

        TimeBucket UpdatedDate { get; set; }
        int? CashflowAccountVersion { get; set; }
        int? FundingAccountVersion { get; set; }
    }
}

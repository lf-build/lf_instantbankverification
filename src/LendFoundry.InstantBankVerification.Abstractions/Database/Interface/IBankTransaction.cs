﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.InstantBankVerification
{
    public interface IBankTransaction : IAggregate
    {
        string TransactionId { get; set; }
        int ProviderId { get; set; }
        string ProviderAccountId { get; set; }
        string ProviderBankLinkId { get; set; }
        string TransactionType { get; set; }
        double Amount { get; set; }
        double? RunningBalance { get; set; }
        DateTime TransactionDate { get; set; }
        string Description { get; set; }
        string CategoryId { get; set; }
        string[] Category { get; set; }
        DateTimeOffset CreatedDate { get; set; }
        bool Pending { get; set; }
        Location Location { get; set; }
        PaymentMeta PaymentMeta { get; set; }
        string Type { get; set; }

    }
}

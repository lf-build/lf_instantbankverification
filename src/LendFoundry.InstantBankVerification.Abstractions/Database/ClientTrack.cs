﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.InstantBankVerification
{
    public class ClientTrack : Aggregate, IClientTrack
    {
        //private IClientTrack clientTrackRequest;

        public ClientTrack()
        {

        }

        public ClientTrack(IClientTrack clientTrackRequest)
        {
            //Institutions = new Institutions(clientTrackRequest.Institutions);
            LinkRequestId = clientTrackRequest?.LinkRequestId;
            LinkSessionId = clientTrackRequest?.LinkSessionId;
            PlaidApiRequestId = clientTrackRequest?.PlaidApiRequestId;
            Status = clientTrackRequest?.Status;
            EntityId = clientTrackRequest?.EntityId;
            EntityType = clientTrackRequest?.EntityType;
            CreatedDate = new TimeBucket(DateTime.UtcNow);

        }
        //public Institutions Institutions { get; set; }
        public string LinkRequestId { get; set; }
        public string LinkSessionId { get; set; }
        public string PlaidApiRequestId { get; set; }
        public string Status { get; set; }
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public TimeBucket CreatedDate { get; set; }
    }
}

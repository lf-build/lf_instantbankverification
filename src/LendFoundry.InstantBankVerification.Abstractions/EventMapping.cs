﻿namespace LendFoundry.InstantBankVerification
{
    public class EventMapping
    {
        public string Name { get; set; }

        public string EventName { get; set; }

        public string WebhookType { get; set; }

        public string WebhookCode { get; set; }

        public string ItemId { get; set; }

        public string Error { get; set; }

        public string NewTransactions { get; set; }

        public string RemovedTransactions { get; set; }
        public string PlaidDERuleName { get; set; }


        //Finicity
        public string Response { get; set; }
        public string Request { get; set; }

        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string FinicityMethodToExecute { get; set; }
        public string FinicityCompletionEventName { get; set; }
        public string FinicityDERuleName { get; set; }

    }
}

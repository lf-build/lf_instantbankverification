﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class AddTransactionDetailResponse: IAddTransactionDetailResponse
    {
        #region Public Properties

        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }
        [JsonProperty(PropertyName = "createdDate")]
        public long CreatedDate { get; set; }

        #endregion
    }

    public interface IAddTransactionDetailResponse
    {
        long Id { get; set; }
        long CreatedDate { get; set; }
    }

}

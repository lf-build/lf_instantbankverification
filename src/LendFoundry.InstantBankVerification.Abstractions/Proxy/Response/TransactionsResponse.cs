﻿using LendFoundry.Foundation.Client;
using LendFoundry.InstantBankVerification.Proxy.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification.Proxy.Response
{
    public class TransactionsResponse: ITransactionsResponse
    {
        #region Public Properties
        [JsonProperty(PropertyName = "moreAvailable")]
        public string MoreAvailable { get; set; }
        [JsonProperty(PropertyName = "found")]
        public int TotalRecords { get; set; }
        [JsonProperty(PropertyName = "displaying")]
        public int DisplayingRecords { get; set; }
        [JsonProperty(PropertyName = "fromDate")]
        public string FromDate { get; set; }
        [JsonProperty(PropertyName = "toDate")]
        public string ToDate { get; set; }
        [JsonProperty(PropertyName = "sort")]
        public string Sort { get; set; }
        [JsonProperty(PropertyName = "transactions")]
        [JsonConverter(typeof(InterfaceListConverter<Proxy.Model.ITransaction,Proxy.Model.Transaction>))]
        public List<Proxy.Model.ITransaction> Transactions { get; set; }

        #endregion        
    }

    public interface ITransactionsResponse
    {
        string MoreAvailable { get; set; }

        int TotalRecords { get; set; }

        int DisplayingRecords { get; set; }

        string FromDate { get; set; }

        string ToDate { get; set; }

        string Sort { get; set; }

        List<Proxy.Model.ITransaction> Transactions { get; set; }
    }
}

﻿using LendFoundry.Foundation.Client;
using LendFoundry.InstantBankVerification.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification.Proxy.Response
{
    public class TxPushResponse : ITxPushResponse
    {
        #region Public Properties

        [JsonProperty(PropertyName = "subscriptions")]
        [JsonConverter(typeof(InterfaceListConverter<ISubscription, Subscription>))]
        public List<ISubscription> Subscriptions { get; set; }

        #endregion
    }

    public interface ITxPushResponse
    {
        List<ISubscription> Subscriptions { get; set; }
    }
}

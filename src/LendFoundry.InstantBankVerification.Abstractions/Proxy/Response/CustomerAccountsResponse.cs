﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace LendFoundry.InstantBankVerification.Proxy.Response
{
    public class CustomerAccountsResponse : ICustomerAccountsResponse
    {
        #region Public Properties

        [JsonProperty(PropertyName = "customerAccountsDetails")]
        [JsonConverter(typeof(InterfaceListConverter<Proxy.Model.ICustomerAccountsDetails, Proxy.Model.CustomerAccountsDetails>))]
        public List<Proxy.Model.ICustomerAccountsDetails> CustomerAccountsDetails { get; set; }

        #endregion
    }

    public interface ICustomerAccountsResponse
    {
        List<Proxy.Model.ICustomerAccountsDetails> CustomerAccountsDetails { get; set; }
    }
}

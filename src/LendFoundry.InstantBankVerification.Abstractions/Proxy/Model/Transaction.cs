﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification.Proxy.Model
{
    public class Transaction: ITransaction
    {
        #region Public Properties
        [JsonProperty(PropertyName = "accountId")]
        public long AccountId { get; set; }
        [JsonProperty(PropertyName = "amount")]
        public double Amount { get; set; }
        [JsonProperty(PropertyName = "bonusAmount")]
        public double? BonusAmount { get; set; }
        [JsonProperty(PropertyName = "checkNum")]
        public string CheckNum { get; set; }
        [JsonProperty(PropertyName = "createdDate")]
        public long CreatedDate { get; set; }
        [JsonProperty(PropertyName = "customerId")]
        public long CustomerId { get; set; }
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
        [JsonProperty(PropertyName = "escrowAmount")]
        public double? EscrowAmount { get; set; }
        [JsonProperty(PropertyName = "feeAmount")]
        public double? FeeAmount { get; set; }
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }
        [JsonProperty(PropertyName = "interestAmount")]
        public double? InterestAmount { get; set; }
        [JsonProperty(PropertyName = "memo")]
        public string Memo { get; set; }
        [JsonProperty(PropertyName = "postedDate")]
        public long PostedDate { get; set; }
        [JsonProperty(PropertyName = "principalAmount")]
        public double? PrincipalAmount { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
        [JsonProperty(PropertyName = "transactionDate")]
        public long TransactionDate { get; set; }
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
        [JsonProperty(PropertyName = "unitQuantity")]
        public double? UnitQuantity { get; set; }
        [JsonProperty(PropertyName = "unitValue")]
        public double? UnitValue { get; set; }
        [JsonProperty(PropertyName = "categorization")]
        public Categorization Categorization { get; set; }

        #endregion        
    }

    public interface ITransaction
    {
        long AccountId { get; set; }
        double Amount { get; set; }
        double? BonusAmount { get; set; }
        string CheckNum { get; set; }
        long CreatedDate { get; set; }
        long CustomerId { get; set; }
        string Description { get; set; }
        double? EscrowAmount { get; set; }
        double? FeeAmount { get; set; }
        long Id { get; set; }
        double? InterestAmount { get; set; }
        string Memo { get; set; }
        long PostedDate { get; set; }
        double? PrincipalAmount { get; set; }
        string Status { get; set; }
        long TransactionDate { get; set; }
        string Type { get; set; }
        double? UnitQuantity { get; set; }
        double? UnitValue { get; set; }
        Categorization Categorization { get; set; }
    }
}

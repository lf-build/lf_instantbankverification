﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification.Proxy.Model
{
    public class Detail : IDetail
    {
        #region Public Properties

        [JsonProperty(PropertyName = "availableBalanceAmount")]
        public double? AvailableBalanceAmount { get; set; }
        [JsonProperty(PropertyName = "periodInterestRate")]
        public double? PeriodInterestRate { get; set; }
        [JsonProperty(PropertyName = "postedDate")]
        public long? PostedDate { get; set; }
        [JsonProperty(PropertyName = "openDate")]
        public long? OpenDate { get; set; }
        [JsonProperty(PropertyName = "periodStartDate")]
        public long? PeriodStartDate { get; set; }
        [JsonProperty(PropertyName = "periodEndDate")]
        public long? PeriodEndDate { get; set; }
        [JsonProperty(PropertyName = "periodDepositAmount")]
        public double? PeriodDepositAmount { get; set; }
        [JsonProperty(PropertyName = "periodInterestAmount")]
        public double? PeriodInterestAmount { get; set; }
        [JsonProperty(PropertyName = "interestYtdAmount")]
        public double? InterestYtdAmount { get; set; }
        [JsonProperty(PropertyName = "interestPriorYtdAmount")]
        public double? InterestPriorYtdAmount { get; set; }
        [JsonProperty(PropertyName = "maturityDate")]
        public long? MaturityDate { get; set; }
        [JsonProperty(PropertyName = "createdDate")]
        public long? CreatedDate { get; set; }

        #endregion
    }

    public interface IDetail
    {
        double? AvailableBalanceAmount { get; set; }
        double? PeriodInterestRate { get; set; }
        long? PostedDate { get; set; }
        long? OpenDate { get; set; }
        long? PeriodStartDate { get; set; }
        long? PeriodEndDate { get; set; }
        double? PeriodDepositAmount { get; set; }
        double? PeriodInterestAmount { get; set; }
        double? InterestYtdAmount { get; set; }
        double? InterestPriorYtdAmount { get; set; }
        long? MaturityDate { get; set; }
        long? CreatedDate { get; set; }
    }

}

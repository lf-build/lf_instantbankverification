﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification.Proxy.Model
{
    public class Categorization : ICategorization
    {
        #region Public Properties

        [JsonProperty(PropertyName = "normalizedPayeeName")]
        public string NormalizedPayeeName { get; set; }
        [JsonProperty(PropertyName = "sic")]
        public string Sic { get; set; }
        [JsonProperty(PropertyName = "category")]
        public string Category { get; set; }
        [JsonProperty(PropertyName = "scheduleC")]
        public string ScheduleC { get; set; }

        #endregion
    }
    public interface ICategorization
    {
        string NormalizedPayeeName { get; set; }
        string Sic { get; set; }
        string Category { get; set; }
        string ScheduleC { get; set; }
    }
}

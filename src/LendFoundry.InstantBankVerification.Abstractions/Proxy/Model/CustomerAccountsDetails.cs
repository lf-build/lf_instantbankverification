﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification.Proxy.Model
{
    public class CustomerAccountsDetails: ICustomerAccountsDetails
    {
        #region Public Properties

        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }
        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "balance")]
        public double Balance { get; set; }
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
        [JsonProperty(PropertyName = "aggregationStatusCode")]
        public int? AggregationStatusCode { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
        [JsonProperty(PropertyName = "customerId")]
        public string CustomerId { get; set; }
        [JsonProperty(PropertyName = "institutionId")]
        public string InstitutionId { get; set; }
        [JsonProperty(PropertyName = "balanceDate")]
        public long? BalanceDate { get; set; }
        [JsonProperty(PropertyName = "aggregationSuccessDate")]
        public long? AggregationSuccessDate { get; set; }
        [JsonProperty(PropertyName = "aggregationAttemptDate")]
        public long? AggregationAttemptDate { get; set; }
        [JsonProperty(PropertyName = "createdDate")]
        public long? CreatedDate { get; set; }
        [JsonProperty(PropertyName = "lastUpdatedDate")]
        public long? LastUpdatedDate { get; set; }
        [JsonProperty(PropertyName = "currency")]
        public string Currency { get; set; }
        [JsonProperty(PropertyName = "lastTransactionDate")]
        public long? LastTransactionDate { get; set; }
        [JsonProperty(PropertyName = "institutionLoginId")]
        public long? InstitutionLoginId { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDetail, Detail>))]
        public IDetail Detail { get; set; }
        [JsonProperty(PropertyName = "displayPosition")]
        public int? DisplayPosition { get; set; }
        public string RealAccountNumber { get; set; }
        public string RoutingNumber { get; set; }
        public string InstitutionName { get; set; }

        #endregion        
    }

    public interface ICustomerAccountsDetails
    {
        long Id { get; set; }
        string Number { get; set; }
        string Name { get; set; }
        double Balance { get; set; }
        string Type { get; set; }
        int? AggregationStatusCode { get; set; }
        string Status { get; set; }
        string CustomerId { get; set; }
        string InstitutionId { get; set; }
        long? BalanceDate { get; set; }
        long? AggregationSuccessDate { get; set; }
        long? AggregationAttemptDate { get; set; }
        long? CreatedDate { get; set; }
        long? LastUpdatedDate { get; set; }
        string Currency { get; set; }
        long? LastTransactionDate { get; set; }
        long? InstitutionLoginId { get; set; }
        IDetail Detail { get; set; }
        int? DisplayPosition { get; set; }
        string RealAccountNumber { get; set; }
        string RoutingNumber { get; set; }
        string InstitutionName { get; set; }
    }
}

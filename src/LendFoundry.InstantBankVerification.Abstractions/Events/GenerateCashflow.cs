﻿namespace LendFoundry.InstantBankVerification
{
    public class GenerateCashflow
    {
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public object Response { get; set; }
        public string ReferenceNumber { get; set; }
       
    }
}

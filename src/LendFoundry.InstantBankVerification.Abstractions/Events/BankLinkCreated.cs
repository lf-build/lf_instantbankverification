﻿namespace LendFoundry.InstantBankVerification
{
    public class BankLinkCreated
    {
        public string EntityId { get; set; }
        public string EntityType { get; set; }

        public string BankLinkNumber { get; set; }

        public string ReferenceNumber { get; set; }
    }
}

﻿using LendFoundry.Foundation.Client;

using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.InstantBankVerification
{
    public interface IFilterAccountDataView
    {
        string RoutingNumber { get; set; }

        string WireRoutingNumber { get; set; }

        string NameOnAccount { get; set; }

        string ProviderAccountId { get; set; }

        string AccountNumberMask { get; set; }

        string AccountNumber { get; set; }

        double? CurrentBalance { get; set; }

        double? AvailableBalance { get; set; }

        string AccountType { get; set; }

        string OwnerName { get; set; }

        bool IsActive { get; set; }

        bool IsDeleted { get; set; }

        IList<IBankTransaction> BankTransactions { get; set; }

    }
    public class FilterAccountDataView : IFilterAccountDataView
    {
        public string RoutingNumber { get; set; }

        public string WireRoutingNumber { get; set; }

        public string NameOnAccount { get; set; }

        public string ProviderAccountId { get; set; }

        public string AccountNumberMask { get; set; }

        public string AccountNumber { get; set; }

        public double? CurrentBalance { get; set; }

        public double? AvailableBalance { get; set; }

        public string AccountType { get; set; }

        public string OwnerName { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankTransaction, BankTransaction>))]
        public IList<IBankTransaction> BankTransactions { get; set; }
    }
}

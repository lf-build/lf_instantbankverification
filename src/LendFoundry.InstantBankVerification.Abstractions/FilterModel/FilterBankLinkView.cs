﻿using LendFoundry.Foundation.Date;

namespace LendFoundry.InstantBankVerification
{
    public interface IFilterBankLinkView
    {
         string Name { get; set; }

         string UserMailId { get; set; }
        string EntityId { get; set; }

        string ProviderBankId { get; set; }

        string ProviderBankName { get; set; }

        bool IsDeleted { get; set; }

        int ProviderId { get; set; }

        string BankLinkId { get; set; }

        TimeBucket LinkedDate { get; set; }
    }
    public class FilterBankLinkView : IFilterBankLinkView
    {
        public string Name { get; set; }

        public string UserMailId { get; set; }

        public string EntityId { get; set; }

        public string ProviderBankId { get; set; }

        public string ProviderBankName { get; set; }

        public bool IsDeleted { get; set; }

        public int ProviderId { get; set; }

        public string BankLinkId { get; set; }

        public TimeBucket LinkedDate { get; set; }
    }
}

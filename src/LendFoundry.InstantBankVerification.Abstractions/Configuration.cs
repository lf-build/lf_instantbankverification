﻿using System.Collections.Generic;

namespace LendFoundry.InstantBankVerification
{
    public interface IConfiguration
    {

        EventMapping[] Events { get; set; }

        List<string> AccountTypes { get; set; }

         string WebhookUrl { get; set; }

        int TransactionDays { get; set; }

        string DateFormat { get; set; }

        string[] AcceptedFileIn { get; set; }
        int MaxUploadFileSizeLimit { get; set; }

        int FilterInMonths { get; set; }

       FinicityConfiguration FinicityConfiguration { get; set; }
       string DeleteTokenRule { get; set; }
    }

    public class Configuration : IConfiguration
    {

        public EventMapping[] Events { get; set; }

        public List<string> AccountTypes { get; set; }

        public string WebhookUrl { get; set; }

        public int TransactionDays { get; set; }

        public string DateFormat { get; set; }

        public string[] AcceptedFileIn { get; set; }
        public int MaxUploadFileSizeLimit { get; set; }

        public int FilterInMonths { get; set; }


        public FinicityConfiguration FinicityConfiguration { get; set; }

        public string DeleteTokenRule { get; set; }
    }
}

﻿using System;

namespace LendFoundry.InstantBankVerification
{
    public class Settings
    {
        private const string Prefix = "IBV";

        public static string ServiceName => Prefix.ToLower();

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public interface IFinicityService
    {
        Task<IAccessTokenResponse> GetAccessToken(string entityType, string entityId);
        Task<IResponseConnection> AddCustomer(string entityType, string entityId, IAddCustomerRequest request);

        Task<IConnectLinkResponse> GenerateConnectLink(string entityType, string entityId, IConnectLinkRequest request);
        Task<ICustomerAccountsResponse> GetCustomerAccounts(string entityType, string entityId, ICustomerDetailRequest request);
        Task<ITransactionsResponse> GetTransactionsByAccount(string entityType, string entityId, Request.ITransactionRequest request);
        Task<ITxPushResponse> EnableTxPush(string entityType, string entityId, ITxPushRequest txPushRequest);
        Task<bool> VerifyFinicityEvents(string entityType, string entityId, string eventName);
        Task<bool> InBoundHookInvokedHandler(string entityType, string entityId, IEventRequest request);
        Task<IAddTransactionDetailResponse> AddTransaction(IAddTransactionDetailRequest transactionDetailRequest);
        Task<dynamic> LoadHistoricTransactions(string entityType, string entityId, ICustomerDetailRequest customerDetailRequest);
        Task UpdateAccountAndSaveTransaction(string entityType, string entityId, dynamic ruleResult);

        
    }
}

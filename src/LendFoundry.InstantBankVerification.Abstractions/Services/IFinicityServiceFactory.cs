﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.InstantBankVerification
{
    public interface IFinicityServiceFactory
    {
        IFinicityService Create(ITokenReader reader, ITokenHandler handler, ILogger logger);

    }
}

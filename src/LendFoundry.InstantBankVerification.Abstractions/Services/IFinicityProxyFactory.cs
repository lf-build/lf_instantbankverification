﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public interface IFinicityProxyFactory
    {
        IFinicityProxy Create(ITokenReader reader, ILogger logger);
    }
}

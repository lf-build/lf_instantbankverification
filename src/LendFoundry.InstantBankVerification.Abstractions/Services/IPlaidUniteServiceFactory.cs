﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.InstantBankVerification
{
    public interface IPlaidUniteServiceFactory
    {
        IPlaidUniteService Create(ITokenReader reader, ITokenHandler handler, ILogger logger);

    }
}

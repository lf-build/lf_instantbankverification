﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.InstantBankVerification
{
    public interface IInstantBankVerificationClientFactory
    {
       IInstantBankVerificationService Create(ITokenReader reader);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public interface IFinicityProxy
    {
        IAccessTokenResponse GetAccessToken(string entitytype, string entityid);

        IAddCustomerResponse AddCustomer(string entityType, string entityId, IAddCustomerRequest addCustomerRequest);

        IConnectLinkResponse GenerateConnectLink(string entitytype, string entityid, IConnectLinkRequest request);
        Proxy.Response.ICustomerAccountsResponse GetCustomerAccounts(string entitytype, string entityid, ICustomerDetailRequest request);
        Proxy.Response.ITransactionsResponse GetTransactionsByAccount(string entitytype, string entityid, Request.ITransactionRequest request);
        Proxy.Response.ITxPushResponse EnableTxPush(string entitytype, string entityid, ITxPushRequest request);

        IAddTransactionDetailResponse AddTransaction(IAddTransactionDetailRequest transactionDetailRequest);

        dynamic LoadHistoricTransactions(string entityType, string entityId, ICustomerDetailRequest customerDetailRequest);

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public interface IPlaidUniteService
    {
        Task<IResponseConnection> AddBankLinkAndAccounts(string entityType, string entityId, IRequestConnection request);

        Task<object> AccoutSync(string entityType, string entityId, IRequestConnection request);
        Task<bool> DeleteConnection(string entityType, string entityId, IRequestConnection request);

        Task<bool> DeleteAccount(IRequestAddBank request);

        Task<bool> AddBank(string entityType, string entityId, IRequestAddBank request);

        Task PullTransaction(IRequestTransaction request);

        Task PullTransactionsForPbgp(IRequestTransaction request);

        Task PlaidErrorHandling(string itemId, IEventRequest request);

        Task<IPlaidRefreshResponse> PlaidRefreshLinking(string entityId);

        Task PlaidClientDataEventHandler(IClientTrack ClientTrackRequest);
        Task<List<string>> DeleteToken();
    }
}

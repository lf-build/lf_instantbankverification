﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.InstantBankVerification
{
    public interface IInstantBankServiceFactory
    {

        IInstantBankVerificationService Create(ITokenReader reader, ITokenHandler handler, ILogger logger);
    }
}

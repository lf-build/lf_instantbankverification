﻿using System.Collections.Generic;
using System.Threading.Tasks;
#if DOTNET2
using LendFoundry.Syndication.PerfectAudit.Abstractions;
#else
using LendFoundry.Syndication.PerfectAudit.Abstractions;
#endif
namespace LendFoundry.InstantBankVerification
{
    public interface IInstantBankVerificationService
    {
        Task<IResponseConnection> BankLinking(string entityType, string entityId, IRequestConnection request);

        Task<object> GetAccounts(string entityType, string entityId, IRequestConnection request);

        Task<object> AccountSync(string entityType, string entityId, IRequestConnection request);

        Task<bool> DeleteConnection(string entityType, string entityId, IRequestConnection request);

        //Task<object> GetGivenAccountDetails(string entityId, string entityType, IRequestAddBank request);

        Task<bool> DeleteAccount(string entityType, string entityId, IRequestAddBank request);

        Task<bool> AddBank(string entityType, string entityId, IRequestAddBank request);

        Task<List<string>> GetAccountIds(string entityId, string bankLinkId);

        Task PullTransaction(IRequestTransaction request);

        Task<ITransactionCashflowResponse> GetTransactionForCashflow(string entityType, string entityId, string filter, string accountId);

        Task<ITransactionCashflowResponse> GetTransactionForCashflow(string entityType, string entityId, string accountId, string fromDate, string toDate);
        Task<ICashflowPayload> GetCashflowPayload(string entityType, string entityId,  string accountId);
        //Task GetCashflowResult(string entityId, string entityType, string accountId);
        Task<object> UploadManualCsv(string entityType, string entityId, PostedFileDetails file);
        Task AddManualDetails(string entityType, string entityId, IRequestManualBankLink request);

        Task<IBankAccountWrapperResponse> GetAccount(string entityType, string entityId, string accountId);

        Task<IFilterBankLinkView> GetBankLinkFilterViewData(string entityId, string bankLinkId);

        Task<List<IFilterAccountDataView>> GetAccoutFilterData(string bankLinkId);

        Task<IAccessTokenResponse> GetAccessToken(string entityType, string entityId);
        Task<IResponseConnection> AddCustomer(string entityType, string entityId, IAddCustomerRequest request);
        Task<IConnectLinkResponse> GenerateConnectLink(string entityType, string entityId, IConnectLinkRequest request);
        Task<ICustomerAccountsResponse> GetCustomerAccounts(string entityType, string entityId, ICustomerDetailRequest request);
        Task<ITransactionsResponse> GetTransactionsByAccount(string entityType, string entityId, Request.ITransactionRequest request);

        Task<ITxPushResponse> EnableTxPush(string entityType, string entityId, ITxPushRequest txPushRequest);
        Task<bool> VerifyFinicityEvents(string entityType, string entityId, string eventName);

        Task<List<IBankTransaction>> GetTransactions(string accountId);

        Task<List<IBankTransaction>> GetTransactions(string entityType, string entityId, string accountId, string fromDate, string toDate);

        Task<IAddTransactionDetailResponse> AddFinicityTransaction(IAddTransactionDetailRequest transactionDetailRequest);

        Task<dynamic> LoadHistoricTransactions(string entityType, string entityId, ICustomerDetailRequest customerDetailRequest);

        Task<IResponseConnection> CreateBook(string entityType, string entityId, RequestConnection connection);
        Task<dynamic> GetPerfectAuditTransactionData(string book_pk);
        Task<bool> InBoundHookInvokedHandler(string entityType, string entityId, IEventRequest request);
        Task<CommonResponse> UploadStatement(string entityType, string entityId, List<FileuploadData> statement, /*string name,*/ string bookPk);

        Task<IBankAccountResponse> GetBankAccountInformation(string entityType, string entityId, string accountId);

        Task UpdateBankAccount(string entityType, string entityId, IBankAccountRequest request);
        Task<CommonResponse> UploadStatement(string entityType, string entityId, byte[] statement, string name, string bookPk);

        Task UpdateRunningBalance();

        Task<ICustomerAccountsResponse> ProcessRemaining(string entityType, string entityId, ICustomerDetailRequest customerDetailRequest);


        Task<bool> AddAccountPreference(string entityType, string entityId, IAccountPreferenceRequest accountPreference);

        Task<IBankAccount> GetBankAccountDetails(string entityType, string entityId, string accountId);

        Task<IList<IBankAccount>> GetAllAccountDetails(string entityType, string entityId);

        Task<IAccountTypeResponse> GetAccountByType(string entityType, string entityId, IAccountTypeRequest request);

        Task AddBankLink(string bankLinkId, string entityType, string entityId);

        Task<IPlaidRefreshResponse> PlaidRefreshLinking(string entityId);

        Task<IBankAccount> GetBankLinkAccountDetails(string entityType, string entityId, string accountId);

        Task<List<string>> DeleteToken();
    }
}

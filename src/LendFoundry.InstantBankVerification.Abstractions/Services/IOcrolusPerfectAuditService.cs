﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
#if DOTNET2
using LendFoundry.Syndication.PerfectAudit.Abstractions;
#else
using LendFoundry.Syndication.PerfectAudit.Abstractions;
#endif


namespace LendFoundry.InstantBankVerification
{
    public interface IOcrolusPerfectAuditService
    {
        Task<IResponseConnection> CreateBook(string entityType, string entityId, RequestConnection connection);

        Task<dynamic> GetTransactions(string book_pk);

        Task<CommonResponse> UploadStatement(string entityType, string entityId,/* string name,*/ List<FileuploadData> statement, string bookPk);

        Task<Analytics> GetSummaryDataByBookId(string bookPk);
        Task<CommonResponse> UploadStatement(string entityType, string entityId, byte[] statement, string name, string bookPk);

    }
}

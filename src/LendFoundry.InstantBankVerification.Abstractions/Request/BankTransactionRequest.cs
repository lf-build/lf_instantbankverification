﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification.Request
{
    public class BankTransactionRequest : IBankTransactionRequest
    {
        public string TransactionId { get; set; }
        public string AccountId { get; set; }
        public string ProviderAccountId { get; set; }
        public double Amount { get; set; }
        public string TransactionDate { get; set; }
        public string Description { get; set; }
        public string Pending { get; set; }
        public string CategoryId { get; set; }
        public string CustomerId { get; set; }
        public IList<string> Categories { get; set; }
        public string Meta { get; set; }
    }

    public interface IBankTransactionRequest
    {
        string TransactionId { get; set; }
        string AccountId { get; set; }
        string ProviderAccountId { get; set; }
        double Amount { get; set; }
        string TransactionDate { get; set; }
        string Description { get; set; }
        string Pending { get; set; }
        string CategoryId { get; set; }
        string CustomerId{ get; set; }
        IList<string> Categories { get; set; }
        string Meta { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class CustomerDetailRequest : ICustomerDetailRequest
    {
        #region Public Properties

        public string FinicityAppToken { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string InstitutionId { get; set; }

        #endregion
    }

    public interface ICustomerDetailRequest
    {
        string FinicityAppToken { get; set; }
        string CustomerId { get; set; }
        string AccountId { get; set; }
        string InstitutionId { get; set; }
    }
}

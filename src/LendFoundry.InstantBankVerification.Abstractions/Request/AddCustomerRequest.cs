﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class AddCustomerRequest: IAddCustomerRequest
    {
        #region Public Properties
        public string FinicityAppToken { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        #endregion

        #region Public Constructor

        public AddCustomerRequest(IAddCustomerRequest addCustomerRequest)
        {
            if (addCustomerRequest != null)
            {
                FinicityAppToken = addCustomerRequest.FinicityAppToken;
                UserName = addCustomerRequest.UserName;
                FirstName = addCustomerRequest.FirstName;
                LastName = addCustomerRequest.LastName;
            }
        }

        #endregion
    }

    public interface IAddCustomerRequest
    {
        string FinicityAppToken { get; set; }
        string UserName { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
    }
}

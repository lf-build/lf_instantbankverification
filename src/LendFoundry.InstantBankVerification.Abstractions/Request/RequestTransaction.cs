﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public interface IRequestTransaction
    {
         string ItemId { get; set; }
         string WebhookCode { get; set; }

        int TransactionCount { get; set; }
    }

    public class RequestTransaction : IRequestTransaction
    {
        [JsonProperty("itemId")]
        public string ItemId { get; set; }

        [JsonProperty("webhookCode")]
        public string WebhookCode { get; set; }

        [JsonProperty("transactionCount")]
        public int TransactionCount { get; set; }
    }
}

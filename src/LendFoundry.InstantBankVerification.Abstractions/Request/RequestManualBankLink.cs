﻿using Newtonsoft.Json;
using System;

namespace LendFoundry.InstantBankVerification
{
    public interface IRequestManualBankLink
    {
        string FirstName { get; set; }

        string LastName { get; set; }

        string UserMailId { get; set; }

        string BankName { get; set; }

        string RoutingNumber { get; set; }

        string WireRoutingNumber { get; set; }

        string AccountNumber { get; set; }

        string AccountNumberMask { get; set; }

        string AccountType { get; set; }

         DateTime? BalanceAsOfDate { get; set; }

         double? CurrentBalance { get; set; }

        double? AvailableBalance { get; set; }

         string ManualType { get; set; }

        string FileName { get; set; }

        string FileContent { get; set; }

        string Type { get; set; }
    }
    public class RequestManualBankLink : IRequestManualBankLink
    {
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("userMailId")]
        public string UserMailId { get; set; }

        [JsonProperty("bankName")]
        public string BankName { get; set; }

        [JsonProperty("routingNumber")]
        public string RoutingNumber { get; set; }

        [JsonProperty("wireRoutingNumber")]
        public string WireRoutingNumber { get; set; }

        [JsonProperty("accountNumber")]
        public string AccountNumber { get; set; }

        [JsonProperty("accountNumberMask")]
        public string AccountNumberMask { get; set; }

        [JsonProperty("accountType")]
        public string AccountType { get; set; }

        [JsonProperty("balanceAsOfDate")]
        public DateTime? BalanceAsOfDate { get; set; }

        [JsonProperty("currentBalance")]
        public double? CurrentBalance { get; set; }

        [JsonProperty("availableBalance")]
        public double? AvailableBalance { get; set; }

        [JsonProperty("manualType")]
        public string ManualType { get; set; }

        [JsonProperty("fileName")]
        public string FileName { get; set; }

        [JsonProperty("fileContent")]
        public string FileContent { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }
}

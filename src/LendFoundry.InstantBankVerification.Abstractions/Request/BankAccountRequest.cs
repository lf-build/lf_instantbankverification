﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class BankAccountRequest : IBankAccountRequest
    {
        public string AccountNumber { get; set; }
        public string BankName { get; set; }
        public string RoutingNumber { get; set; }
        public double CurrentBalance { get; set; }
        public double AvailableBalance { get; set; }
        public string AccountType { get; set; }
        public string Source { get; set; }
        public string AccountId { get; set; }
        public string BalanceAsOfDate { get; set; }
        public string NameOnAccount { get; set; }
        public string ProviderAccountId { get; set; }
        public string Version { get; set; }
    }
    public interface IBankAccountRequest
    {
        string AccountNumber { get; set; }
        string BankName { get; set; }
        string RoutingNumber { get; set; }
        double CurrentBalance { get; set; }
        double AvailableBalance { get; set; }
        string AccountType { get; set; }
        string Source { get; set; }
        string AccountId { get; set; }
        string BalanceAsOfDate { get; set; }
        string NameOnAccount { get; set; }
        string ProviderAccountId { get; set; }
        string Version { get; set; }
    }

}

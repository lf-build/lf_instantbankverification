﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class ConnectLinkRequest: IConnectLinkRequest
    {
        #region Public Properties

        public string FinicityAppToken { get; set; }
        public string CustomerId { get; set; }
        public string RedirectUri { get; set; }
        public string Type { get; set; }

        #endregion

        #region Public Constructor

        public ConnectLinkRequest(IConnectLinkRequest connectLinkRequest)
        {
            if (connectLinkRequest != null)
            {
                FinicityAppToken = connectLinkRequest.FinicityAppToken;
                CustomerId = connectLinkRequest.CustomerId;
                RedirectUri = connectLinkRequest.RedirectUri;
                Type = connectLinkRequest.Type;
               
            }
        }

        #endregion
    }

    public interface IConnectLinkRequest
    {
        string FinicityAppToken { get; set; }

        string CustomerId { get; set; }
        string RedirectUri { get; set; }
        string Type { get; set; }
       
    }
}

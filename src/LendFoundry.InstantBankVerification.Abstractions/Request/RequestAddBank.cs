﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.InstantBankVerification
{
    public interface IRequestAddBank
    {
        string Token { get; set; }

        string ItemId { get; set; }
        string RoutingNumber { get; set; }

        string AccountNumber { get; set; }

        string AccountId { get; set; }

        List<string> AccountType { get; set; }
    }

    public class RequestAddBank : IRequestAddBank
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("itemId")]
        public string ItemId { get; set; }

        [JsonProperty("routingNumber")]
        public string RoutingNumber { get; set; }

        [JsonProperty("accountNumber")]
        public string AccountNumber { get; set; }

        [JsonProperty("accountId")]
        public string AccountId { get; set; }

        [JsonProperty("accountType")]
        public List<string> AccountType { get; set; }
    }
}

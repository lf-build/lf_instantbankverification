﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class AddTransactionDetailRequest : IAddTransactionDetailRequest
    {
        #region Public Properties

        public double Amount { get; set; }
        public string Description { get; set; }
        public long PostedDate { get; set; }
        public long TransactionDate { get; set; }
        public string FinicityAppToken { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }

        #endregion
    }

    public interface IAddTransactionDetailRequest
    {
        double Amount { get; set; }
        string Description { get; set; }
        long PostedDate { get; set; }
        long TransactionDate { get; set; }
        string FinicityAppToken { get; set; }
        string CustomerId { get; set; }
        string AccountId { get; set; }
    }

}

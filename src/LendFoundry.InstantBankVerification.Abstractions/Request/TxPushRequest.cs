﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class TxPushRequest : ITxPushRequest
    {
        #region Public Properties

        public string FinicityAppToken { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string CallBackURL { get; set; }
        public string SubscriptionId { get; set; }

        #endregion

        #region Public Constructor

        public TxPushRequest()
        {

        }

        #endregion
    }
    public interface ITxPushRequest
    {
        string FinicityAppToken { get; set; }
        string CustomerId { get; set; }
        string AccountId { get; set; }
        string CallBackURL { get; set; }
        string SubscriptionId { get; set; }
    }
}

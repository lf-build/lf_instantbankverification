﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification.Request
{
    public class TransactionRequest : ITransactionRequest
    {
        #region Public Properties

        public string FinicityAppToken { get; set; }
        public string CustomerId { get; set; }
        public int? TransactionDurationInDays { get; set; }
        //public long? AccountId { get; set; }
         public List<SimulationAccounts> SimulationAccounts{ get; set; }
        #endregion
    }

    public interface ITransactionRequest
    {
        string FinicityAppToken { get; set; }
        string CustomerId { get; set; }
        int? TransactionDurationInDays { get; set; }
        //long? AccountId { get; set; }
         List<SimulationAccounts> SimulationAccounts{ get; set; }
    }

     public class SimulationAccounts 
        {
            public long SimulationAccountId { get; set; }
            public string AccountType { get; set; }

        }

}

﻿using Newtonsoft.Json;

namespace LendFoundry.InstantBankVerification
{
    public interface IRequestConnection
    {
        string ProviderName { get; set; }
        string ProviderToken { get; set; }
        string Type { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string UserMailId { get; set; }
        string LinkSessionId { get; set; }
        string LinkReferenceId { get; set; }
        string BankName { get; set; }
        string BankId { get; set; }
    }

    public class RequestConnection : IRequestConnection
    {
        [JsonProperty("providerName")]
        public string ProviderName { get; set; }

        [JsonProperty("providerToken")]
        public string ProviderToken { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("userMailId")]
        public string UserMailId { get; set; }
        [JsonProperty("linkSessionId")]
        public string LinkSessionId { get; set; }
        [JsonProperty("referenceId")]
        public string LinkReferenceId { get; set; }
        [JsonProperty("bankName")]
        public string BankName { get; set; }

        [JsonProperty("bankId")]
        public string BankId { get; set; }
    }
}

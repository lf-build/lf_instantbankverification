﻿namespace LendFoundry.InstantBankVerification
{
    public enum Provider
    {
        Manual = 0,
        Plaid = 1,
        Finicity = 2,
        Ocrolus = 3
    }
}

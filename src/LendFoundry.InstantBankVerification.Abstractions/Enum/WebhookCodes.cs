﻿namespace LendFoundry.InstantBankVerification
{
    public enum WebhookCodes
    {
        Initial_Update,
        Historical_Update,
        Default_Update
        //Transactions_Removed
    }
}

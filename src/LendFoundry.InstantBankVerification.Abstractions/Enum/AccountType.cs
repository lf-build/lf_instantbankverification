﻿namespace LendFoundry.InstantBankVerification
{
    public enum AccountType
    {
        Funding = 1,
        Cashflow = 2
    }
}

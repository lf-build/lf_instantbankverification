﻿namespace LendFoundry.InstantBankVerification
{
    public enum FilterType
    {
        All = 0,
        Latest = 1
    }
}

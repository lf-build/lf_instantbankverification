﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public interface IBankAccountRepository : IRepository<IBankAccount>
    {
        Task AddAccount(IBankAccount account);
        Task<IBankAccount> GetAccount(string bankLinkId, string accountNumber);
        Task<IList<IBankAccount>> GetAccounts(string bankLinkId);
        Task<IBankAccount> GetAccount(string bankLinkId);
        Task<bool> UpdateBankAccount(string bankLinkId);

        Task<bool> UpdateBankAccount(string bankLinkId, string accountNumber);

        Task<bool> AccountExists(string accountId);

        Task<IBankAccount> GetAccountByProviderAccountId(string accountId);

        Task<bool> UpdateAccount(IBankAccount account);

        Task<bool> ResetAccountPreference(List<string> bankLinkIds, AccountType accountType);

        Task<List<string>> GetAccountIds(string bankLinkId);

        Task<bool> UpdateAccountPreference(IAccountPreference request, int ? accountVersion);

        Task<IBankAccount> GetAccountDetails(List<string> bankLinkIds, string accontId);

        Task<IList<IBankAccount>> GetAccountsByEntityId(List<string> bankLinkIds);

        Task<IBankAccount> GetCashflowAccount(List<string> bankLinkIds);

        Task<IBankAccount> GetFundingAccount(List<string> bankLinkIds);
    }
}

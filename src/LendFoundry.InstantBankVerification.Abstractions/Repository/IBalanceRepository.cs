﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public interface IBalanceRepository : IRepository<IRunningBalanceDetails>
    {
        Task<IList<IRunningBalanceDetails>> GetProviderAccountIds();

        Task<bool> UpdateAccountDetails(string Id);

        Task<bool> AddAccountDetails(IEnumerable<IRunningBalanceDetails> accounts);
        

    }
}

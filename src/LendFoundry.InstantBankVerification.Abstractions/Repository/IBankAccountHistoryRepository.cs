﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public interface IBankAccountHistoryRepository : IRepository<IBankAccount>
    {
        Task AddAccount(IBankAccount account);
        Task<IBankAccount> GetAccount(string bankLinkId, string accountNumber);
        Task<IList<IBankAccount>> GetAccounts(string bankLinkId);

        Task<bool> UpdateBankAccount(string bankLinkId);

        Task<bool> UpdateBankAccount(string bankLinkId, string accountNumber);

        Task<bool> AccountExists(string accountId);

        Task<bool> UpdateAccount(IBankAccount account);
    }
}

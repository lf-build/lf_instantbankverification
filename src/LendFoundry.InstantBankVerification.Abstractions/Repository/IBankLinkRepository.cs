﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public interface IBankLinkRepository : IRepository<IBankLink>
    {
        Task<IBankLink> GetBankLinkDataById(string entityId);

        Task<string> GetBankLinkId(string token);

        Task<bool> UpdateBankLink(string token);
        Task<bool> UpdateBankLink(string entityId, int ProviderId);

        Task<string> GetAccessToken(string itemId);

        Task<IBankLink> GetBankLinkData(string itemId);

        Task<bool> IsBankLinked(string itemId, string token);

        Task<bool> IsBankLinkedByEntityId(string entityId);

        Task<IBankLink> GetBankLinkDataByEntityId(string entityId);

        Task<List<IBankLink>> GetBankLinksByEntityId(string entityId);

        Task<IBankLink> GetBankLinkingData(string entityId, string id);
        Task<List<IBankLink>> GetAllBankLinkData();
    }
}

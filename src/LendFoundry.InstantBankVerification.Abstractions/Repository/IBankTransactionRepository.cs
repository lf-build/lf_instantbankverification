﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public interface IBankTransactionRepository : IRepository<IBankTransaction>
    {
        Task<bool> AddTransaction(IBankTransaction transaction);

        Task<List<IBankTransaction>> GetTransactions(string entityType, string entityId, string accountId, DateTime fromDate, DateTime toDate);

        Task<bool> AddTransactions(IEnumerable<IBankTransaction> transactions);

        Task<IList<IBankTransaction>> GetTransactions(string accountId);

        Task<IList<string>> GetTransactionIdsByProviderBankLinkId(string providerLinkId);

        Task<double?> GetFirstTransactionRunningBalance(string accountId);

        Task<long> GetTransactionCount(string providerBankLinkId);

        Task<IList<IBankTransaction>> GetFilterTransactionData(string accountId, DateTime serverDate, int filterInMonths);
        Task<bool> UpdateTransaction(IBankTransaction transaction); 
    }
}

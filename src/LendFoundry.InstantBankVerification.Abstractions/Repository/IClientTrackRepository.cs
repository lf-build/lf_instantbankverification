﻿using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    /// <summary>
    /// IClientTrackRepository
    /// </summary>
    public interface IClientTrackRepository : IRepository<IClientTrack>
    {
        /// <summary>
        /// AddItem
        /// </summary>
        /// <param name="item">item</param>
        /// <returns>string</returns>
        Task<string> AddItem(IClientTrack item);
    }
}

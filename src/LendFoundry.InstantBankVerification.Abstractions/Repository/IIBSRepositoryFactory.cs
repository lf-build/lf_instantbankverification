﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.InstantBankVerification
{
    public interface IIBSRepositoryFactory
    {
        IBankLinkRepository CreateBankLinkRepository(ITokenReader reader);

        IBankAccountRepository CreateBankAccountRepository(ITokenReader reader);

        IBankAccountHistoryRepository CreateBankAccountHistoryRepository(ITokenReader reader);

        IBankTransactionRepository CreateBankTransactionRepository(ITokenReader reader);

        IClientTrackRepository CreateClientTrackRepository(ITokenReader reader);
        IBalanceRepository CreateBalanceRepository(ITokenReader reader);

    }
}

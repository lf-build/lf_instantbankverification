﻿using LendFoundry.InstantBankVerification.Proxy.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class TransactionsResponse: ITransactionsResponse
    {
        #region Public Properties

        public string MoreAvailable { get; set; }
        public int TotalRecords { get; set; }
        public int DisplayingRecords { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string Sort { get; set; }
        public IList<ITransaction> Transactions { get; set; }

        #endregion

        #region Public Constructor

        public TransactionsResponse(Proxy.Response.ITransactionsResponse response)
        {
            TotalRecords = response.TotalRecords;
            DisplayingRecords = response.DisplayingRecords;
            MoreAvailable = response.MoreAvailable;
            FromDate = response.FromDate;
            ToDate = response.ToDate;
            Sort = response.Sort;
            Transactions = response.Transactions;
        }

        #endregion
    }

    public interface ITransactionsResponse
    {
        string MoreAvailable { get; set; }
        int TotalRecords { get; set; }
        int DisplayingRecords { get; set; }
        string FromDate { get; set; }
        string ToDate { get; set; }
        string Sort { get; set; }
        IList<ITransaction> Transactions { get; set; }
    }
}

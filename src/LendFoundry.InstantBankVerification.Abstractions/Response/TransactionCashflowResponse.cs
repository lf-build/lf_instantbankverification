﻿using Newtonsoft.Json;

namespace LendFoundry.InstantBankVerification
{
    public interface ITransactionCashflowResponse
    {
         string EntityId { get; set; }

         string EntityType { get; set; }

        string FilterType { get; set; }

        EventData EventData { get; set; }
    }

    public class TransactionCashflowResponse : ITransactionCashflowResponse
    {
        [JsonProperty("entityId")]
        public string EntityId { get; set; }

        [JsonProperty("entityType")]
        public string EntityType { get; set; }

        [JsonProperty("filterType")]
        public string FilterType { get; set; }

        [JsonProperty("eventData")]
        public EventData EventData { get; set; }
    }
}

﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class CustomerAccountsResponse : ICustomerAccountsResponse
    {
        #region Public Properties

        public IList<Proxy.Model.ICustomerAccountsDetails> CustomerAccountsDetails { get; set; }

        #endregion

        #region Public Constructor

        public CustomerAccountsResponse(Proxy.Response.ICustomerAccountsResponse customerAccountsResponse)
        {
            if (customerAccountsResponse != null)
            {
                CustomerAccountsDetails = customerAccountsResponse.CustomerAccountsDetails;
            }
        }

        public CustomerAccountsResponse(dynamic dynamicCustomerAccountsResponse)
        {
            if (dynamicCustomerAccountsResponse != null)
            {
                string jsonString = JsonConvert.SerializeObject(dynamicCustomerAccountsResponse);
                Proxy.Response.CustomerAccountsResponse customerAccountsResponse = JsonConvert.DeserializeObject<Proxy.Response.CustomerAccountsResponse>(jsonString);
                CustomerAccountsDetails = customerAccountsResponse.CustomerAccountsDetails;
            }
        }

        #endregion


    }
    public interface ICustomerAccountsResponse
    {
        IList<Proxy.Model.ICustomerAccountsDetails> CustomerAccountsDetails { get; set; }
    }
}

﻿using LendFoundry.InstantBankVerification.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class TxPushResponse : ITxPushResponse
    {
        #region Public Properties

        public IList<ISubscription> Subscriptions { get; set; }

        #endregion

        #region Public Constructor

        public TxPushResponse(Proxy.Response.ITxPushResponse response)
        {
            Subscriptions = response.Subscriptions;
        }

        #endregion
    }

    public interface ITxPushResponse
    {
        IList<ISubscription> Subscriptions { get; set; }
    }
}

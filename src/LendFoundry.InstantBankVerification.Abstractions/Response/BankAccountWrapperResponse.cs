﻿namespace LendFoundry.InstantBankVerification
{
    public interface IBankAccountWrapperResponse
    {
        string AccountId { get; set; }
        string Mask { get; set; }

        double? AvailableBalance { get; set; }

        double? CurrentBalance { get; set; }

        
    }

    public class BankAccountWrapperResponse : IBankAccountWrapperResponse
    {
        public string AccountId { get; set; }
        public string Mask { get; set; }

        public double?  AvailableBalance { get; set; }

        public double?  CurrentBalance { get; set; }
        
    }
}

﻿namespace LendFoundry.InstantBankVerification
{
    public interface IResponseConnection
    {
        string Token { get; set; }

        string BankLinkId { get; set; }

        string ProviderLinkId { get; set; }

    }
    public class ResponseConnection : IResponseConnection
    {
        public string Token { get; set; }

        public string BankLinkId { get; set; }

        public string ProviderLinkId { get; set; }

    }
}

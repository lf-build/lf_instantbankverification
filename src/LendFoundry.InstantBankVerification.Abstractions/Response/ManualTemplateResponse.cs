﻿namespace LendFoundry.InstantBankVerification
{
    public interface IManualTemplateResponse
    {
         Account[] Accounts { get; set; }
         Transaction[] Transactions { get; set; }
    }
    public class ManualTemplateResponse : IManualTemplateResponse
    {
        public Account[] Accounts { get; set; }
        public Transaction[] Transactions { get; set; }
    }
}

//public class ManualAccount
//{
//    public string ProviderAccountId { get; set; }
//    public string AvailableBalance { get; set; }
//    public string CurrentBalance { get; set; }
//    public string BankName { get; set; }
//    public string AccountType { get; set; }
//    public string AccountNumber { get; set; }
//    public string Source { get; set; }
//}

//public class ManualTransaction
//{
//    public string ProviderAccountId { get; set; }
//    public string AccountId { get; set; }
//    public string Amount { get; set; }
//    public string Categories { get; set; }
//    public string CategoryId { get; set; }
//    public string Description { get; set; }
//    public string Meta { get; set; }
//    public string Pending { get; set; }
//    public string TransactionDate { get; set; }
//}

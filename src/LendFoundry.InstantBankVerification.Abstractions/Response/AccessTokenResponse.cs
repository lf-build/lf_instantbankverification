﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class AccessTokenResponse : IAccessTokenResponse
    {
        #region Public Constructor

        public AccessTokenResponse()
        {

        }

        public AccessTokenResponse(IAccessTokenResponse response)
        {
            if (response != null)
                AccessToken = response.AccessToken;
        }

        #endregion
        public string AccessToken { get; set; }
    }
    public interface IAccessTokenResponse
    {
        string AccessToken { get; set; }
    }
}

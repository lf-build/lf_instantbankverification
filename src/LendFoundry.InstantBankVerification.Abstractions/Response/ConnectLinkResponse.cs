﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class ConnectLinkResponse : IConnectLinkResponse
    {
        #region Public Constructor

        public ConnectLinkResponse()
        {

        }

        public ConnectLinkResponse(IAccessTokenResponse response)
        {
            if (response != null)
                link = response.AccessToken;
        }

        #endregion
        public string link { get; set; }
    }
    public interface IConnectLinkResponse
    {
        string link { get; set; }
    }
}

﻿namespace LendFoundry.InstantBankVerification
{
    public interface IPlaidRefreshResponse
    {
         string PublicToken { get; set; }

        string RequestId { get; set; }
    }
    public class PlaidRefreshResponse : IPlaidRefreshResponse
    {
        public PlaidRefreshResponse()
        {

        }
        public PlaidRefreshResponse(string publicToken , string requestId)
        {
            PublicToken = publicToken;
            RequestId = requestId;
        }


        public string PublicToken { get; set; }

        public string RequestId { get; set; }
    }
}

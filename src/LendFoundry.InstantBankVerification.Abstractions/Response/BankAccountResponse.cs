﻿using LendFoundry.Foundation.Date;

namespace LendFoundry.InstantBankVerification
{
    public interface IBankAccountResponse
    {
        string AccountId { get; set; }

        string RoutingNumber { get; set; }

        string BankName { get; set; }

        string AccountNumber { get; set; }

        double? CurrentBalance { get; set; }

        double? AvailableBalance { get; set; }

        string AccountType { get; set; }

        string Source { get; set; }

        TimeBucket BalanceAsOfDate { get; set; }
    }

    public class BankAccountResponse : IBankAccountResponse
    {
        public string AccountId { get; set; }

        public string RoutingNumber { get; set; }

        public string BankName { get; set; }

        public string AccountNumber { get; set; }

        public double? CurrentBalance { get; set; }

        public double? AvailableBalance { get; set; }

        public string AccountType { get; set; }

        public string Source { get; set; }

        public TimeBucket BalanceAsOfDate { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LendFoundry.InstantBankVerification
{
    public class EventData
    {
        [JsonProperty("accounts")]
        public Account Accounts { get; set; }

        [JsonProperty("transactions")]
        public List<Transaction> Transactions { get; set; }

        [JsonProperty("request")]
        public string Request { get; set; }

        [JsonProperty("referenceNumber")]
        public string ReferenceNumber { get; set; }

    }

    public class Account
    {

        [JsonProperty("bankName")]
        public string BankName { get; set; }
        [JsonProperty("nameOnAccount")]
        public string NameOnAccount { get; set; }
        [JsonProperty("officialAccountName")]
        public string OfficialAccountName { get; set; }

        [JsonProperty("providerAccountId")]
        public string ProviderAccountId { get; set; }

        [JsonProperty("accountNumber")]
        public string AccountNumber { get; set; }

        [JsonProperty("currentBalance")]
        public double? CurrentBalance { get; set; }

        [JsonProperty("availableBalance")]
        public double? AvailableBalance { get; set; }

        [JsonProperty("accountType")]
        public string AccountType { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("runningBalance")]
        double RunningBalance { get; set; }
    }


    public class Transaction
    {
        [JsonProperty("entityId")]
        public string EntityId { get; set; }

        [JsonProperty("entityType")]
        public string EntityType { get; set; }

        [JsonProperty("accountId")]
        public string AccountId { get; set; }

        [JsonProperty("providerAccountId")]
        public string ProviderAccountId { get; set; }

        [JsonProperty("amount")]
        public double Amount { get; set; }

        [JsonProperty("transactionDate")]
        public DateTime? TransactionDate { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("pending")]
        public bool Pending { get; set; }

        [JsonProperty("categoryId")]
        public string CategoryId { get; set; }

        [JsonProperty("categories")]
        public string[] Categories { get; set; }

        [JsonProperty("meta")]
        public string Meta { get; set; }

        [JsonProperty("createdOn")]
        public DateTimeOffset CreatedOn { get; set; }

        [JsonProperty("updatedOn")]
        public DateTimeOffset? UpdatedOn { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("runningBalance")]
        public double RunningBalance { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class AddCustomerResponse : IAddCustomerResponse
    {
        #region Public Properties

      
        public string CustomerId { get; set; }
      
        public string CreatedDate { get; set; }

        #endregion      
    }
    public interface IAddCustomerResponse
    {
        string CustomerId { get; set; }
        string CreatedDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class FinicityConfiguration
    {
        public string FinicitySyndicationURL { get; set; }
        public string GetAccessTokenEndPoint { get; set; }
        public string AddCustomerEndPoint { get; set; }
        public string GenerateConnectLinkEndPoint { get; set; }
        public string GetCustomerAccountsEndPoint { get; set; }
        public string GetTransactionforAllAccountEndPoint { get; set; }
        public string EnableTxPushEndPoint { get; set; }
        public string AddTestTransactionEndPoint { get; set; }
        public string LoadHistoricTransactionEndPoint { get; set; }
        public string AuthorizationToken { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.InstantBankVerification
{
    public class EventRequest : IEventRequest
    {
        public string DERuleName { get; set; }
       
        public object data { get; set; }
    }

    public interface IEventRequest
    {
        string DERuleName { get; set; }
       
        object data { get; set; }
    }

}
